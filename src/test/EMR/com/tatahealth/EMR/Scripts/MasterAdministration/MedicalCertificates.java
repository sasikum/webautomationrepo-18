package com.tatahealth.EMR.Scripts.MasterAdministration;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.util.Calendar;
import java.util.Properties;
import java.util.Random;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.tatahealth.EMR.pages.Login.LoginPage;
import com.tatahealth.EMR.pages.Login.SweetAlertPage;
import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.Billing.Login;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.API.libraries.SheetsAPI;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.DownloadPDF;
import com.tatahealth.ReusableModules.Web_Testbase;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.Scripts.Consultation.GlobalPrintTest;
import com.tatahealth.EMR.Scripts.Login.*;
import com.tatahealth.EMR.pages.MasterAdministration.MedicalCertificatesPage;
import com.tatahealth.ReusableModules.*;



public class MedicalCertificates {

	public static ExtentTest logger;
	MedicalCertificatesPage MCP = new MedicalCertificatesPage();
		
	public String getrandomtemplate()
	{
		Random rnum = new Random();
		return "Sample"+rnum.nextInt(20000); 
		
	}

		
	String MedicalCertifcateTemplate=this.getrandomtemplate();
	String LeaveCertificateTemplate=this.getrandomtemplate();
		
	//String doctor=SheetsAPI.getDataProperties(Web_Testbase.input + ".Doctor");
	//String patient=SheetsAPI.getDataProperties(Web_Testbase.input + ".patient");
	
	String doctor="Palle";
	String patient="Sampleuser";
	
	public void clickFutureDate(String expected_month, String day) throws Exception
	{
		
		while(true)
		{
			String text = Web_GeneralFunctions.getText(MCP.getmonth(Login_Doctor.driver, logger), "Getting the month from the cal", Login_Doctor.driver, logger);
			System.out.println(text+" -------");
			if(text.equals(expected_month))
			{
				break;
			}
			
			else
			{
				
			Web_GeneralFunctions.click(MCP.getnextbutton(Login_Doctor.driver, logger), "Clicking on next button", Login_Doctor.driver, logger);
			
			}
						
		}
		
		
		Web_GeneralFunctions.click(MCP.getday(Login_Doctor.driver, logger, day), "Clicking on day", Login_Doctor.driver, logger);
		
	}
	
	
	@BeforeClass(alwaysRun = true, groups= {"Regression","MasterAdministration"})
	@Parameters("role")
	public static void beforeClass(String role) throws Exception {
		Login_Doctor.executionName="Medical Certificates";		
		Login_Doctor.LoginTestwithDiffrentUser(role);
		}

	
	@AfterClass(alwaysRun = true, groups = { "Regression", "MasterAdministration" })
	public static void afterClass() throws Exception {

		Login_Doctor.LogoutTest();
		Reports.extent.flush();
		
	}
	
	//String doctor = SheetsAPI.getDataProperties(Web_Testbase.input + ".doctor");
	//String patient = SheetsAPI.getDataProperties(Web_Testbase.input + ".patient");
	
	@Test(priority = 1,groups= {"Regression","MedicalCertificateTest"})
	public synchronized void MedicalCertificateTest() throws Exception {

		logger = Reports.extent.createTest("MedicalCertificate");
		
		Web_GeneralFunctions.click(MCP.getleftmainmenu(Login_Doctor.driver, logger), "Clicking on Humburger menu", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(MCP.getsidelink(Login_Doctor.driver, logger), "Click on Master Admin Tab", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(MCP.getlink(Login_Doctor.driver, logger), "Clicking on Medical Certificate tab", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.selectElementByVisibleText(MCP.getMedicalFitnessSelectbox(Login_Doctor.driver, logger), "Medical Fitness", "Selecting Medical Fitness..", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(MCP.getButton(Login_Doctor.driver, logger), "Clicking on Create Template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(5);
		Web_GeneralFunctions.sendkeys(MCP.gettemplate(Login_Doctor.driver, logger), MedicalCertifcateTemplate, "Creating a Template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(MCP.getlinktexttitle(Login_Doctor.driver, logger), "Selecting the title for template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(MCP.getlinktextpatient(Login_Doctor.driver, logger), "Selecting the patient for template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(MCP.getlinktextdiagnosis(Login_Doctor.driver, logger), "Selecting the diagnosis for template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(MCP.getlinktextdoctor(Login_Doctor.driver, logger), "Selecting the doctor for template..", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(MCP.getPriviewButton(Login_Doctor.driver, logger), "Clicking on Preview button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(MCP.getclosebuttonPreview(Login_Doctor.driver, logger), "Click on preveiw close button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(MCP.getSaveButton(Login_Doctor.driver, logger), "Click on Save Button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, MCP.gettoast_messege(Login_Doctor.driver, logger));
		Web_GeneralFunctions.wait(3);
		Assert.assertTrue(MCP.gettoast_messege(Login_Doctor.driver, logger).isDisplayed(), "Toast Messege displayed successfully");
		
					
	}
	
	@Test(priority = 2, groups= {"Regression","LeaveCertificateTest"})
	public synchronized void LeaveCertificateTest() throws Exception {

		logger = Reports.extent.createTest("LeaveCertificate");
		
		Web_GeneralFunctions.click(MCP.getleftmainmenu(Login_Doctor.driver, logger), "Clicking on Humburger menu", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(MCP.getsidelink(Login_Doctor.driver, logger), "Click on Master Admin Tab", Login_Doctor.driver, logger);
		
		//Above step failing
		
		Web_GeneralFunctions.click(MCP.getlink(Login_Doctor.driver, logger), "Clicking on Medical Certificate tab", Login_Doctor.driver, logger);
				
		Web_GeneralFunctions.selectElementByVisibleText(MCP.getMedicalFitnessSelectbox(Login_Doctor.driver, logger), "Leave certificate", "Selecting Medical Fitness..", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(MCP.getButton(Login_Doctor.driver, logger), "Clicking on Create Template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(5);
		Web_GeneralFunctions.sendkeys(MCP.gettemplate(Login_Doctor.driver, logger), LeaveCertificateTemplate, "Creating a Template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(MCP.getlinktexttitle(Login_Doctor.driver, logger), "Selecting the title for template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(MCP.getlinktextpatient(Login_Doctor.driver, logger), "Selecting the patient for template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(MCP.getlinktextdiagnosis(Login_Doctor.driver, logger), "Selecting the diagnosis for template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(MCP.getlinktextdoctor(Login_Doctor.driver, logger), "Selecting the doctor for template..", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(MCP.getlinktextdiagnosis(Login_Doctor.driver, logger), "Clicking the diagonisis", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(MCP.getlinktextnofDays(Login_Doctor.driver, logger), "Clicking on No of Days", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(MCP.getlinktextfromDate(Login_Doctor.driver, logger), "Clicking on From date", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(MCP.getlinktexttoDate(Login_Doctor.driver, logger), "Clicking on the to date", Login_Doctor.driver, logger);
		
		Web_GeneralFunctions.click(MCP.getPriviewButton(Login_Doctor.driver, logger), "Clicking on Preview button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(MCP.getclosebuttonPreview(Login_Doctor.driver, logger), "Click on preveiw close button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(MCP.getSaveButton(Login_Doctor.driver, logger), "Click on Save Button", Login_Doctor.driver, logger);
		
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, MCP.gettoast_messege(Login_Doctor.driver, logger));
		Web_GeneralFunctions.wait(3);
		Assert.assertTrue(MCP.gettoast_messege(Login_Doctor.driver, logger).isDisplayed(), "Toast Messege displayed successfully");
					
	}
	
	
	@Parameters({"doctor","patient"})
	@Test(priority = 3, groups= {"Regression","MedicalCertificate_Printleave_MFitness_Cert"})
	public synchronized void MedicalCertificate_Printleave_MFitness_Cert(String doctor, String patient) throws Exception {

		logger = Reports.extent.createTest("Print M/L Certificate");
		
		String noofDays="3";
		
		Web_GeneralFunctions.click(MCP.getleftmainmenu(Login_Doctor.driver, logger), "Clicking on Humburger menu", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(MCP.getMedicalCertificateTab(Login_Doctor.driver, logger), "Click on Medical Certificate Tab", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		
		Web_GeneralFunctions.click(MCP.getPrintmedicalCertificatelink(Login_Doctor.driver, logger), "Clicking on Print medical certificate tab", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		
		Web_GeneralFunctions.selectElementByVisibleText(MCP.getCertificatetype_Selectbox(Login_Doctor.driver, logger), "Select Certificate", "Not selecting any certificate", Login_Doctor.driver, logger);
		//Assert.assertTrue(MCP.getSelectTemplate_Selectbox2(Login_Doctor.driver, logger).getSize().toString().equals("1"), "No Template is found in the select box");
				
		Web_GeneralFunctions.selectElementByVisibleText(MCP.getCertificatetype_Selectbox(Login_Doctor.driver, logger), "Leave certificate", "Clicking on the select box", Login_Doctor.driver, logger);
		Web_GeneralFunctions.selectElementByVisibleText(MCP.getSelectTemplate_Selectbox2(Login_Doctor.driver, logger), LeaveCertificateTemplate, "Selcting the Leave certificate template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		
		try
		{
		Web_GeneralFunctions.sendkeys(MCP.gettemplate_textarea(Login_Doctor.driver, logger), "test textarea", "Entering the text to template", Login_Doctor.driver, logger);
		}
		catch (Exception e)
		{
        Assert.assertTrue(MCP.gettemplate_textarea(Login_Doctor.driver, logger).getAttribute("readonly").contains("true"),"The textarea is readOnly");	
		}
		
		Web_GeneralFunctions.selectElementByVisibleText(MCP.getDoctor_Selectbox(Login_Doctor.driver, logger), doctor, "Selecting Doctor feild", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		Web_GeneralFunctions.selectElementByVisibleText(MCP.getTitle_Selectbox(Login_Doctor.driver, logger), "Mr", "Selcting tilte", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		Web_GeneralFunctions.sendkeys(MCP.getPatient_Inputbox(Login_Doctor.driver, logger), "asdfjkl", "Entered free text", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		Assert.assertTrue(MCP.getpatientsearchresult(Login_Doctor.driver, logger).isDisplayed(), "No result found pop up shown successfully");
		Web_GeneralFunctions.clear(MCP.getPatient_Inputbox(Login_Doctor.driver, logger), "Clearing the patient input box", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		//Web_GeneralFunctions.sendkeys(MCP.getPatient_Inputbox(Login_Doctor.driver, logger), patient, "Enter Patient", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(MCP.getPatient_Inputbox(Login_Doctor.driver, logger), "s", "Enter keys", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(MCP.getPatient_Inputbox(Login_Doctor.driver, logger), "a", "Enter keys", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(MCP.getPatient_Inputbox(Login_Doctor.driver, logger), "m", "Enter keys", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		Web_GeneralFunctions.click(MCP.getPatient_selectlink(Login_Doctor.driver, logger), "Selecting the patient", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(MCP.getDiagonsis_Inputbox(Login_Doctor.driver, logger), "xyz Diognaosis", "Enter Diagnosis", Login_Doctor.driver, logger);
		
		try {
		Web_GeneralFunctions.sendkeys(MCP.getNoofDays_Inputbox(Login_Doctor.driver, logger), "alphabets", "Entering Alphabets", Login_Doctor.driver, logger);
		}
		catch (Exception e)
		{
			Assert.assertTrue(MCP.getNoofDays_Inputbox(Login_Doctor.driver, logger).getAttribute("type").contains("number"), "No of Days feild is a number type");
		}
		
		Web_GeneralFunctions.sendkeys(MCP.getNoofDays_Inputbox(Login_Doctor.driver, logger), noofDays, "Enter Number of Days", Login_Doctor.driver, logger);
		
		
		LocalDate currentdate = LocalDate.now();
		System.out.println("Current date: "+currentdate);
	      //Getting the current day
	      int currentDay = currentdate.getDayOfMonth();
	      System.out.println("Current day: "+currentDay);
	      //Getting the current month
	      Month currentMonth = currentdate.getMonth().plus(2);
	      System.out.println("Current month: "+currentMonth);
	      //getting the current year
	      int currentYear = currentdate.getYear();
	      System.out.println("Current Year: "+currentYear);
		
		/*
		 * DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy"); Calendar cal =
		 * Calendar.getInstance(); System.out.println(dateFormat.format(cal.getTime()));
		 * String datefor= dateFormat.format(cal.getTime());
		 */
		
	      try
	      {
	    	  Web_GeneralFunctions.click(MCP.getfromdatebutton(Login_Doctor.driver, logger), "clicking on from date", Login_Doctor.driver, logger);
			  Web_GeneralFunctions.wait(2);
			  this.clickFutureDate("December 2020", "15");
			  Web_GeneralFunctions.wait(2);
			  Web_GeneralFunctions.click(MCP.gettodatebutton(Login_Doctor.driver, logger), "Clicking on to date", Login_Doctor.driver, logger);
			  this.clickFutureDate("December 2020", "12");
			  Web_GeneralFunctions.wait(2);
	    	  	    	     	  
	      }
	      catch (Exception e)
	      {
	    	  
	    	  Assert.assertTrue(true, "Successfully not selected the date less than from date");
	    	  
	      }
	      
		  
		  
		  Web_GeneralFunctions.click(MCP.getfromdatebutton(Login_Doctor.driver, logger), "clicking on from date", Login_Doctor.driver, logger);
		  Web_GeneralFunctions.wait(2);
		  this.clickFutureDate("December 2020", "15");
		  Web_GeneralFunctions.wait(2);
		  Web_GeneralFunctions.click(MCP.gettodatebutton(Login_Doctor.driver, logger), "Clicking on to date", Login_Doctor.driver, logger);
		  this.clickFutureDate("December 2020", "17");
		  Web_GeneralFunctions.wait(2);
		  
		/*
		 * Web_GeneralFunctions.sendkeys(MCP.getFromDate_Inputbox(Login_Doctor.driver,
		 * logger), datefor, "Enter From Date", Login_Doctor.driver, logger);
		 * Web_GeneralFunctions.wait(5);
		 * Web_GeneralFunctions.sendkeys(MCP.getToDate_Inputbox(Login_Doctor.driver,
		 * logger), datefor, "Enter To Date", Login_Doctor.driver, logger);
		 */		//Web_GeneralFunctions.click(MCP.getCurrentDate(Login_Doctor.driver, logger), "Enter current date", Login_Doctor.driver, logger);
		
		Web_GeneralFunctions.click(MCP.getPriviewButton(Login_Doctor.driver, logger), "Click on preview button", Login_Doctor.driver, logger);	
		Web_GeneralFunctions.wait(3);
		
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, MCP.getdoctorvalidate(Login_Doctor.driver, logger, doctor));
		Assert.assertTrue(MCP.getdoctorvalidate(Login_Doctor.driver, logger, doctor).isDisplayed());
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, MCP.getpatientvalidate(Login_Doctor.driver, logger, patient));
		Assert.assertTrue(MCP.getpatientvalidate(Login_Doctor.driver, logger, patient).isDisplayed());
		
		Web_GeneralFunctions.click(MCP.getclosebuttonPreview(Login_Doctor.driver, logger), "Close Preview", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(MCP.getSavePrint_Button(Login_Doctor.driver, logger), "Click on Save and Print", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
	    //this.LeavePdfValidation();
		
		Web_GeneralFunctions.switchTabs("Switch to the previous TAB", 0, Login_Doctor.driver, logger);
		//Medical Fitness Certificate
		
		//Web_GeneralFunctions.wait(5);
		//Web_GeneralFunctions.click(MCP.getleftmainmenu(Login_Doctor.driver, logger), "Clicking on Humburger menu", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(5);
		Web_GeneralFunctions.click(MCP.getMedicalCertificateTab(Login_Doctor.driver, logger), "Click on Medical Certificate Tab", Login_Doctor.driver, logger);
		
		Web_GeneralFunctions.click(MCP.getPrintmedicalCertificatelink(Login_Doctor.driver, logger), "Clicking on Print medical certificate tab", Login_Doctor.driver, logger);
		Web_GeneralFunctions.selectElementByVisibleText(MCP.getCertificatetype_Selectbox(Login_Doctor.driver, logger), "Medical Fitness", "Clicking on the select box", Login_Doctor.driver, logger);
		Web_GeneralFunctions.selectElementByVisibleText(MCP.getSelectTemplate_Selectbox2(Login_Doctor.driver, logger), MedicalCertifcateTemplate, "Selcting the medical certificate template", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		
		try
		{
		Web_GeneralFunctions.sendkeys(MCP.gettemplate_textarea(Login_Doctor.driver, logger), "test textarea", "Entering the text to template", Login_Doctor.driver, logger);
		}
		catch (Exception e)
		{
        Assert.assertTrue(MCP.gettemplate_textarea(Login_Doctor.driver, logger).getAttribute("readonly").contains("true"), "The textarea is readOnly");	
		}
		
		Web_GeneralFunctions.click(MCP.getSavePrint_Button(Login_Doctor.driver, logger), "Click on Save and Print", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, MCP.getnotify_container(Login_Doctor.driver, logger));
		Web_GeneralFunctions.wait(2);
		Assert.assertTrue(MCP.getnotify_container(Login_Doctor.driver, logger).isDisplayed(), "Error Message Container displayed successfully");
		
		Web_GeneralFunctions.selectElementByVisibleText(MCP.getDoctor_Selectbox(Login_Doctor.driver, logger), doctor, "Select doctor", Login_Doctor.driver, logger);		
		Web_GeneralFunctions.selectElementByVisibleText(MCP.getTitle_Selectbox(Login_Doctor.driver, logger), "Mr", "Selcting tilte", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(MCP.getPatient_Inputbox(Login_Doctor.driver, logger), "s", "Enter keys", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(MCP.getPatient_Inputbox(Login_Doctor.driver, logger), "a", "Enter keys", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(MCP.getPatient_Inputbox(Login_Doctor.driver, logger), "m", "Enter keys", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		Web_GeneralFunctions.click(MCP.getPatient_selectlink(Login_Doctor.driver, logger), "Selecting the patient", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(MCP.getDiagonsis_Inputbox(Login_Doctor.driver, logger), "medical Diognaosis", "Enter Diagnosis", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(MCP.getPriviewButton(Login_Doctor.driver, logger), "Click on preview button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(5);
		
		Assert.assertTrue(MCP.getdoctorvalidate(Login_Doctor.driver, logger, doctor).isDisplayed());
		Assert.assertTrue(MCP.getpatientvalidate(Login_Doctor.driver, logger, patient).isDisplayed());
		
		Web_GeneralFunctions.click(MCP.getclosebuttonPreview(Login_Doctor.driver, logger), "Close Preview", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(MCP.getSavePrint_Button(Login_Doctor.driver, logger), "Click on Save and Print", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(5);
	}
	
	@Parameters({"patient","doctor", "UHID", "Phno"})	
	@Test(priority = 4, groups= {"Regression","Search_MedicalCert"})
	public synchronized void Search_MedicalCert(String patient, String doctor, String UHID, String Phno) throws Exception {

		logger = Reports.extent.createTest("Search Medical Certificate");
		
		Web_GeneralFunctions.switchTabs("Switch to the previous TAB", 0, Login_Doctor.driver, logger);
		
		//Web_GeneralFunctions.click(MCP.getleftmainmenu(Login_Doctor.driver, logger), "Clicking on Humburger menu", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(MCP.getMedicalCertificateTab(Login_Doctor.driver, logger), "Click on Medical Certificate Tab", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(MCP.getSearchMedicalCertificatelink(Login_Doctor.driver, logger), "Click on Search medical Certificate Tab", Login_Doctor.driver, logger);
		
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.sendkeys(MCP.getPatientName_Inputbox2(Login_Doctor.driver, logger), patient, "Enter patient name for search", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(MCP.getSearch_Button(Login_Doctor.driver, logger), "Clicking on Search button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(10);
		Web_GeneralFunctions.clear(MCP.getPatientName_Inputbox2(Login_Doctor.driver, logger), "Clear Patient Name", Login_Doctor.driver, logger);
		
		try
		{
			Web_GeneralFunctions.sendkeys(MCP.getUHID_Inputbox(Login_Doctor.driver, logger), "abcdef", "Enter alphabets", Login_Doctor.driver, logger);
		}
		catch (Exception e)
		{
			Assert.assertTrue(MCP.getUHID_Inputbox(Login_Doctor.driver, logger).getAttribute("type").contains("number"), "UHID Text box is type number");
		}
		
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.sendkeys(MCP.getUHID_Inputbox(Login_Doctor.driver, logger), UHID, "Enter UHID Number", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(MCP.getSearch_Button(Login_Doctor.driver, logger), "Clicking on Search button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(10);
		Web_GeneralFunctions.clear(MCP.getUHID_Inputbox(Login_Doctor.driver, logger), "Clear UHID", Login_Doctor.driver, logger);
		
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy"); Calendar cal =
				  Calendar.getInstance(); System.out.println(dateFormat.format(cal.getTime()));
				  String datefor= dateFormat.format(cal.getTime());
		
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.sendkeys(MCP.getFromDate_Inputbox2(Login_Doctor.driver, logger), datefor, "Enter From Date", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(MCP.getToDate_Inputbox2(Login_Doctor.driver, logger), datefor , "Enter To Date", Login_Doctor.driver, logger);
		
		Web_GeneralFunctions.click(MCP.getcertificate_selectbox(Login_Doctor.driver, logger), "Clicking on certificate", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(MCP.getcertificate_option1(Login_Doctor.driver, logger), "Click on the option1", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(MCP.getcertificate_option2(Login_Doctor.driver, logger), "Click on the option2", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(MCP.getSearch_Button(Login_Doctor.driver, logger), "Clicking on Search button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clear(MCP.getFromDate_Inputbox2(Login_Doctor.driver, logger), "Clearing date", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clear(MCP.getToDate_Inputbox2(Login_Doctor.driver, logger), "Clearing date", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(5);
		
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.sendkeys(MCP.getMobileNo_Inputbox(Login_Doctor.driver, logger), "987654321", "Enter mobile number less than 10 digits", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(MCP.getSearch_Button(Login_Doctor.driver, logger), "Clicking on Search button", Login_Doctor.driver, logger);
		Assert.assertTrue(MCP.geterror_message(Login_Doctor.driver, logger).isDisplayed(), "Error Message displayed successfully");
		
		Web_GeneralFunctions.clear(MCP.getMobileNo_Inputbox(Login_Doctor.driver, logger), "Clear Mobile no", Login_Doctor.driver, logger);
		
		Web_GeneralFunctions.sendkeys(MCP.getMobileNo_Inputbox(Login_Doctor.driver, logger), Phno, "Enter Mobile number", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(MCP.getSearch_Button(Login_Doctor.driver, logger), "Clicking on Search button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.clear(MCP.getMobileNo_Inputbox(Login_Doctor.driver, logger), "Clear Mobile no", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(5);

		Web_GeneralFunctions.click(MCP.getprint(Login_Doctor.driver, logger, patient), "Click on Print", Login_Doctor.driver, logger);				 
		
		Web_GeneralFunctions.wait(5);
		
	}
	
	
}
