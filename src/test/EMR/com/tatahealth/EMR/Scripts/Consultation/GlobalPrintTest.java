
package com.tatahealth.EMR.Scripts.Consultation;

import static org.testng.Assert.assertTrue;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.util.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Consultation.DiagnosisPage;
import com.tatahealth.EMR.pages.Consultation.GeneralAdvicePage;
import com.tatahealth.EMR.pages.Consultation.GlobalPrintPage;
import com.tatahealth.EMR.pages.Consultation.LabResultPage;
import com.tatahealth.EMR.pages.Consultation.PrescriptionPage;
import com.tatahealth.EMR.pages.Consultation.SymptomPage;
import com.tatahealth.EMR.pages.Consultation.VitalsPage;
import com.tatahealth.EMR.pages.PracticeManagement.PracticeManagementPages;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.DownloadPDF;
import com.tatahealth.ReusableModules.ReusableData;

public class GlobalPrintTest {

	@BeforeClass(alwaysRun=true,groups= {"Regression","Appointment_Doctor"})
	public static void beforeClass() throws Exception {
		ConsultationTest currentTest = new ConsultationTest();
		currentTest.loginAndStartConsultation("Global Print");
	}
	
	@AfterClass(groups= {"Regression","Appointment_Doctor"})
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		System.out.println("Closed Login_Doctor.driver");
	}
	
	public static String medicalKitName = "";
	ExtentTest logger;

	GlobalPrintPage gp = new GlobalPrintPage();
	SymptomTest smt = new SymptomTest();
	PrescriptionPage prescription = new PrescriptionPage();
	PrescriptionTest ptest = new PrescriptionTest();
	WebDriver driver;
	public List<String> results = new ArrayList<String>();
	String ga = "";

	public synchronized void moveToPrintModule() throws Exception {

		logger = Reports.extent.createTest("EMR Move To Print Module");
		Web_GeneralFunctions.click(gp.getGlobalPrintModule(Login_Doctor.driver, logger), "Move to print module",Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
	}

	public synchronized void printAll() throws Exception {

		logger = Reports.extent.createTest("EMR Click Print All Button");
		Web_GeneralFunctions.click(gp.getPrintAllButton(Login_Doctor.driver, logger), "Click Print All Button",
				Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(5);
	}

	public synchronized void print() throws Exception {

		logger = Reports.extent.createTest("EMR Click Print  Button");
		Web_GeneralFunctions.click(gp.getPrintButton(Login_Doctor.driver, logger), "Click Print  Button",Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(5);

	}

	public synchronized void saveMedicalKit() throws Exception {

		logger = Reports.extent.createTest("EMR Click Save as medical kit Button");
		Web_GeneralFunctions.click(gp.saveMedicalKit(Login_Doctor.driver, logger), "Click Save as medical kit  Button",Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
	}

	public synchronized void closeMedicalKitButton() throws Exception {

		logger = Reports.extent.createTest("EMR Click Save as medical kit Button");
		Web_GeneralFunctions.click(gp.getMedicalKitClosePopup(Login_Doctor.driver, logger),"Click  medical kit close  Button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
	}

	public synchronized void shareToFollowUp() throws Exception {

		logger = Reports.extent.createTest("EMR Click Share To Follow up  Button");
		Web_GeneralFunctions.click(gp.getShareToFollowUpButton(Login_Doctor.driver, logger),"Click Share to Follow Up  Button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(5);

		String xpath = "//div[@class='sweet-alert showSweetAlert visible']";
		boolean status = gp.getEmptyDiagnosisAlertPopUp(Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		 if(status == true) {
		Web_GeneralFunctions.click(gp.getEmptyDiagnosisConfirm(Login_Doctor.driver,logger), "Click confirm in empty diagnosis pop up", Login_Doctor.driver,logger); 
		Web_GeneralFunctions.wait(8);
		}
	}

	public synchronized void checkout() throws Exception {

		logger = Reports.extent.createTest("EMR Click check out  Button");
		Web_GeneralFunctions.click(gp.getCheckOutButton(Login_Doctor.driver, logger), "Click Check out  Button",
				Login_Doctor.driver, logger);
		Thread.sleep(3000);

	}

	public synchronized void clickViewSummary() throws Exception {

		logger = Reports.extent.createTest("EMR Click check out  Button");
		Web_GeneralFunctions.click(gp.getViewSummaryButton(Login_Doctor.driver, logger),
				"Click Check out view summary Button", Login_Doctor.driver, logger);
		Thread.sleep(5000);

	}
	
	public synchronized void clickCheckOutClose() throws Exception {

		logger = Reports.extent.createTest("EMR Click check out  Button");
		Web_GeneralFunctions.click(gp.getViewSummaryCloseButton(Login_Doctor.driver, logger),
				"Click Check out close Button", Login_Doctor.driver, logger);
		Thread.sleep(5000);

	}

	
	public synchronized String getPDFPage() throws Exception {

		logger = Reports.extent.createTest("EMR Move to Pdf page");

		String url = Web_GeneralFunctions.switchTabs("Move to new PDF tab", 1, Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		System.out.println("pdf : " + url);

		DownloadPDF pdf = new DownloadPDF();
		String filename = pdf.pdfDownload(url);

		String pdfContent = pdf.verifyPDFContent(filename);

		Web_GeneralFunctions.closeDriver(Login_Doctor.driver);
		url = Web_GeneralFunctions.switchTabs("Move to application tab", 0, Login_Doctor.driver, logger);

		return pdfContent;

	}

	// @Test(groups= {"Regression","Login"},priority=15)

	@Test(groups = { "Regression", "Login" }, priority = 821)
	public synchronized void DivValidation() throws Exception {

		logger = Reports.extent.createTest("EMR deselect the selected div");
		moveToPrintModule();

		try {

			Assert.isTrue(gp.consultationSectionsCaseSheet(Login_Doctor.driver, logger).isDisplayed(),"case sheet is not preset");
			Assert.isTrue(gp.consultationSectionsGA(Login_Doctor.driver, logger).isDisplayed(), "GA is not preset");
			Assert.isTrue(gp.consultationSectionsVitals(Login_Doctor.driver, logger).isDisplayed(),"Vital is not preset");
			Assert.isTrue(gp.consultationSectionsDiagnosis(Login_Doctor.driver, logger).isDisplayed(),"Diagnosis is not preset");
			Assert.isTrue(gp.consultationSectionsFollowUp(Login_Doctor.driver, logger).isDisplayed(),"FollowUp is not preset");
			Assert.isTrue(gp.consultationSectionsLabTests(Login_Doctor.driver, logger).isDisplayed(),"LabTest is not preset");
			Assert.isTrue(gp.consultationSectionsMedicalHistory(Login_Doctor.driver, logger).isDisplayed(),"medical histoty is not preset");
			Assert.isTrue(gp.consultationSectionsReferral(Login_Doctor.driver, logger).isDisplayed()," Referal is not preset");

		} catch (Exception e) {
			System.out.println(e);
		}

	}

	@Test(groups = { "Regression", "Login" }, priority = 822)
	public synchronized void saveExistingMedicalKitPresentInMedicalKitModule() throws Exception {
		Web_GeneralFunctions.click(gp.moveToSymptomModule(Login_Doctor.driver, logger), "move to symptom module",
				Login_Doctor.driver, logger);
		logger = Reports.extent.createTest("EMR saved medical kit present in medical kit module");
		smt.moveToSymtpomModule();
		Web_GeneralFunctions.wait(1);

		Web_GeneralFunctions.click(gp.getMedicalKitFromDropDown("GeneralAdviceKit", Login_Doctor.driver, logger),"Get medical kit name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(6);
		Web_GeneralFunctions.click(gp.getAppyMedicalKitButton(Login_Doctor.driver, logger), "Click on appy kit button",Login_Doctor.driver, logger);

	}

	@Test(groups = { "Regression", "Login" }, priority = 823)
	public synchronized void getAllresult() throws Exception {
		smt.moveToSymtpomModule();
		SymptomPage sp = new SymptomPage();
		VitalsPage vPages = new VitalsPage();
		VitalsTest vTest = new VitalsTest();
		DiagnosisPage dp = new DiagnosisPage();
		DiagnosisTest dt = new DiagnosisTest();
		PrescriptionTest pt = new PrescriptionTest();
		PrescriptionPage pg = new PrescriptionPage();
		GeneralAdviceTest gat = new GeneralAdviceTest();
		GeneralAdvicePage gap = new GeneralAdvicePage();

		//Web_GeneralFunctions.click(sp.getSymptom(Login_Doctor.driver, 1, logger),"Click on Symptom", driver, logger);
		String symptom = Web_GeneralFunctions.getAttribute(sp.getSymptom(Login_Doctor.driver, 0, logger), "value","get Symptom", driver, logger);
		results.add(symptom);

		vTest.moveToVitalModule();
		Thread.sleep(500);

		String weightText = Web_GeneralFunctions.getAttribute(vPages.getWeightElement(Login_Doctor.driver, logger),"value", "get weight field", Login_Doctor.driver, logger);
		Thread.sleep(100);
		results.add(weightText);

		String heightText = Web_GeneralFunctions.getAttribute(vPages.getHeightElement(Login_Doctor.driver, logger),"value", "get height field", Login_Doctor.driver, logger);
		Thread.sleep(100);
		results.add(weightText);

		/*
		 * dt.moveToDiagnosisModule(); Thread.sleep(500); String Diagnosis=
		 * dp.getDiagnosisTextField(0, Login_Doctor.driver, logger).getText();
		 * results.add(Diagnosis);
		 */

		pt.moveToPrescriptionModule();
		String prescription = Web_GeneralFunctions.getText(pg.getDrugName(0, Login_Doctor.driver, logger),"geting drug name", Login_Doctor.driver, logger);
		results.add(prescription);
		gat.moveToGeneralAdvice();
		ga = Web_GeneralFunctions.getText(gap.getGeneralAdviceTextArea(Login_Doctor.driver, logger), "geting drug name",Login_Doctor.driver, logger);
		results.add(ga);

		System.out.println(results);
	}

	@Test(groups = { "Regression", "Login" }, priority = 824)
	public synchronized void printAllResult() throws Exception {
		boolean status = false;
		logger = Reports.extent.createTest("EMR pdf verification");
		LabResultPage labResult = new LabResultPage();
		GlobalPrintTest gp = new GlobalPrintTest();
		moveToPrintModule();
		Thread.sleep(500);
		printAll();
		String pdfContent = gp.getPDFPage();
		System.out.println("pdf content : " + pdfContent);
		System.out.println("lab result size : " + results.size());
		int i = 0;
		while (i < results.size()) {
			if (pdfContent.contains(results.get(i))) {
				status = true;
			} else {
				status = false;
				break;
			}
			i++;
		}

		if (status == true) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}

		Web_GeneralFunctions.wait(2);
		System.out.println("PDF validation for saved values");
	}

	public synchronized void readCheckOutPdf() throws Exception {
		boolean status = false;
		logger = Reports.extent.createTest("EMR pdf verification");
		LabResultPage labResult = new LabResultPage();
		GlobalPrintTest gp = new GlobalPrintTest();
		moveToPrintModule();
		Thread.sleep(500);
		String pdfContent = gp.getPDFPage();
		System.out.println("pdf content : " + pdfContent);
		System.out.println("lab result size : " + results.size());
		int i = 0;
		System.out.println();
		while (i < results.size()) {
			if (pdfContent.contains(results.get(i))) {
				status = true;
			} else {
				status = false;
				break;
			}
			i++;
		}

		if (status == true) {
			assertTrue(true);
		} else {
					assertTrue(false);
		}

		Web_GeneralFunctions.wait(2);
		System.out.println("PDF validation for saved values");
	}
	
	  @Test(groups = { "Regression", "Login" }, priority = 825) public synchronized
	  void uncheckAllDiv() throws Exception {
	  
	  logger = Reports.extent.createTest("EMR deselect the selected div");
	  moveToPrintModule();
	  
	  
	  try {
	  
	  
	  gp.consultationSectionsCaseSheet(Login_Doctor.driver,logger).click();
	  //gp.consultationSectionsGA(Login_Doctor.driver, logger).click();
	  gp.consultationSectionsVitals(Login_Doctor.driver, logger).click();
	  gp.consultationSectionsDiagnosis(Login_Doctor.driver, logger).click();
	  gp.consultationSectionsFollowUp(Login_Doctor.driver, logger).click();
	  gp.consultationSectionsLabTests(Login_Doctor.driver, logger).click();
	  gp.consultationSectionsMedicalHistory(Login_Doctor.driver, logger).click();
	  gp.consultationSectionsReferral(Login_Doctor.driver, logger).click();
	  gp.consultationSectionsSymptoms(Login_Doctor.driver, logger).click();
	  gp.consultationSectionsPrescription(Login_Doctor.driver, logger).click();
	  
	  Web_GeneralFunctions.click(gp.consultationSectionsGA(Login_Doctor.driver,logger), "Deselect the ga",Login_Doctor.driver, logger);
	  }
	  catch (Exception e) { System.out.println(e); }
	  
	  }
	  
	  @Test(groups = { "Regression", "Login" }, priority = 826) 
	  public synchronized void clickOnPrintWithOutSelctingAnyDiv() throws Exception {
	  
	  logger = Reports.extent.createTest("EMR clickOnPrintWithOutSelctingAnyDiv");
	  moveToPrintModule();
	  
	  
	  try {
	  
	  print(); 
	  Thread.sleep(100);
	  
	  } catch (Exception e) { System.out.println(e); }
	  
	  }
	  
	  @Test(groups = { "Regression", "Login" }, priority = 827)
	  public synchronized void clickOnSaveAsDefaultWithOutSelctingAnyDiv() throws Exception {
	  
	  logger = Reports.extent.createTest("EMR clickOnPrintWithOutSelctingAnyDiv");
	  moveToPrintModule();
 
	  try {
	  
		  Web_GeneralFunctions.click(gp.setAsDefaultButton(Login_Doctor.driver, logger), "click set as default button",Login_Doctor.driver, logger);
		  Web_GeneralFunctions.wait(1);
		  String text = Web_GeneralFunctions.getText(gp.getMessage(Login_Doctor.driver, logger), "Get success message",Login_Doctor.driver, logger);
		  Web_GeneralFunctions.wait(1);
		  if(text.equalsIgnoreCase("Please select atleast one section to set as default")) {
				assertTrue(true);
			} else {
				assertTrue(false);
			}	  
	  Web_GeneralFunctions.click(gp.consultationSectionsLabTests(Login_Doctor.driver,logger), "Deselect the ga",Login_Doctor.driver, logger); }
	  catch (Exception e) {
	  	e.printStackTrace();
	  	}
	  
	  }
	  
	  @Test(groups= {"Regression","Login"},priority=828) 
	  public synchronized void printAllResultAfterAllDivUnchecked()throws Exception{
		  boolean status = false;
	  logger = Reports.extent.createTest("EMR pdf verification"); 
	  LabResultPage labResult = new LabResultPage(); 
	  GlobalPrintTest gp = new GlobalPrintTest();
	  
	  moveToPrintModule();
	  Thread.sleep(500);
	  printAll(); 
	  String pdfContent =gp.getPDFPage(); 
	  System.out.println("pdf content : "+pdfContent);
	  System.out.println("lab result size : "+results.size()); 
	  int i =0;
	  System.out.println(); 
	  while(i<results.size()) {
	  if(pdfContent.contains(results.get(i)))
	  { status = true; }
	  else { status =
	  false; break; } i++; }
	  
	  if(status == true) { assertTrue(true); }else { assertTrue(false); }
	  
	  Thread.sleep(2000); System.out.println("PDF validation for saved values"); }
	  
	  @Test(groups = { "Regression", "Login" }, priority = 829)
	  public synchronized void verifyPrintPDF() throws Exception {
	  
	  logger = Reports.extent.createTest("EMR print with div");
	  
	  moveToPrintModule();
	  //Web_GeneralFunctions.click(gp.consultationSectionsGA(Login_Doctor.driver,logger), "Select the ga",Login_Doctor.driver, logger);
		 
	  print(); 
	  String pdfContent = getPDFPage();
	  
	  // if (pdfContent.contains("General Advice")||pdfContent. contains("General \\nAdvice"))
	/*  
	if (pdfContent.contains(ga)) 
	{
	  assertTrue(true); } else { assertTrue(false); } Thread.sleep(500); 
	  }*/
	  
	  if(pdfContent.contains("Lab Tests")) {
		  assertTrue(true); } else { assertTrue(false); } Thread.sleep(500); 
	  }
	  
	  @Test(groups = { "Regression", "Login" }, priority = 830) public
	  synchronized void validateShareToFollowUp() throws Exception {
		  ConsultationTest consult = new ConsultationTest();

	  logger = Reports.extent.createTest("EMR ShareToFollowUP PDF validation");
	  moveToPrintModule();
 	  shareToFollowUp(); 
	  readCheckOutPdf();
	  consult.openFollowUpConsultation();	 
	  Web_GeneralFunctions.wait(2);
	      moveToPrintModule();
	  }
	  
	 
	
	
	//.........................................................................................
	// @Test(groups = { "Regression", "Login" }, priority = 936)
	public synchronized void filldata() throws Exception {
		int row = 0;
		logger = Reports.extent.createTest("EMR fill sysmtoms");
		try {
			DiagnosisPage diagnosis = new DiagnosisPage();
			// filled data for diagnosis
			Web_GeneralFunctions.click(diagnosis.moveToDiagnosisModule(Login_Doctor.driver, logger),
					"Move to diagnosis module", Login_Doctor.driver, logger);
			Thread.sleep(1000);
			WebElement element = diagnosis.selectDiagnosisFromDropDown(Login_Doctor.driver, row, "", logger);
			Web_GeneralFunctions.click(element, "select diagnosis from drop down", Login_Doctor.driver, logger);

			// move to general medicine
			GeneralAdvicePage gap = new GeneralAdvicePage();
			Web_GeneralFunctions.click(gap.getGeneralAdvice(Login_Doctor.driver, logger),
					"Move to general advice module", Login_Doctor.driver, logger);
			Thread.sleep(1000);

		} catch (Exception e) {
			// TODO: handle exception
		}
		Thread.sleep(1000);

	}

	// @Test(groups= {"Regression","Login"},priority=944)
	public synchronized void deselectDiv() throws Exception {

		logger = Reports.extent.createTest("EMR deselect the selected div");
		// moveToPrintModule();
		Web_GeneralFunctions.click(gp.consultationSectionsGA(Login_Doctor.driver, logger), "Deselect the ga",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);

	}

	// @Test(groups= {"Regression","Login"},priority=932)
	// depends on saveGeneralAdvice // nedd to run after general advice module
	public synchronized void deselectElementPresentInPrintAllPDF() throws Exception {

		logger = Reports.extent.createTest("EMR Print all pdf - deselected module present in pdf");
		printAll();
		String pdfContent = getPDFPage();

		if (pdfContent.contains("General Advice")) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
		Thread.sleep(1000);

	}

	// @Test(groups= {"Regression","Login"},priority=933)
	public synchronized void deselectElementPresentInPrintPDF() throws Exception {

		logger = Reports.extent.createTest("EMR Print  pdf - deselected module present in pdf");
		print();
		String pdfContent = getPDFPage();

		if (pdfContent.contains("General Advice")) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
		Thread.sleep(1000);

	}

	// @Test(groups= {"Regression","Login"},priority=934)
	public synchronized void selectDeselectElement() throws Exception {

		logger = Reports.extent.createTest("EMR Print  pdf - select deselected module present in pdf");
		Web_GeneralFunctions.click(gp.consultationSectionsGA(Login_Doctor.driver, logger), "Select deselected element",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);

		System.out.println("************************ Global Print Start ****************************************");
	}

	// @Test(groups= {"Regression","Login"},priority=935)
	public synchronized void setAsDefault() throws Exception {

		logger = Reports.extent.createTest("EMR set as default");
		Web_GeneralFunctions.click(gp.setAsDefaultButton(Login_Doctor.driver, logger), "click set as default button",Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(1);
		String text = Web_GeneralFunctions.getText(gp.getMessage(Login_Doctor.driver, logger), "Get success message",Login_Doctor.driver, logger);
		if (text.equalsIgnoreCase("Default Global print section saved successfully")) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}
	}

	/*
	 * @Test(groups= {"Regression","Login"},priority=936) public synchronized
	 * voidclickMedicalKit()throws Exception{
	 * 
	 * logger = Reports.extent.createTest("EMR save as medical kit");
	 * Web_GeneralFunctions.click(gp.getSaveAsMedicalKit(Login_Doctor.driver,
	 * logger), "Click save as medical kit", Login_Doctor.driver, logger);
	 * Thread.sleep(1000); }
	 * 
	 * @Test(groups= {"Regression","Login"},priority=937) public synchronized void
	 * saveMedicalKitWithoutName()throws Exception{
	 * 
	 * logger = Reports.extent.createTest("EMR save  medical kit without kit name");
	 * saveMedicalKit(); String message =
	 * Web_GeneralFunctions.getText(gp.getMessage(Login_Doctor.driver, logger),
	 * "Get warning message", Login_Doctor.driver, logger); Thread.sleep(1000);
	 * System.out.println("message : "+message);
	 * if(message.equalsIgnoreCase("Please enter medical kit name")) {
	 * assertTrue(true); }else { assertTrue(false); } Thread.sleep(1000); }
	 * 
	 * @Test(groups= {"Regression","Login"},priority=938) public synchronized void
	 * saveMedicalKitWithProperValue()throws Exception{
	 * 
	 * logger = Reports.extent.createTest("EMR  medical kit with proper values"); //
	 * smt.Consultation_001(); //smt.Consultation_002();
	 * 
	 * medicalKitName = RandomStringUtils.randomAlphabetic(10);
	 * Web_GeneralFunctions.sendkeys(gp.getMedicalKitName(Login_Doctor.driver,
	 * logger), medicalKitName, "Sending medical kit value", Login_Doctor.driver,
	 * logger); Thread.sleep(1000); saveMedicalKit(); String message =
	 * Web_GeneralFunctions.getText(gp.getMessage(Login_Doctor.driver, logger),
	 * "Get success message", Login_Doctor.driver, logger); Thread.sleep(1000);
	 * if(message.equalsIgnoreCase("Medical kit created successfully.")) {
	 * assertTrue(true); }else { assertTrue(false); } Thread.sleep(1000); }
	 * 
	 * @Test(groups= {"Regression","Login"},priority=939) public synchronized void
	 * saveMedicalKitWithDuplicateName()throws Exception{
	 * 
	 * logger =
	 * Reports.extent.createTest("EMR  medical kit with duplicate kit name");
	 * clickMedicalKit();
	 * 
	 * Web_GeneralFunctions.sendkeys(gp.getMedicalKitName(Login_Doctor.driver,
	 * logger), medicalKitName, "sending duplicate value", Login_Doctor.driver,
	 * logger); Thread.sleep(1000); saveMedicalKit(); String text =
	 * Web_GeneralFunctions.getText(gp.getMessage(Login_Doctor.driver, logger),
	 * "Get duplicate alert", Login_Doctor.driver, logger); Thread.sleep(1000);
	 * if(text.equalsIgnoreCase("Medical kit name already exists.")) {
	 * assertTrue(true); }else { assertTrue(false); } Thread.sleep(1000);
	 * closeMedicalKitButton(); Thread.sleep(1000); }
	 * 
	 * @Test(groups= {"Regression","Login"},priority=940) public synchronized void
	 * savedMedicalKitPresentInMedicalKitModule()throws Exception{
	 * 
	 * logger = Reports.extent.
	 * createTest("EMR saved medical kit present in medical kit module");
	 * Web_GeneralFunctions.click(gp.moveToSymptomModule(Login_Doctor.driver,
	 * logger), "move to symptom module", Login_Doctor.driver, logger);
	 * Thread.sleep(1000); String text =
	 * Web_GeneralFunctions.getText(gp.getMedicalKitFromDropDown(medicalKitName,
	 * Login_Doctor.driver, logger), "Get medical kit name", Login_Doctor.driver,
	 * logger); Thread.sleep(1000); if(text.equalsIgnoreCase(medicalKitName)) {
	 * assertTrue(true); }else { assertTrue(false); } Thread.sleep(1000);
	 * 
	 * }
	 */

	// @Test(groups= {"Regression","Login"},priority=941)
	public synchronized void printAllPDF() throws Exception {
		boolean prescriptionStatus = false;
		logger = Reports.extent.createTest("EMR Prescription Print All PDF");

		/*
		 * ptest.moveToPrescriptionModule();
		 * Web_GeneralFunctions.click(prescription.getDrugName(1, Login_Doctor.driver,
		 * logger), "Focus on 1st row", Login_Doctor.driver, logger);
		 * Thread.sleep(1000);
		 */
		List<String> med = PrescriptionTest.medicine;
		System.out.println("medicine list attribute : " + med);
		String medi = "";
		int j = 0;
		while (j < med.size()) {

			medi += med.get(j) + " ";
			j++;
		}

		System.out.println("medicine list in : " + medi);
		// savedDiagnosis = diagnosis.getDiagnosisData(row, Login_Doctor.driver,
		// logger);
		moveToPrintModule();
		System.out.println("moveToPrintModule done");
		printAll();
		String pdfContent = getPDFPage();

		System.out.println("pdf content : " + pdfContent);
		int medicineStart = pdfContent.indexOf("Prescription:");
		int medicineEnd = pdfContent.indexOf("General Advice:");

		String medicineFromPDF = pdfContent.substring(medicineStart, medicineEnd);

		medicineStart = medicineFromPDF.indexOf("Instructions");
		medicineFromPDF = medicineFromPDF.substring(medicineStart);
		System.out.println("medicine from pdf : " + medicineFromPDF);
		String[] mediArr = medicineFromPDF.split(" ");
		String medValue = "";
		int i = 0;
		System.out.println("medicine array : " + mediArr);
		// Integer j = 1;
		List<String> liMedi = new ArrayList<String>();
		while (i < mediArr.length) {

			liMedi.add(mediArr[i]);
			// medValue += mediArr[i]+" ";
			i++;
		}

		System.out.println("list medicine : " + liMedi);
		// System.out.println("medvalue : "+medValue);
		/*
		 * if(pdfContent.contains("Prescription:")) {
		 * 
		 * int i =0; while(i<medicine.size()) {
		 * System.out.println("medicine print all: "+medicine.get(i));
		 * if(pdfContent.contains(medicine.get(i))) { prescriptionStatus = true; }else {
		 * prescriptionStatus = false; break; } i++; }
		 * 
		 * 
		 * }else { prescriptionStatus = false; }
		 * 
		 * if(prescriptionStatus == true) { assertTrue(true); }else { assertTrue(false);
		 * }
		 */
		Thread.sleep(2000);
	}

	// @Test(groups= {"Regression","Login"},priority=942)
	public synchronized void shareToFollowUpPDFVerification() throws Exception {
		boolean symptomStatus = false;
		boolean referralStatus = false;
		boolean followUpStatus = false;
		boolean generalAdviceStatus = false;
		boolean diagnosisStatus = false;
		boolean vitalStatus = false;
		boolean labTestStatus = false;

		PracticeManagementPages pmp = new PracticeManagementPages();
		logger = Reports.extent.createTest("EMR Soft check out PDF Check");

		int i = 0;
		moveToPrintModule();
		shareToFollowUp();
		String pdfContent = getPDFPage();

		// System.out.println("share to follow up pdf : "+pdfContent);

		List<String> consultationArr = ReusableData.savedConsultationData.get("Symptoms");
		// System.out.println("consultation array size : "+consultationArr.size()+ "
		// "+consultationArr);
		if (pdfContent.contains("Symptoms")) {
			// int i =0;
			while (i < consultationArr.size()) {
				// System.out.println("share to follow up : "+consultationArr.get(i));
				if (pdfContent.contains(consultationArr.get(i))) {
					symptomStatus = true;
				} else {
					symptomStatus = false;
					break;
				}
				i++;
			}

		} else {
			symptomStatus = false;
		}

		consultationArr = ReusableData.savedConsultationData.get("Diagnosis");
		// if(pdfContent.contains(""))
		System.out.println("consultation array diagnosis : " + consultationArr);
		i = 0;
		String diagnosisCheck = "";
		if (pdfContent.contains("Diagnosis")) {
			while (i < consultationArr.size()) {
				diagnosisCheck = consultationArr.get(i);
				// System.out.println("diagnosis check : "+diagnosisCheck);
				if (pdfContent.contains(diagnosisCheck)) {
					diagnosisStatus = true;
				} else {
					diagnosisStatus = false;
					break;
				}
				i++;
			}
		} else {
			diagnosisStatus = false;
		}
		System.out.println("diagnosis status : " + diagnosisStatus);

		consultationArr = ReusableData.savedConsultationData.get("Referral");
		// System.out.println("consultation arr referral : "+consultationArr.get(0));
		if (pdfContent.contains("Referral")) {
			String refer = consultationArr.get(0);
			// System.out.println("share to follow up referral : "+ refer);
			if (pdfContent.contains(refer)) {
				referralStatus = true;
			} else {
				referralStatus = false;
				// break;
			}

		} else {
			referralStatus = false;
		}

		consultationArr = ReusableData.savedConsultationData.get("FollowUp");
		// System.out.println("consultation arr follow up : "+consultationArr.get(0));
		if (pdfContent.contains("Follow up")) {
			String follow = consultationArr.get(0);
			// System.out.println("share to follow up Follow Up : "+ follow);
			if (pdfContent.contains(follow)) {
				followUpStatus = true;
			} else {
				followUpStatus = false;
				// break;
			}

		} else {
			followUpStatus = false;
		}

		consultationArr = ReusableData.savedConsultationData.get("GeneralAdvice");

		if (pdfContent.contains("General Advice")) {
			String ga = consultationArr.get(0);
			// System.out.println("consultation arr ga : "+consultationArr.get(0));
			// ga = ga.substring(0, 1).toUpperCase()+ga.substring(1);
			if (pdfContent.contains(ga)) {
				generalAdviceStatus = true;
			} else {
				generalAdviceStatus = false;
			}

		} else {
			generalAdviceStatus = false;
		}

		consultationArr = ReusableData.savedConsultationData.get("Vitals");
		// System.out.println("saved vitals : "+consultationArr);
		if (pdfContent.contains("Vitals")) {
			i = 0;
			String value = "";
			while (i < consultationArr.size()) {
				value = consultationArr.get(i);
				// System.out.println("value of vitals : "+value);
				if (pdfContent.contains(value)) {
					vitalStatus = true;
				} else {
					vitalStatus = false;
					break;
				}
				i++;
			}

		} else {
			vitalStatus = false;
		}

		consultationArr = ReusableData.savedConsultationData.get("LabTest");
		// System.out.println("saved lab test : "+consultationArr);
		if (pdfContent.contains("Lab Tests")) {
			i = 0;
			while (i < consultationArr.size()) {
				// System.out.println("lab test : "+consultationArr.get(i));
				String searchTestNamePDF = consultationArr.get(i);
				if (pdfContent.contains(searchTestNamePDF)) {
					labTestStatus = true;
				} else {
					labTestStatus = false;
					break;
				}
				i++;
			}

		} else {
			labTestStatus = false;
		}

		if (symptomStatus == true && referralStatus == true && followUpStatus == true && generalAdviceStatus == true
				&& vitalStatus == true && labTestStatus == true && diagnosisStatus == true) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}

		Thread.sleep(2000);

		Web_GeneralFunctions.click(pmp.getOrgHeader(Login_Doctor.driver, logger), "click org header",
				Login_Doctor.driver, logger);
		Thread.sleep(3000);
	}
	
	

//	@Test(groups= {"Regression","Login"},priority=943)
	public synchronized void checkedOutPDFVerification() throws Exception {
		boolean symptomStatus = false;
		boolean referralStatus = false;
		boolean followUpStatus = false;
		boolean generalAdviceStatus = false;
		boolean diagnosisStatus = false;
		boolean vitalStatus = false;
		boolean labTestStatus = false;

		PracticeManagementPages pmp = new PracticeManagementPages();
		logger = Reports.extent.createTest("EMR  check out PDF Check");

		ConsultationTest consult = new ConsultationTest();
		consult.openFollowUpConsultation();

		int i = 0;
		moveToPrintModule();
		checkout();
		clickViewSummary();
		String pdfContent = getPDFPage();

		// System.out.println("share to checked out pdf : "+pdfContent);

		List<String> consultationArr = ReusableData.savedConsultationData.get("Symptoms");
		// System.out.println("consultation array size : "+consultationArr.size()+ "
		// "+consultationArr);
		if (pdfContent.contains("Symptoms")) {
			// int i =0;
			while (i < consultationArr.size()) {
				// System.out.println("share to follow up : "+consultationArr.get(i));
				if (pdfContent.contains(consultationArr.get(i))) {
					symptomStatus = true;
				} else {
					symptomStatus = false;
					break;
				}
				i++;
			}

		} else {
			symptomStatus = false;
		}

		consultationArr = ReusableData.savedConsultationData.get("Diagnosis");
		// if(pdfContent.contains(""))
		System.out.println("consultation array diagnosis : " + consultationArr);
		i = 0;
		String diagnosisCheck = "";
		if (pdfContent.contains("Diagnosis")) {
			while (i < consultationArr.size()) {
				diagnosisCheck = consultationArr.get(i);
				// System.out.println("diagnosis check : "+diagnosisCheck);
				if (pdfContent.contains(diagnosisCheck)) {
					diagnosisStatus = true;
				} else {
					diagnosisStatus = false;
					break;
				}
				i++;
			}
		} else {
			diagnosisStatus = false;
		}

		consultationArr = ReusableData.savedConsultationData.get("Referral");
		// System.out.println("consultation arr referral : "+consultationArr.get(0));
		if (pdfContent.contains("Referral")) {
			String refer = consultationArr.get(0);
			// System.out.println("share to follow up referral : "+ refer);
			if (pdfContent.contains(refer)) {
				referralStatus = true;
			} else {
				referralStatus = false;
				// break;
			}

		} else {
			referralStatus = false;
		}

		consultationArr = ReusableData.savedConsultationData.get("FollowUp");
		// System.out.println("consultation arr follow up : "+consultationArr.get(0));
		if (pdfContent.contains("Follow up")) {
			String follow = consultationArr.get(0);
			// System.out.println("share to follow up Follow Up : "+ follow);
			if (pdfContent.contains(follow)) {
				followUpStatus = true;
			} else {
				followUpStatus = false;
				// break;
			}

		} else {
			followUpStatus = false;
		}

		consultationArr = ReusableData.savedConsultationData.get("GeneralAdvice");

		if (pdfContent.contains("General Advice")) {
			String ga = consultationArr.get(0);
			// System.out.println("consultation arr ga : "+consultationArr.get(0));
			// ga = ga.substring(0, 1).toUpperCase()+ga.substring(1);
			if (pdfContent.contains(ga)) {
				generalAdviceStatus = true;
			} else {
				generalAdviceStatus = false;
			}

		} else {
			generalAdviceStatus = false;
		}

		consultationArr = ReusableData.savedConsultationData.get("Vitals");
		// System.out.println("saved vitals : "+consultationArr);
		if (pdfContent.contains("Vitals")) {
			i = 0;
			String value = "";
			while (i < consultationArr.size()) {
				value = consultationArr.get(i);
				// System.out.println("value of vitals : "+value);
				if (pdfContent.contains(value)) {
					vitalStatus = true;
				} else {
					vitalStatus = false;
					break;
				}
				i++;
			}

		} else {
			vitalStatus = false;
		}

		consultationArr = ReusableData.savedConsultationData.get("LabTest");
		// System.out.println("saved lab test : "+consultationArr);
		if (pdfContent.contains("Lab Tests")) {
			i = 0;
			while (i < consultationArr.size()) {
				// System.out.println("lab test : "+consultationArr.get(i));
				String searchTestNamePDF = consultationArr.get(i);
				if (pdfContent.contains(searchTestNamePDF)) {
					labTestStatus = true;
				} else {
					labTestStatus = false;
					break;
				}
				i++;
			}

		} else {
			labTestStatus = false;
		}

		if (symptomStatus == true && referralStatus == true && followUpStatus == true && generalAdviceStatus == true
				&& vitalStatus == true && labTestStatus == true && diagnosisStatus == true) {
			assertTrue(true);
		} else {
			assertTrue(false);
		}

		Thread.sleep(2000);

		Web_GeneralFunctions.click(pmp.getOrgHeader(Login_Doctor.driver, logger), "click org header",
				Login_Doctor.driver, logger);
		Thread.sleep(3000);
		System.out.println("************************ Global Print ends ****************************************");

	}

}
