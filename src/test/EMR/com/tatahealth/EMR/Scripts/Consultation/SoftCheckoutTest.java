package com.tatahealth.EMR.Scripts.Consultation;
import static org.testng.Assert.assertTrue;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.API.libraries.SheetsAPI;
import com.tatahealth.DB.Scripts.OTP;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.Scripts.PracticeManagement.PracticeManagementTest;
import com.tatahealth.EMR.pages.AppointmentDashboard.AllSlotsPage;
import com.tatahealth.EMR.pages.AppointmentDashboard.AppointmentsPage;
import com.tatahealth.EMR.pages.AppointmentDashboard.OpenVisitsPage;
import com.tatahealth.EMR.pages.Billing.Billing;
import com.tatahealth.EMR.pages.Consultation.ConsultationPage;
import com.tatahealth.EMR.pages.Consultation.DiagnosisPage;
import com.tatahealth.EMR.pages.Consultation.GeneralAdvicePage;
import com.tatahealth.EMR.pages.Consultation.GlobalPrintPage;
import com.tatahealth.EMR.pages.Consultation.LabResultPage;
import com.tatahealth.EMR.pages.Consultation.LabTestPage;
import com.tatahealth.EMR.pages.Consultation.PrescriptionPage;
import com.tatahealth.EMR.pages.Consultation.ReferralPage;
import com.tatahealth.EMR.pages.Consultation.SymptomPage;
import com.tatahealth.EMR.pages.Consultation.VitalsPage;
import com.tatahealth.EMR.pages.OpenConsultations.OpenConsultationsPage;
import com.tatahealth.EMR.pages.PracticeManagement.MedicalKitPages;
import com.tatahealth.EMR.pages.PracticeManagement.PracticeManagementPages;
import com.tatahealth.EMR.pages.RegisterPatient.PatientRegisterPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.DownloadPDF;
import com.tatahealth.ReusableModules.Web_Testbase;

public class SoftCheckoutTest {
	public static String labTestName ="";
	public static String diagnosisName ="";
	public static String medicineName ="";
	public static String pdfSummaryContent ="";
	public static String openConsultationDateTime="";
	public static String symptomName ="";
	public static String caseSheetDetails = "";
	public static String generalAdviceText="";
	public static String newPatientId="";
	public static String vitalHeight ="";
	public static String vitalWeight ="";
	public static String vitalBMI  ="";
	
	PracticeManagementPages pm = new PracticeManagementPages();
	OpenConsultationsPage ocp = new OpenConsultationsPage();
	OpenVisitsPage ovp = new OpenVisitsPage();
	PracticeManagementTest pmt = new PracticeManagementTest();
	LabTestPage ltp = new LabTestPage();
	GlobalPrintPage gpp = new GlobalPrintPage();
	ReferralPage rp = new ReferralPage();
	Billing bp = new Billing();
	DiagnosisPage dp = new DiagnosisPage();
	SymptomPage sp = new SymptomPage();
	LabResultPage lrp = new LabResultPage();
	AppointmentsPage ap = new AppointmentsPage();
	PrescriptionPage pp = new PrescriptionPage();
	MedicalKitPages mk = new MedicalKitPages();
	PrescriptionTest pt = new PrescriptionTest();
	GlobalPrintTest gpt = new GlobalPrintTest();
	ConsultationPage cp = new ConsultationPage();
	GeneralAdviceTest gat = new GeneralAdviceTest();
	GeneralAdvicePage gap = new GeneralAdvicePage();
	PatientRegisterPage prp = new PatientRegisterPage();
	VitalsPage vp = new VitalsPage();
	ExtentTest logger;
	
	
	
	@BeforeClass(alwaysRun=true,groups= {"Regression"})
	public static void beforeClass() throws Exception {
		Login_Doctor.executionName="EMR SoftCheckout Validation";
		Login_Doctor.LoginTest();
		
	} 
	
	
	@AfterClass(alwaysRun=true,groups = { "Regression"})
	public static void afterClass(){
		Login_Doctor.LogoutTest();
	}
	
	@Test(groups= {"SoftCheckout"},priority =1)
	public synchronized void CheckoutPreviousOpenConsultations()throws Exception{
		logger = Reports.extent.createTest("EMR checkout previous open consultations");
		
		moveToOpenConsultationsPage();
		String UHID = SheetsAPI.getDataProperties(Web_Testbase.input+".PatientUHID");
		Web_GeneralFunctions.sendkeys(ocp.getPatientIdField(Login_Doctor.driver, logger), UHID, "enter UHID in patient UHID search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
		if(!Web_GeneralFunctions.isVisible(ocp.getNoOpenConsultationsText(Login_Doctor.driver, logger), Login_Doctor.driver)) {
				List<WebElement> openConsultationCheckoutElements = ocp.getAllCheckoutElements(Login_Doctor.driver, logger);
				for(WebElement checkout:openConsultationCheckoutElements) {
					Web_GeneralFunctions.click(checkout, "click on checkout link", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getYesInCloseConsultationModal(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(ocp.getYesInCloseConsultationModal(Login_Doctor.driver, logger), "click on yes button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
				}
				Web_GeneralFunctions.sendkeys(ocp.getPatientIdField(Login_Doctor.driver, logger), UHID, "enter UHID in patient UHID search field", Login_Doctor.driver, logger);
				Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
				Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getNoOpenConsultationsText(Login_Doctor.driver, logger));
				assertTrue(Web_GeneralFunctions.isDisplayed(ocp.getNoOpenConsultationsText(Login_Doctor.driver, logger)));
			
			}
			Login_Doctor.LogoutTest();
		}
	
		
	//SO_40 covered here
	@Test(groups= {"SoftCheckout"},priority =2)
	public synchronized void verifyInVistiDetailsModalNotDisplayedForPatientHavingInvisitWithDifferentDoctor()throws Exception{
		logger = Reports.extent.createTest("EMR Invisit modal not displayed for patient having invisit with different doctor");
		
		Login_Doctor.LoginTestwithDiffrentUser("Lego");
		moveToOpenConsultationsPage();
		String UHID = SheetsAPI.getDataProperties(Web_Testbase.input+".PatientUHID");
		Web_GeneralFunctions.sendkeys(ocp.getPatientIdField(Login_Doctor.driver, logger), UHID, "enter UHID in patient UHID search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
		if(!Web_GeneralFunctions.isVisible(ocp.getNoOpenConsultationsText(Login_Doctor.driver, logger), Login_Doctor.driver)) {
			List<WebElement> openConsultationCheckoutElements = ocp.getAllCheckoutElements(Login_Doctor.driver, logger);
			for(WebElement checkout:openConsultationCheckoutElements) {
				Web_GeneralFunctions.click(checkout, "click on checkout link", Login_Doctor.driver, logger);
				Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getYesInCloseConsultationModal(Login_Doctor.driver, logger));
				Web_GeneralFunctions.click(ocp.getYesInCloseConsultationModal(Login_Doctor.driver, logger), "click on yes button", Login_Doctor.driver, logger);
				Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
			}
		}
		pmt.clickOrgHeader();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,cp.clickAllSlots(Login_Doctor.driver, logger));
		ConsultationTest.UHID=SheetsAPI.getDataProperties(Web_Testbase.input+".PatientUHID");
		openInvisitDetailsModal();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
		Login_Doctor.LogoutTest();
		Login_Doctor.LoginTest();
		ConsultationTest.slotId="";
		openInvisitDetailsModal();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
		assertTrue(!Web_GeneralFunctions.isDisplayed(ovp.getCheckoutAndStartNewConsultationButton(Login_Doctor.driver, logger)));
		assertTrue(!Web_GeneralFunctions.isDisplayed(ovp.getStartFollowupConsultationButton(Login_Doctor.driver, logger)));
		assertTrue(!Web_GeneralFunctions.isDisplayed(ovp.getStartNewConsultationButton(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(lrp.moveToLabResultModule(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(rp.moveToReferral(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(gpp.getGlobalPrintModule(Login_Doctor.driver, logger)));
	}
	
	
	//SO_09 covered here
	@Test(groups= {"SoftCheckout"},priority =3)
	public synchronized void validateOpenVistiDetailsModalParameters()throws Exception{
		logger = Reports.extent.createTest("EMR verify Display of Open visit modal parameters");
		pmt.clickOrgHeader();
		ConsultationTest.UHID =SheetsAPI.getDataProperties(Web_Testbase.input+".PatientUHID");
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,cp.clickAllSlots(Login_Doctor.driver, logger));
		goToConsultationPage();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(dp.moveToDiagnosisModule(Login_Doctor.driver, logger),"click on diagnosis option", Login_Doctor.driver, logger);
		DiagnosisTest.row=0;
		List<WebElement> diagnosisDeleteElements = ocp.getAllDiagnosisRowDeleteElements(Login_Doctor.driver, logger);
		for(WebElement e:diagnosisDeleteElements) {
			Web_GeneralFunctions.click(e, "remove already added Diagnosis", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(1);
		}
		Web_GeneralFunctions.scrollElementToCenter(ocp.getPrescriptionHeaderInConsultationPage(Login_Doctor.driver, logger), Login_Doctor.driver);
		List<WebElement> prescriptionDeleteElements = ocp.getAllPrescriptionRowDeleteElements(Login_Doctor.driver, logger);
		for(WebElement e:prescriptionDeleteElements) {
			Web_GeneralFunctions.click(e, "remove already added prescriptions", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(1);
		}
		Web_GeneralFunctions.click(rp.moveToReferral(Login_Doctor.driver, logger), "click on referral in side nav", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.getMessage(Login_Doctor.driver, logger));
		Web_GeneralFunctions.scrollElementToCenter(ocp.getFollowupLablel(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getFollowupCheckbox(Login_Doctor.driver, logger));
		if(ocp.getFollowupInputField(Login_Doctor.driver, logger).isEnabled()) {
			Web_GeneralFunctions.click(ocp.getFollowupCheckbox(Login_Doctor.driver, logger), "click on followup checkbox", Login_Doctor.driver, logger);
		}
		Web_GeneralFunctions.click(gpp.getGlobalPrintModule(Login_Doctor.driver, logger), "click on global print link in side nav", Login_Doctor.driver, logger);
		openInvisitDetailsModal();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ovp.getCheckoutAndStartNewConsultationButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ovp.getStartFollowupConsultationButton(Login_Doctor.driver, logger));
		assertTrue(Web_GeneralFunctions.isDisplayed(ovp.getDiagnosisLabel(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(ovp.getDateLabel(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(ovp.getMedicinesLabel(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(ovp.getFollowupLabel(Login_Doctor.driver, logger)));
	}
	
	//SO_13 covered here
	@Test(groups= {"SoftCheckout"},priority =4)
	public synchronized void validateDisplayOfEmptyParameterValuesInOpenVisitModal()throws Exception{
		logger = Reports.extent.createTest("EMR verify Empty paramter values in open visit modal");
		String diagnosisValue = Web_GeneralFunctions.getText(ovp.getDiagnosisText(Login_Doctor.driver, logger), "get Diagnosis value", Login_Doctor.driver, logger);
		String medicinesValue = Web_GeneralFunctions.getText(ovp.getMedicinesText(Login_Doctor.driver, logger), "get medicines value", Login_Doctor.driver, logger);
		String followupValue = Web_GeneralFunctions.getText(ovp.getFollowupText(Login_Doctor.driver, logger), "get follow up value", Login_Doctor.driver, logger);
		String dateValue = Web_GeneralFunctions.getText(ovp.getDateText(Login_Doctor.driver, logger), "extract date value", Login_Doctor.driver, logger);
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		String expectedDateValue = sdf.format(d);
		assertTrue(dateValue.equals(expectedDateValue));
		assertTrue(medicinesValue.trim().equals("-"));
		assertTrue(diagnosisValue.trim().equals("-"));
		assertTrue(followupValue.trim().equals("-"));
	}
	
	//SO_14 covered here
	@Test(groups= {"SoftCheckout"},priority =5)
	public synchronized void verifyButtonsDisplayOnOpenVisitModal()throws Exception{
		logger = Reports.extent.createTest("EMR verify display of buttons in open visit modal");
		assertTrue(Web_GeneralFunctions.isDisplayed(ovp.getCheckoutAndStartNewConsultationButton(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(ovp.getStartFollowupConsultationButton(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(ovp.getStartNewConsultationButton(Login_Doctor.driver, logger)));
		Web_GeneralFunctions.click(ovp.getCloseButton(Login_Doctor.driver, logger),"close open visits modal", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ovp.getFilterByLabelInAppointmentspage(Login_Doctor.driver, logger));
		pmt.clickOrgHeader();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,cp.clickAllSlots(Login_Doctor.driver, logger));
		goToConsultationPage();
	
	}
	
	//SO_08 covered here
	@Test(groups= {"SoftCheckout"},priority =6)
	public synchronized void verifySoftCheckoutPdfNotTriggered()throws Exception{
		logger = Reports.extent.createTest("EMR save consultation details And verify softcheckout Pdf not triggered");
		DiagnosisTest.row=0;
		PrescriptionTest.row=0;
		SymptomTest.row=0;
		LabTestModuleTest.row=1;
		PrescriptionPage prescription = new PrescriptionPage();
		PrescriptionTest pTest = new PrescriptionTest();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(sp.selectSymptomFromDropDown(Login_Doctor.driver, SymptomTest.row, "pain", logger), "select any one symptom", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ocp.getExaminationInConsultationPage(Login_Doctor.driver, logger), "click on examination link", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.getMessage(Login_Doctor.driver, logger));
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(dp.moveToDiagnosisModule(Login_Doctor.driver, logger), "click on diagnosis", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.getMessage(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(dp.selectDiagnosisFromDropDown(Login_Doctor.driver, DiagnosisTest.row, "Typhoid", logger), "select diagnosis from dropdown ", Login_Doctor.driver, logger);
		Web_GeneralFunctions.scrollElementToCenter(ocp.getPrescriptionHeaderInConsultationPage(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(prescription.getClinicSpecificCheckBox(Login_Doctor.driver, logger), "deselect clinic specific checkbox", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(prescription.selectMedicineFromDropDown("tablet", PrescriptionTest.row, Login_Doctor.driver, logger),"select one medicine from dropdown",Login_Doctor.driver,logger);
		pTest.selectFrequnecyFromMasterList();
		pTest.enterDuration();
		Web_GeneralFunctions.scrollElementToCenter(gap.getGeneralAdviceTextArea(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gap.getGeneralAdviceTextArea(Login_Doctor.driver, logger));
		gat.enterGeneralAdvice();
		Web_GeneralFunctions.click(gpp.getGlobalPrintModule(Login_Doctor.driver, logger), "click on global print", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.getMessage(Login_Doctor.driver, logger));
		Web_GeneralFunctions.scrollElementToCenter(ocp.getCaseSheetNotesElement(Login_Doctor.driver, logger), Login_Doctor.driver);
		caseSheetDetails = RandomStringUtils.randomAlphabetic(25);
		Web_GeneralFunctions.sendkeys(ocp.getCaseSheetNotesElement(Login_Doctor.driver, logger), caseSheetDetails, "send notes text to case sheet", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(dp.moveToDiagnosisModule(Login_Doctor.driver, logger), "move to diagnosis module", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.getMessage(Login_Doctor.driver, logger));
		Web_GeneralFunctions.wait(2);
		SymptomTest.row = 1;
		symptomName = Web_GeneralFunctions.getAttribute(sp.getSymptom(Login_Doctor.driver, SymptomTest.row, logger), "value", "extract symptom value", Login_Doctor.driver, logger);
		Web_GeneralFunctions.scrollElementToCenter(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), Login_Doctor.driver);
		diagnosisName = Web_GeneralFunctions.getAttribute(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), "value", "extract diagnosis text", Login_Doctor.driver, logger);
		Web_GeneralFunctions.scrollElementToCenter(prescription.getDrugName(PrescriptionTest.row, Login_Doctor.driver, logger), Login_Doctor.driver);
		medicineName = Web_GeneralFunctions.getAttribute(prescription.getDrugName(PrescriptionTest.row, Login_Doctor.driver, logger), "value", "get medicine name", Login_Doctor.driver, logger);
		vitalHeight = Web_GeneralFunctions.getAttribute(vp.getHeightElement(Login_Doctor.driver, logger), "value", "get height value", Login_Doctor.driver, logger);
		vitalWeight = Web_GeneralFunctions.getAttribute(vp.getWeightElement(Login_Doctor.driver, logger), "value", "get Weight value", Login_Doctor.driver, logger);
		vitalBMI = Web_GeneralFunctions.getAttribute(vp.getBMIElement(Login_Doctor.driver, logger), "value", "get BMI value", Login_Doctor.driver, logger);
		Web_GeneralFunctions.scrollElementToCenter(gap.getGeneralAdviceTextArea(Login_Doctor.driver, logger), Login_Doctor.driver);
		generalAdviceText = Web_GeneralFunctions.getText(gap.getGeneralAdviceTextArea(Login_Doctor.driver, logger), "extract generalAdvice text", Login_Doctor.driver, logger);
		Web_GeneralFunctions.scrollElementToCenter(ocp.getFollowupLablel(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getFollowupCheckbox(Login_Doctor.driver, logger));
		if(!ocp.getFollowupInputField(Login_Doctor.driver, logger).isEnabled()) {
			Web_GeneralFunctions.click(ocp.getFollowupCheckbox(Login_Doctor.driver, logger), "click on followup checkbox", Login_Doctor.driver, logger);
		}
		Web_GeneralFunctions.click(gpp.getGlobalPrintModule(Login_Doctor.driver, logger), "click on global print link in side nav", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.getMessage(Login_Doctor.driver, logger));
		assertTrue(ocp.getWindowHandlesSize(Login_Doctor.driver, logger)==1);
	}
	
	
	//SO_11 covered here
	@Test(groups= {"SoftCheckout"},priority =7)
	public synchronized void verifySavedConsultationDetailsInOpenVisitsPage()throws Exception{
		logger = Reports.extent.createTest("EMR verify saved consultation details in open visits page");
		pmt.clickOrgHeader();
		ConsultationTest.UHID =SheetsAPI.getDataProperties(Web_Testbase.input+".PatientUHID");
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,cp.clickAllSlots(Login_Doctor.driver, logger));
		openInvisitDetailsModal();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ovp.getCheckoutAndStartNewConsultationButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ovp.getStartFollowupConsultationButton(Login_Doctor.driver, logger));
		String savedDiagnosis =Web_GeneralFunctions.getAttribute(ovp.getDiagnosisText(Login_Doctor.driver, logger), "title", "get selected diagnosis value", Login_Doctor.driver, logger);
		String savedMedicines = Web_GeneralFunctions.getAttribute(ovp.getMedicinesText(Login_Doctor.driver, logger), "title", "get selected medicines", Login_Doctor.driver, logger);
		String savedFollowup = Web_GeneralFunctions.getAttribute(ovp.getFollowupText(Login_Doctor.driver, logger), "title", "get followup date", Login_Doctor.driver, logger);
		assertTrue(savedDiagnosis.contains(diagnosisName));
		assertTrue(savedMedicines.contains(medicineName));
		assertTrue(savedFollowup.trim().equals(getFutureDate(7)));
		
	}
	
	//SO_31 covered here
	@Test(groups= {"SoftCheckout"},priority =8)
	public synchronized void searchPatientWithValidRegisteredName()throws Exception{
		logger = Reports.extent.createTest("EMR search patient with valid Registered name");
		pmt.clickOrgHeader();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
		moveToOpenConsultationsPage();
		String searchName =SheetsAPI.getDataProperties(Web_Testbase.input+".PatName");
		Web_GeneralFunctions.sendkeys(ocp.getPatientNameSearchField(Login_Doctor.driver, logger), searchName, "send serachname to patient name field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
		String patientDetails = Web_GeneralFunctions.getAttribute(ocp.getPatientDetailsElement(Login_Doctor.driver, logger), "title", "get patient details from serach results", Login_Doctor.driver, logger);
		assertTrue(patientDetails.trim().contains(searchName));
	}
	
	//SO_37 covered here
	@Test(groups= {"SoftCheckout"},priority =9)
	public synchronized void searchPatientWithValidRegisteredUHID()throws Exception{
		logger = Reports.extent.createTest("EMR search patient with valid UHID");
		Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
		String UHID = SheetsAPI.getDataProperties(Web_Testbase.input+".PatientUHID");
		Web_GeneralFunctions.sendkeys(ocp.getPatientIdField(Login_Doctor.driver, logger), UHID, "sending UHID to patient id search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
		String patientDetails = Web_GeneralFunctions.getAttribute(ocp.getPatientDetailsElement(Login_Doctor.driver, logger), "title", "get patient details from serach results", Login_Doctor.driver, logger);
		assertTrue(patientDetails.trim().contains(UHID));
		
	}
	
	//SO_32 covered here
	@Test(groups= {"SoftCheckout"},priority =10)
	public synchronized void searchPatientWithInvalidName()throws Exception{
		logger = Reports.extent.createTest("EMR search patient with invalid name");
		Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
		String invalidSearchName = SheetsAPI.getDataProperties(Web_Testbase.input+".InvalidName");
		Web_GeneralFunctions.sendkeys(ocp.getPatientNameSearchField(Login_Doctor.driver, logger), invalidSearchName, "send serachname to patient name field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
		assertTrue(Web_GeneralFunctions.isDisplayed(ocp.getNoOpenConsultationsText(Login_Doctor.driver, logger)));
		
	}
	
	//SO_38 covered here
	@Test(groups= {"SoftCheckout"},priority =11)
	public synchronized void searchPatientWithUnregisteredUHID(){
		logger = Reports.extent.createTest("EMR search patient with Unregistered UHID");
		Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
		String UHID = RandomStringUtils.randomNumeric(5);
		Web_GeneralFunctions.sendkeys(ocp.getPatientIdField(Login_Doctor.driver, logger), UHID, "sending UHID to patient id search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
		assertTrue(Web_GeneralFunctions.isDisplayed(ocp.getNoOpenConsultationsText(Login_Doctor.driver, logger)));
		
	}
	
	//SO_33 covered here
	@Test(groups= {"SoftCheckout"},priority =12)
	public synchronized void searchPatientWithRegisterdValidMobileNumber()throws Exception{
		logger = Reports.extent.createTest("EMR search patient with valid mobile Number");
		Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
		String mobileNumber = SheetsAPI.getDataProperties(Web_Testbase.input+".PatMobileNumber");
		Web_GeneralFunctions.sendkeys(ocp.getMobileNumberSearchField(Login_Doctor.driver, logger), mobileNumber, "send mobile number to search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
		String patientMobileNumber = Web_GeneralFunctions.getText(ocp.getPatientMobileNumberElement(Login_Doctor.driver, logger), "get patient mobile number", Login_Doctor.driver, logger);
		assertTrue(patientMobileNumber.trim().equals(mobileNumber));
		
	}
	
	//SO_34 covered here
	@Test(groups= {"SoftCheckout"},priority =13)
	public synchronized void searchPatientWithUnregisteredMobileNumber()throws Exception{
		logger = Reports.extent.createTest("EMR search patient with Unregistered/invalid mobile number");
		Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
		String mobileNumber = SheetsAPI.getDataProperties(Web_Testbase.input+".InvalidPhNo");
		Web_GeneralFunctions.sendkeys(ocp.getMobileNumberSearchField(Login_Doctor.driver, logger), mobileNumber, "send mobile number to search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
		assertTrue(Web_GeneralFunctions.isDisplayed(ocp.getNoOpenConsultationsText(Login_Doctor.driver, logger)));
		
	}
	
	//SO_35 covered here
	@Test(groups= {"SoftCheckout"},priority =14)
	public synchronized void verifyPopOverErrorMessageForInvalidMobileNumber()throws Exception{
		logger = Reports.extent.createTest("EMR verify Pop over error message for invalid mobile Number");
		Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
		String mobileNumber = RandomStringUtils.randomAlphabetic(5);
		Web_GeneralFunctions.sendkeys(ocp.getMobileNumberSearchField(Login_Doctor.driver, logger), mobileNumber, "send mobile number to search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getMobileNumberPopOverMessage(Login_Doctor.driver, logger));
		String popOverMessage = Web_GeneralFunctions.getText(ocp.getMobileNumberPopOverMessage(Login_Doctor.driver, logger), "extract pop over error message", Login_Doctor.driver, logger);
		assertTrue(popOverMessage.trim().equalsIgnoreCase("The mobile Number must be a Number."));
	
		
	}
	
	//SO_36 covered here
	@Test(groups= {"SoftCheckout"},priority =15)
	public synchronized void verifyFutureDatesDisabledForEndDateField()throws Exception{
		logger = Reports.extent.createTest("EMR verify future dates are disabled for end date field");
		Web_GeneralFunctions.clearWebElement(ocp.getMobileNumberSearchField(Login_Doctor.driver, logger), "clear mobile number field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ocp.getEndDateField(Login_Doctor.driver, logger), "click on End date field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getMonthPicker(Login_Doctor.driver, logger));
		LocalDate currentDate = LocalDate.now();
		int currentDay = currentDate.getDayOfMonth();
		int currentYear = currentDate.getYear();
		int nextDay  = currentDay+1;
		String nextYear = Integer.toString(currentYear+1);
		List<String> allDisplayedYears = ocp.getAllYearOptionsAsList(Login_Doctor.driver, logger);
		for(String year:allDisplayedYears) {
			assertTrue(!year.equals(nextYear));
		}
		String classProperty = Web_GeneralFunctions.getAttribute(ocp.getFutureDayElement(Login_Doctor.driver, nextDay, logger), "class", "get property of datepicker class", Login_Doctor.driver, logger);
		assertTrue(classProperty.trim().contains("unselectable"));
		assertTrue(classProperty.trim().contains("disabled"));
		String nextNavigationlinkClassProperty = Web_GeneralFunctions.getAttribute(ocp.getNextDatePicker(Login_Doctor.driver, logger), "class", "get class property ", Login_Doctor.driver, logger);
		assertTrue(nextNavigationlinkClassProperty.trim().contains("disabled"));
	}
	
	//SO_39 covered here
	@Test(groups= {"SoftCheckout"},priority =16)
	public synchronized void verifyPopOverErrorMessageForInvalidUHID()throws Exception{
		logger = Reports.extent.createTest("EMR verify Pop over error message for invalid alpahbetic UHID");
		Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
		String invalidUHID = SheetsAPI.getDataProperties(Web_Testbase.input+".SpecialChars");
		Web_GeneralFunctions.sendkeys(ocp.getPatientIdField(Login_Doctor.driver, logger), invalidUHID, "enter invalid UHID", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getMobileNumberPopOverMessage(Login_Doctor.driver, logger));
		String popOverMessage = Web_GeneralFunctions.getText(ocp.getMobileNumberPopOverMessage(Login_Doctor.driver, logger), "extract pop over error message", Login_Doctor.driver, logger);
		assertTrue(popOverMessage.trim().equalsIgnoreCase("The Patient id must be a Number."));
		Web_GeneralFunctions.clearWebElement(ocp.getPatientIdField(Login_Doctor.driver, logger), "clear mobile number field", Login_Doctor.driver, logger);
		
	
	
	}
	
	//SO_24 covered here
	@Test(groups= {"SoftCheckout"},priority =17)
	public synchronized void verifyBillingPageDisplayedAfterSoftCheckout()throws Exception{
		logger = Reports.extent.createTest("EMR veirfy Billing page displayed after softCheckout");
		pmt.clickOrgHeader();
		goToConsultationPage();
		LabTestModuleTest.row =1;
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.scrollElementToCenter(ltp.getLabTest(Login_Doctor.driver, LabTestModuleTest.row, logger), Login_Doctor.driver);
		Web_GeneralFunctions.clearWebElement(ltp.getLabTest(Login_Doctor.driver, LabTestModuleTest.row, logger),"clear lab test field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ltp.selectLabTestFromDropDown(Login_Doctor.driver, LabTestModuleTest.row,"Blood", logger), "select lab test from dropdown", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(dp.moveToDiagnosisModule(Login_Doctor.driver, logger), "click no prescription in right nav", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ltp.getMessage(Login_Doctor.driver, logger));
		labTestName = Web_GeneralFunctions.getAttribute(ltp.getLabTest(Login_Doctor.driver, LabTestModuleTest.row, logger), "value", "get lab test name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(gpp.getGlobalPrintModule(Login_Doctor.driver, logger), "click on global print", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getShareToFollowUpButton(Login_Doctor.driver, logger));
		assertTrue(Web_GeneralFunctions.isDisplayed(gpp.getShareToFollowUpButton(Login_Doctor.driver, logger)));
		Web_GeneralFunctions.click(gpp.getShareToFollowUpButton(Login_Doctor.driver, logger), "click on share to followup", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(4);
		pdfSummaryContent = gpt.getPDFPage();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, bp.getEmrCreateBillServiceSectionText(Login_Doctor.driver, logger));
		assertTrue(Web_GeneralFunctions.isDisplayed(ocp.getBillingHeader(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(bp.getEmrCreateBillServiceSectionText(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(ocp.getCreateBillHeader(Login_Doctor.driver, logger)));
	}
	
	//SO_01 covered here
	@Test(groups= {"SoftCheckout"},priority =18)
	public synchronized void verifyPDFGeneratedAfterSoftCheckout(){
		logger = Reports.extent.createTest("EMR verify pdf genereted after softCheckout");
		assertTrue(pdfSummaryContent.contains(medicineName));
		assertTrue(pdfSummaryContent.contains(diagnosisName.substring(6)));
		assertTrue(pdfSummaryContent.contains(labTestName));
		assertTrue(pdfSummaryContent.contains(symptomName));
		assertTrue(pdfSummaryContent.contains(generalAdviceText));
		assertTrue(pdfSummaryContent.contains(vitalHeight));
		assertTrue(pdfSummaryContent.contains(vitalBMI));
		assertTrue(pdfSummaryContent.contains(vitalWeight));
		assertTrue(pdfSummaryContent.toLowerCase().contains(caseSheetDetails.toLowerCase()));
	}

	
	//SO_04 covered here
	@Test(groups= {"SoftCheckout"},priority =19)
	public synchronized void verifyDraftTextNotPresentInSummaryPdf(){
		logger = Reports.extent.createTest("EMR veirfy DRAFT text not present in summary pdf");
		assertTrue(!pdfSummaryContent.toLowerCase().contains("draft"));
	}
	
	//sO-06 covered here
	@Test(groups= {"SoftCheckout"},priority =20)
	public synchronized void verifyErrorMessageForSecondSoftCheckout()throws Exception{
		logger = Reports.extent.createTest("EMR Display of toast message for no change in consultation details");
		pmt.clickOrgHeader();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,cp.clickAllSlots(Login_Doctor.driver, logger));
		goToConsultationPage();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.scrollElementToCenter(gpp.getShareToFollowUpButton(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(gpp.getShareToFollowUpButton(Login_Doctor.driver, logger), "click on share to followup button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.getMessage(Login_Doctor.driver, logger));
		String toastText = Web_GeneralFunctions.getText(dp.getMessage(Login_Doctor.driver, logger), "extract toast text", Login_Doctor.driver, logger);
		assertTrue(toastText.trim().equalsIgnoreCase("no changes in consultation."));
		assertTrue(Login_Doctor.driver.getWindowHandles().size()==1);
	}
	
	//SO_07 covered here
	@Test(groups= {"SoftCheckout"},priority =21)
	public synchronized void verifySoftCheckoutButtonIsClickable()throws Exception{
		logger = Reports.extent.createTest("EMR verify SoftCheckout button is clickable");
		pmt.clickOrgHeader();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,cp.clickAllSlots(Login_Doctor.driver, logger));
		goToConsultationPage();
		LabTestModuleTest.row =1;
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.scrollElementToCenter(ltp.getLabTest(Login_Doctor.driver, LabTestModuleTest.row, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(ltp.getDeleteButton(LabTestModuleTest.row, Login_Doctor.driver, logger), "delete lab test row", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(dp.moveToDiagnosisModule(Login_Doctor.driver, logger), "click no prescription in right nav", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ltp.getMessage(Login_Doctor.driver, logger));
		Web_GeneralFunctions.scrollElementToCenter(ltp.getLabTest(Login_Doctor.driver, LabTestModuleTest.row, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(ltp.selectLabTestFromDropDown(Login_Doctor.driver, LabTestModuleTest.row,"Thyroid", logger), "select lab test from dropdown", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(dp.moveToDiagnosisModule(Login_Doctor.driver, logger), "click no prescription in right nav", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ltp.getMessage(Login_Doctor.driver, logger));
		labTestName = Web_GeneralFunctions.getAttribute(ltp.getLabTest(Login_Doctor.driver, LabTestModuleTest.row, logger), "value", "get lab test name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(gpp.getGlobalPrintModule(Login_Doctor.driver, logger), "click on global print", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getShareToFollowUpButton(Login_Doctor.driver, logger));
		assertTrue(Web_GeneralFunctions.isClickable(gpp.getShareToFollowUpButton(Login_Doctor.driver, logger), Login_Doctor.driver));
		Web_GeneralFunctions.click(gpp.getShareToFollowUpButton(Login_Doctor.driver, logger), "click on share to followup", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(3);
		assertTrue(ocp.getWindowHandlesSize(Login_Doctor.driver, logger)==2);
		pdfSummaryContent = gpt.getPDFPage();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, bp.getEmrCreateBillServiceSectionText(Login_Doctor.driver, logger));
		assertTrue(Web_GeneralFunctions.isDisplayed(ocp.getBillingHeader(Login_Doctor.driver, logger)));
		assertTrue(Web_GeneralFunctions.isDisplayed(bp.getEmrCreateBillServiceSectionText(Login_Doctor.driver, logger)));
		assertTrue(pdfSummaryContent.contains(medicineName));
		assertTrue(pdfSummaryContent.contains(diagnosisName.substring(6)));
		assertTrue(pdfSummaryContent.contains(labTestName));
		assertTrue(pdfSummaryContent.contains(symptomName));
		assertTrue(pdfSummaryContent.contains(generalAdviceText));
	}
	
	//SO_25 covered here
		@Test(groups= {"SoftCheckout"},priority =22)
		public synchronized void verifyBillingPageNotDisplayedAfterPayingBill()throws Exception{
			logger = Reports.extent.createTest("EMR verify billing page not displayed after paying bill for first softcheckout");
			DiagnosisTest.row =0;
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, bp.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service",0));
			Web_GeneralFunctions.sendkeys(bp.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", 0), "test", "sending service text", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, bp.getEmrCreateBillServiceList(Login_Doctor.driver, logger).get(2));
			Web_GeneralFunctions.click(bp.getEmrCreateBillServiceList(Login_Doctor.driver, logger).get(2), "select a service", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(bp.getEmrCreateBillPayementModeTextBox(Login_Doctor.driver, logger, "paymentModeCash"), "check cash payment option", Login_Doctor.driver, logger);
			Web_GeneralFunctions.isChecked(bp.getEmrCreateBillPayementModeTextBox(Login_Doctor.driver, logger, "paymentModeCash"), Login_Doctor.driver);
			Web_GeneralFunctions.click(bp.getEmrCreateBillGenerateButton(Login_Doctor.driver, logger), "click on generate bill button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, bp.getEmrGeneratePatientNameBillNumber(Login_Doctor.driver, logger));
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, bp.getEmrGenerateBillCloseButton(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(bp.getEmrGenerateBillCloseButton(Login_Doctor.driver, logger), "click on bill close button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
			goToConsultationPage();
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(dp.moveToDiagnosisModule(Login_Doctor.driver, logger), "click no prescription in right nav", Login_Doctor.driver, logger);
			Web_GeneralFunctions.clearWebElement(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), "clear diagnosis field", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(dp.selectDiagnosisFromDropDown(Login_Doctor.driver, DiagnosisTest.row, "Typhoid", logger), "select diagnosis from dropdown ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(gpp.getGlobalPrintModule(Login_Doctor.driver, logger), "click on global print module link", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.getMessage(Login_Doctor.driver, logger));
			diagnosisName = Web_GeneralFunctions.getAttribute(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), "value", "extract diagnosis text", Login_Doctor.driver, logger);
			Web_GeneralFunctions.scrollElementToCenter(gpp.getShareToFollowUpButton(Login_Doctor.driver, logger), Login_Doctor.driver);
			assertTrue(Web_GeneralFunctions.isClickable(gpp.getShareToFollowUpButton(Login_Doctor.driver, logger), Login_Doctor.driver));
			Web_GeneralFunctions.click(gpp.getShareToFollowUpButton(Login_Doctor.driver, logger), "click on share to followup", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(3);
			gpt.getPDFPage();
			assertTrue(Web_GeneralFunctions.isDisplayed(ap.getAllSlotsPage(Login_Doctor.driver, logger)));
			assertTrue(!Web_GeneralFunctions.isDisplayed(ocp.getBillingHeader(Login_Doctor.driver, logger)));
			assertTrue(!Web_GeneralFunctions.isDisplayed(bp.getEmrCreateBillServiceSectionText(Login_Doctor.driver, logger)));
		}
	
	//SO_05 covered here
	@Test(groups= {"SoftCheckout"},priority =23)
	public synchronized void verifyStatusOfAppointmentAfterSoftcheckout()throws Exception{
		logger = Reports.extent.createTest("EMR verify Appointment status after soft checkout");
		moveToOpenConsultationsPage();
		String UHID = SheetsAPI.getDataProperties(Web_Testbase.input+".PatientUHID");
		Web_GeneralFunctions.sendkeys(ocp.getPatientIdField(Login_Doctor.driver, logger), UHID, "sending UHID to patient id search field", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
		String patientDetails = Web_GeneralFunctions.getAttribute(ocp.getPatientDetailsElement(Login_Doctor.driver, logger), "title", "get patient details from serach results", Login_Doctor.driver, logger);
		String consultationStatus = Web_GeneralFunctions.getText(ocp.getConsultationStatus(Login_Doctor.driver, logger), "extract status of consultation", Login_Doctor.driver, logger);
		assertTrue(patientDetails.trim().contains(UHID));
		assertTrue(consultationStatus.trim().equals("Follow up"));
	}
	
	//SO_41 covered here
		@Test(groups= {"SoftCheckout"},priority =24)
		public synchronized void verifyCloseConsultationPopupDisplay(){
			logger = Reports.extent.createTest("EMR Display of Close Consultation confirmation pop up");
			Web_GeneralFunctions.click(ocp.getFirstOpenConsultationCheckoutButton(Login_Doctor.driver, logger), "click on checkout button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver,ocp.getCloseConsultationConfirmationPopup(Login_Doctor.driver, logger));
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getNoInCloseConsultationModal(Login_Doctor.driver, logger));
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getYesInCloseConsultationModal(Login_Doctor.driver, logger));
			assertTrue(Web_GeneralFunctions.isDisplayed(ocp.getCloseConsultationConfirmationPopup(Login_Doctor.driver, logger)));
			assertTrue(Web_GeneralFunctions.isDisplayed(ocp.getCloseConfirmationPopupText(Login_Doctor.driver, logger)));
			assertTrue(Web_GeneralFunctions.isDisplayed(ocp.getNoInCloseConsultationModal(Login_Doctor.driver, logger)));
			assertTrue(Web_GeneralFunctions.isDisplayed(ocp.getYesInCloseConsultationModal(Login_Doctor.driver, logger)));
			Web_GeneralFunctions.click(ocp.getNoInCloseConsultationModal(Login_Doctor.driver, logger), "click no on close confirmation modal", Login_Doctor.driver, logger);
		}
		
		//SO_20 covered here
		@Test(groups= {"SoftCheckout"},priority =25)
		public synchronized void verifyAbleToStartNewConsultationWithExistingOpenConsultation()throws Exception{
			logger = Reports.extent.createTest("EMR Able to start new visit with existing open visit");
			pmt.clickOrgHeader();
			openInvisitDetailsModal();
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ovp.getStartNewConsultationButton(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(ovp.getStartNewConsultationButton(Login_Doctor.driver, logger), "click on start new consultation button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
			assertTrue(Web_GeneralFunctions.isDisplayed(rp.moveToReferral(Login_Doctor.driver, logger)));
			assertTrue(Web_GeneralFunctions.isDisplayed(gpp.getGlobalPrintModule(Login_Doctor.driver, logger)));
			moveToOpenConsultationsPage();
			String UHID = SheetsAPI.getDataProperties(Web_Testbase.input+".PatientUHID");
			Web_GeneralFunctions.sendkeys(ocp.getPatientIdField(Login_Doctor.driver, logger), UHID, "sending UHID to patient id search field", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
			assertTrue(ocp.getListOfOpenConsultationRowElements(Login_Doctor.driver, logger).size()==2);
			List<WebElement> patientDetailsList = ocp.getAllOpenConsultationPatientColumnValuesAsList(Login_Doctor.driver, logger);
			for(WebElement patient :patientDetailsList) {
				assertTrue(Web_GeneralFunctions.getAttribute(patient, "title","get patient name", Login_Doctor.driver, logger).trim().contains(UHID));
			}
			List<WebElement> consultationStatusList = ocp.getAllOpenConsultationStatusColumnValuesAsList(Login_Doctor.driver, logger);
			for(WebElement constatus:consultationStatusList) {
				String status = Web_GeneralFunctions.getAttribute(constatus, "title", "extract status", Login_Doctor.driver, logger);
				assertTrue(status.trim().equalsIgnoreCase("Consulting") || status.trim().equalsIgnoreCase("Follow up"));
			}
		}
		
		//SO_21 covered here
		@Test(groups= {"SoftCheckout"},priority =26)
		public synchronized void verifyDiagnosisValueIsVisitSpecific()throws Exception{
			logger = Reports.extent.createTest("EMR verify Diagnosis Value is Visit specific");
			DiagnosisTest.row=0;
			Web_GeneralFunctions.click(ocp.getConsultingAppointmentViewButton(Login_Doctor.driver, logger), "click on view button ", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
			Web_GeneralFunctions.scrollElementToCenter(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), Login_Doctor.driver);
			String secondVisitDiagnosisName = Web_GeneralFunctions.getAttribute(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), "value", "extract diagnosis text", Login_Doctor.driver, logger);
			assertTrue(!secondVisitDiagnosisName.equals(diagnosisName));
			assertTrue(secondVisitDiagnosisName.isEmpty());
	
		}
		
		//SO_15 covered here
		@Test(groups= {"SoftCheckout"},priority =27)
		public synchronized void verifyCountOfButtonsDisplayedOnOpenVisitModalForTwoOpenVisits()throws Exception{
			logger = Reports.extent.createTest("EMR verify count of buttons displayed for each open visit row");
			pmt.clickOrgHeader();
			openInvisitDetailsModal();
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ovp.getCloseButton(Login_Doctor.driver, logger));
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver,ovp.getConsultationHeaderLabel(Login_Doctor.driver, logger));
			assertTrue(ovp.getButtonElementsInFirstOpenVisitRow(Login_Doctor.driver, logger).size()==2);
			assertTrue(ovp.getButtonElementsInSecondOpenVisitRow(Login_Doctor.driver, logger).size()==2);
			assertTrue(ovp.getOpenVisitButtonRows(Login_Doctor.driver, logger).size()==2);
		}
		
		//SO_28 covered here
				@Test(groups= {"SoftCheckout"},priority =28)
				public synchronized void verifyButtonsDisplayedOnOpenVisitModalForTwoOpenVisits()throws Exception{
					logger = Reports.extent.createTest("EMR verify buttons displayed for two open visits");
					List<WebElement> buttonElementsInFirstRow = ovp.getButtonElementsInFirstOpenVisitRow(Login_Doctor.driver, logger);
					List<WebElement> buttonElementsInSecondRow = ovp.getButtonElementsInSecondOpenVisitRow(Login_Doctor.driver, logger);
					assertTrue(Web_GeneralFunctions.getText(buttonElementsInFirstRow.get(0), "first row first button text", Login_Doctor.driver, logger).trim().equalsIgnoreCase("Check out and start new consultation"));
					assertTrue(Web_GeneralFunctions.getText(buttonElementsInFirstRow.get(1), "first row second button text", Login_Doctor.driver, logger).trim().equalsIgnoreCase("Start follow up consultation"));
					assertTrue(Web_GeneralFunctions.getText(buttonElementsInSecondRow.get(0), "first row first button text", Login_Doctor.driver, logger).trim().equalsIgnoreCase("Check out and start new consultation"));
					assertTrue(Web_GeneralFunctions.getText(buttonElementsInSecondRow.get(1), "first row second button text", Login_Doctor.driver, logger).trim().equalsIgnoreCase("Start follow up consultation"));
					Web_GeneralFunctions.click(ovp.getCloseButton(Login_Doctor.driver, logger), "click on close button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ovp.getFilterByLabelInAppointmentspage(Login_Doctor.driver, logger));
				}
				
				
				//SO_42 covered here
				@Test(groups= {"SoftCheckout"},priority =29)
				public synchronized void verifyButtonsFunctionalityInCloseConsultationsConfirmationModal()throws Exception{
					logger = Reports.extent.createTest("EMR verify buttons functionality in close consultation confirmation modal");
					moveToOpenConsultationsPage();
					String UHID = SheetsAPI.getDataProperties(Web_Testbase.input+".PatientUHID");
					Web_GeneralFunctions.sendkeys(ocp.getPatientIdField(Login_Doctor.driver, logger), UHID, "sending UHID to patient id search field", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(ocp.getFollowUpAppointmentCheckoutButton(Login_Doctor.driver, logger), "click on checkout button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver,ocp.getCloseConsultationConfirmationPopup(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getNoInCloseConsultationModal(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getYesInCloseConsultationModal(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(ocp.getNoInCloseConsultationModal(Login_Doctor.driver, logger), "click on No in confirmation modal", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getPatientNameSearchField(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getPatientIdField(Login_Doctor.driver, logger));
					assertTrue(ocp.getCloseConsultationConfirmationPopup(Login_Doctor.driver, logger)==null);
					Web_GeneralFunctions.click(ocp.getFollowUpAppointmentCheckoutButton(Login_Doctor.driver, logger), "click on checkout button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver,ocp.getCloseConsultationConfirmationPopup(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getNoInCloseConsultationModal(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(ocp.getYesInCloseConsultationModal(Login_Doctor.driver, logger), "click yes in confirmation modal", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getPatientNameSearchField(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getPatientIdField(Login_Doctor.driver, logger));
					Web_GeneralFunctions.sendkeys(ocp.getPatientIdField(Login_Doctor.driver, logger), UHID, "sending UHID to patient id search field", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
					assertTrue(ocp.getListOfOpenConsultationRowElements(Login_Doctor.driver, logger).size()==1);
					openConsultationDateTime =Web_GeneralFunctions.getText(ocp.getDateText(Login_Doctor.driver, logger), "get date time value", Login_Doctor.driver, logger);
				}
				
				//SO_17 covered here
				@Test(groups= {"SoftCheckout"},priority =30)
				public synchronized void clickOnStartFollowupWithEmptyPreviousConsultationData()throws Exception{
					logger = Reports.extent.createTest("EMR verify able to close previous consultation with empty data");
					pmt.clickOrgHeader();
					DiagnosisTest.row =0;
					PrescriptionTest.row=0;
					LabTestModuleTest.row =1;
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
					openInvisitDetailsModal();
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ovp.getCheckoutAndStartNewConsultationButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ovp.getStartFollowupConsultationButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(ovp.getStartFollowupConsultationButton(Login_Doctor.driver, logger), "click on start followup consultation button ", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
					Web_GeneralFunctions.scrollElementToCenter(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), Login_Doctor.driver);
					diagnosisName = Web_GeneralFunctions.getAttribute(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), "value", "extract diagnosis text", Login_Doctor.driver, logger);
					Web_GeneralFunctions.scrollElementToCenter(pp.getDrugName(PrescriptionTest.row, Login_Doctor.driver, logger), Login_Doctor.driver);
					medicineName = Web_GeneralFunctions.getAttribute(pp.getDrugName(PrescriptionTest.row, Login_Doctor.driver, logger), "value", "get medicine name", Login_Doctor.driver, logger);
					Web_GeneralFunctions.scrollElementToCenter(ltp.getLabTest(Login_Doctor.driver, LabTestModuleTest.row, logger), Login_Doctor.driver);
					labTestName = Web_GeneralFunctions.getAttribute(ltp.getLabTest(Login_Doctor.driver, LabTestModuleTest.row, logger), "value", "get lab test name", Login_Doctor.driver, logger);
					pmt.clickOrgHeader();
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
					moveToOpenConsultationsPage();
					String UHID = SheetsAPI.getDataProperties(Web_Testbase.input+".PatientUHID");
					Web_GeneralFunctions.sendkeys(ocp.getPatientIdField(Login_Doctor.driver, logger), UHID, "sending UHID to patient id search field", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
					String newConsultationDateTime =Web_GeneralFunctions.getText(ocp.getDateText(Login_Doctor.driver, logger), "get date time value", Login_Doctor.driver, logger);
					assertTrue(ocp.getListOfOpenConsultationRowElements(Login_Doctor.driver, logger).size()==1);
					assertTrue(!newConsultationDateTime.trim().equals(openConsultationDateTime));
					assertTrue(diagnosisName.isEmpty());
					assertTrue(medicineName.isEmpty());
					assertTrue(labTestName.isEmpty());
					openConsultationDateTime = newConsultationDateTime;
				}
			
				//SO_16 covered here
				@Test(groups= {"SoftCheckout"},priority =31)
				public synchronized void veriyOldVisitDetailsCopiedToNewVisitForFollowup()throws Exception{
					logger = Reports.extent.createTest("EMR verify old visit details copied to new visit for follow up");
					SymptomTest.row =0;
					DiagnosisTest.row =0;
					PrescriptionTest.row =0;
					LabTestModuleTest.row =1;
					Web_GeneralFunctions.click(ocp.getConsultingAppointmentViewButton(Login_Doctor.driver, logger), "click on view button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptom(Login_Doctor.driver, SymptomTest.row, logger));
					Web_GeneralFunctions.click(sp.selectSymptomFromDropDown(Login_Doctor.driver, SymptomTest.row, "pain", logger), "select any one symptom", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(dp.moveToDiagnosisModule(Login_Doctor.driver, logger), "move to diagnosis module", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.getMessage(Login_Doctor.driver, logger));
					Web_GeneralFunctions.wait(3);
					Web_GeneralFunctions.scrollElementToCenter(ocp.getCaseSheetNotesElement(Login_Doctor.driver, logger), Login_Doctor.driver);
					caseSheetDetails = RandomStringUtils.randomAlphabetic(25);
					Web_GeneralFunctions.sendkeys(ocp.getCaseSheetNotesElement(Login_Doctor.driver, logger), caseSheetDetails, "send notes text to case sheet", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(dp.moveToDiagnosisModule(Login_Doctor.driver, logger), "move to diagnosis module", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.getMessage(Login_Doctor.driver, logger));
					Web_GeneralFunctions.clearWebElement(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), "click on diagnosis text field", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(dp.selectDiagnosisFromDropDown(Login_Doctor.driver, DiagnosisTest.row, "Typhoid", logger), "select diagnosis from dropdown ", Login_Doctor.driver, logger);
					Web_GeneralFunctions.scrollElementToCenter(ocp.getPrescriptionHeaderInConsultationPage(Login_Doctor.driver, logger), Login_Doctor.driver);
					Web_GeneralFunctions.click(pp.getClinicSpecificCheckBox(Login_Doctor.driver, logger), "deselect clinic specific checkbox", Login_Doctor.driver, logger);
					Web_GeneralFunctions.clearWebElement(pp.getDrugName(PrescriptionTest.row, Login_Doctor.driver, logger), "click on prescription text field", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(pp.selectMedicineFromDropDown("tablet", PrescriptionTest.row, Login_Doctor.driver, logger),"select one medicine from dropdown",Login_Doctor.driver,logger);
					pt.selectFrequnecyFromMasterList();
					pt.enterDuration();
					Web_GeneralFunctions.scrollElementToCenter(gap.getGeneralAdviceTextArea(Login_Doctor.driver, logger), Login_Doctor.driver);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gap.getGeneralAdviceTextArea(Login_Doctor.driver, logger));
					gat.enterGeneralAdvice();
					Web_GeneralFunctions.click(gpp.getGlobalPrintModule(Login_Doctor.driver, logger), "click on global print", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.getMessage(Login_Doctor.driver, logger));
					Web_GeneralFunctions.scrollElementToCenter(ltp.getLabTest(Login_Doctor.driver, LabTestModuleTest.row, logger), Login_Doctor.driver);
					Web_GeneralFunctions.clearWebElement(ltp.getLabTest(Login_Doctor.driver, LabTestModuleTest.row, logger),"clear lab test field", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(ltp.selectLabTestFromDropDown(Login_Doctor.driver, LabTestModuleTest.row,"Blood", logger), "select lab test from dropdown", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(dp.moveToDiagnosisModule(Login_Doctor.driver, logger), "click no prescription in right nav", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ltp.getMessage(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger), "click on symptoms menu ", Login_Doctor.driver, logger);
					SymptomTest.row =1;
					symptomName = Web_GeneralFunctions.getAttribute(sp.getSymptom(Login_Doctor.driver, SymptomTest.row, logger), "value", "extract symptom value", Login_Doctor.driver, logger);
					Web_GeneralFunctions.scrollElementToCenter(ltp.getLabTest(Login_Doctor.driver, LabTestModuleTest.row, logger), Login_Doctor.driver);
					labTestName = Web_GeneralFunctions.getAttribute(ltp.getLabTest(Login_Doctor.driver, LabTestModuleTest.row, logger), "value", "get lab test name", Login_Doctor.driver, logger);
					Web_GeneralFunctions.scrollElementToCenter(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), Login_Doctor.driver);
					diagnosisName = Web_GeneralFunctions.getAttribute(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), "value", "extract diagnosis text", Login_Doctor.driver, logger);
					Web_GeneralFunctions.scrollElementToCenter(pp.getDrugName(PrescriptionTest.row, Login_Doctor.driver, logger), Login_Doctor.driver);
					medicineName = Web_GeneralFunctions.getAttribute(pp.getDrugName(PrescriptionTest.row, Login_Doctor.driver, logger), "value", "get medicine name", Login_Doctor.driver, logger);
					Web_GeneralFunctions.scrollElementToCenter(gap.getGeneralAdviceTextArea(Login_Doctor.driver, logger), Login_Doctor.driver);
					generalAdviceText = Web_GeneralFunctions.getText(gap.getGeneralAdviceTextArea(Login_Doctor.driver, logger), "extract generalAdvice text", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(gpp.getGlobalPrintModule(Login_Doctor.driver, logger), "click on global print button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getShareToFollowUpButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(gpp.getShareToFollowUpButton(Login_Doctor.driver, logger), "click on share to followup", Login_Doctor.driver, logger);
					Web_GeneralFunctions.wait(3);
					pdfSummaryContent = gpt.getPDFPage();
					pmt.clickOrgHeader();
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
					openInvisitDetailsModal();
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ovp.getCheckoutAndStartNewConsultationButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ovp.getStartFollowupConsultationButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(ovp.getStartFollowupConsultationButton(Login_Doctor.driver, logger), "click on start followup consultation button ", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger), "click on symptoms menu ", Login_Doctor.driver, logger);
					String copiedSymptom = Web_GeneralFunctions.getAttribute(sp.getSymptom(Login_Doctor.driver, SymptomTest.row, logger), "value", "extract symptom value", Login_Doctor.driver, logger);
					Web_GeneralFunctions.scrollElementToCenter(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), Login_Doctor.driver);
					String copiedDiagnosis = Web_GeneralFunctions.getAttribute(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), "value", "extract diagnosis text", Login_Doctor.driver, logger);
					Web_GeneralFunctions.scrollElementToCenter(pp.getDrugName(PrescriptionTest.row, Login_Doctor.driver, logger), Login_Doctor.driver);
					String copiedMedicine= Web_GeneralFunctions.getAttribute(pp.getDrugName(PrescriptionTest.row, Login_Doctor.driver, logger), "value", "get medicine name", Login_Doctor.driver, logger);
					Web_GeneralFunctions.scrollElementToCenter(ltp.getLabTest(Login_Doctor.driver, LabTestModuleTest.row, logger), Login_Doctor.driver);
					String copiedLabTest = Web_GeneralFunctions.getAttribute(ltp.getLabTest(Login_Doctor.driver, LabTestModuleTest.row, logger), "value", "get lab test name", Login_Doctor.driver, logger);
					moveToOpenConsultationsPage();
					String UHID = SheetsAPI.getDataProperties(Web_Testbase.input+".PatientUHID");
					Web_GeneralFunctions.sendkeys(ocp.getPatientIdField(Login_Doctor.driver, logger), UHID, "sending UHID to patient id search field", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
					String newConsultationDateTime =Web_GeneralFunctions.getText(ocp.getDateText(Login_Doctor.driver, logger), "get date time value", Login_Doctor.driver, logger);
					assertTrue(ocp.getListOfOpenConsultationRowElements(Login_Doctor.driver, logger).size()==1);
					Web_GeneralFunctions.click(ocp.getConsultingAppointmentViewButton(Login_Doctor.driver, logger), "click on view button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
					assertTrue(copiedSymptom.equalsIgnoreCase(symptomName));
					assertTrue(copiedDiagnosis.equalsIgnoreCase(diagnosisName));
					assertTrue(copiedMedicine.equalsIgnoreCase(medicineName));
					assertTrue(copiedLabTest.equalsIgnoreCase(labTestName));
					assertTrue(!newConsultationDateTime.trim().equals(openConsultationDateTime));
					openConsultationDateTime = newConsultationDateTime;
				
				}
				
				//SO_22 covered here
				@Test(groups= {"SoftCheckout"},priority =32)
				public synchronized void veriyPreviousVisitDiagnosisValuesCopiedToNewVisit()throws Exception{
					logger = Reports.extent.createTest("EMR Previous Diagnosis Values copied to new visit for follow up");
					SymptomTest.row =0;
					DiagnosisTest.row =0;
					PrescriptionTest.row =0;
					LabTestModuleTest.row =1;
					Web_GeneralFunctions.click(gpp.getGlobalPrintModule(Login_Doctor.driver, logger), "click on global print button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getShareToFollowUpButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(gpp.getShareToFollowUpButton(Login_Doctor.driver, logger), "click on share to followup", Login_Doctor.driver, logger);
					Web_GeneralFunctions.wait(3);
					pdfSummaryContent = gpt.getPDFPage();
					pmt.clickOrgHeader();
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
					openInvisitDetailsModal();
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ovp.getCheckoutAndStartNewConsultationButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ovp.getStartFollowupConsultationButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(ovp.getStartFollowupConsultationButton(Login_Doctor.driver, logger), "click on start followup consultation button ", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
					Web_GeneralFunctions.scrollElementToCenter(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), Login_Doctor.driver);
					String copiedDiagnosis = Web_GeneralFunctions.getAttribute(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), "value", "extract diagnosis text", Login_Doctor.driver, logger);
					moveToOpenConsultationsPage();
					String UHID = SheetsAPI.getDataProperties(Web_Testbase.input+".PatientUHID");
					Web_GeneralFunctions.sendkeys(ocp.getPatientIdField(Login_Doctor.driver, logger), UHID, "sending UHID to patient id search field", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
					String newConsultationDateTime =Web_GeneralFunctions.getText(ocp.getDateText(Login_Doctor.driver, logger), "get date time value", Login_Doctor.driver, logger);
					assertTrue(ocp.getListOfOpenConsultationRowElements(Login_Doctor.driver, logger).size()==1);
					assertTrue(!newConsultationDateTime.trim().equals(openConsultationDateTime));
					assertTrue(copiedDiagnosis.trim().equals(diagnosisName));
					openConsultationDateTime = newConsultationDateTime;
				}
				
				
				//SO_19 covered here
				@Test(groups= {"SoftCheckout"},priority =33)
				public synchronized void veriyPreviousVisitDetailsNotCopiedToNewVisit()throws Exception{
					logger = Reports.extent.createTest("EMR verify previous visit details Not copied to new visit for Checkout And New Consultation option");
					SymptomTest.row =0;
					DiagnosisTest.row =0;
					PrescriptionTest.row =0;
					LabTestModuleTest.row =1;
					pmt.clickOrgHeader();
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
					openInvisitDetailsModal();
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ovp.getCheckoutAndStartNewConsultationButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ovp.getStartFollowupConsultationButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(ovp.getCheckoutAndStartNewConsultationButton(Login_Doctor.driver, logger), "click on checkout and start new consultation button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger), "click on symptoms menu ", Login_Doctor.driver, logger);
					String copiedSymptom = Web_GeneralFunctions.getAttribute(sp.getSymptom(Login_Doctor.driver, SymptomTest.row, logger), "value", "extract symptom value", Login_Doctor.driver, logger);
					Web_GeneralFunctions.scrollElementToCenter(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), Login_Doctor.driver);
					String copiedDiagnosis = Web_GeneralFunctions.getAttribute(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), "value", "extract diagnosis text", Login_Doctor.driver, logger);
					Web_GeneralFunctions.scrollElementToCenter(pp.getDrugName(PrescriptionTest.row, Login_Doctor.driver, logger), Login_Doctor.driver);
					String copiedMedicine= Web_GeneralFunctions.getAttribute(pp.getDrugName(PrescriptionTest.row, Login_Doctor.driver, logger), "value", "get medicine name", Login_Doctor.driver, logger);
					Web_GeneralFunctions.scrollElementToCenter(ltp.getLabTest(Login_Doctor.driver, LabTestModuleTest.row, logger), Login_Doctor.driver);
					String copiedLabTest = Web_GeneralFunctions.getAttribute(ltp.getLabTest(Login_Doctor.driver, LabTestModuleTest.row, logger), "value", "get lab test name", Login_Doctor.driver, logger);
					pmt.clickOrgHeader();
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
					moveToOpenConsultationsPage();
					String UHID = SheetsAPI.getDataProperties(Web_Testbase.input+".PatientUHID");
					Web_GeneralFunctions.sendkeys(ocp.getPatientIdField(Login_Doctor.driver, logger), UHID, "sending UHID to patient id search field", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
					String newConsultationDateTime =Web_GeneralFunctions.getText(ocp.getDateText(Login_Doctor.driver, logger), "get date time value", Login_Doctor.driver, logger);
					assertTrue(ocp.getListOfOpenConsultationRowElements(Login_Doctor.driver, logger).size()==1);
					assertTrue(!newConsultationDateTime.trim().equals(openConsultationDateTime));
					assertTrue(Web_GeneralFunctions.getAttribute(ocp.getConsultationStatus(Login_Doctor.driver, logger), "title", "get status name", Login_Doctor.driver, logger).trim().equalsIgnoreCase("consulting"));
					assertTrue(copiedSymptom.isEmpty());
					assertTrue(copiedDiagnosis.isEmpty());
					assertTrue(copiedMedicine.isEmpty());
					assertTrue(copiedLabTest.isEmpty());
					openConsultationDateTime = newConsultationDateTime;
				}
				
				//SO_23 covered here
				@Test(groups= {"SoftCheckout"},priority =34)
				public synchronized void veriyPreviousVisitDiagnosisValueNotCopiedToNewVisit()throws Exception{
					logger = Reports.extent.createTest("EMR verify previous visit Diagnosis Value Not copied to new visit for Checkout And New Consultation option");
					SymptomTest.row =1;
					DiagnosisTest.row =0;
					PrescriptionTest.row =0;
					LabTestModuleTest.row =1;
					Web_GeneralFunctions.click(ocp.getConsultingAppointmentViewButton(Login_Doctor.driver, logger), "click on view button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(dp.moveToDiagnosisModule(Login_Doctor.driver, logger), "move to diagnosis module", Login_Doctor.driver, logger);
					Web_GeneralFunctions.clearWebElement(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), "click on diagnosis text field", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(dp.selectDiagnosisFromDropDown(Login_Doctor.driver, DiagnosisTest.row, "Typhoid", logger), "select diagnosis from dropdown ", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(gpp.getGlobalPrintModule(Login_Doctor.driver, logger), "click on global print", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.getMessage(Login_Doctor.driver, logger));
					Web_GeneralFunctions.scrollElementToCenter(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), Login_Doctor.driver);
					diagnosisName = Web_GeneralFunctions.getAttribute(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), "value", "extract diagnosis text", Login_Doctor.driver, logger);
					Web_GeneralFunctions.scrollElementToCenter(gpp.getShareToFollowUpButton(Login_Doctor.driver, logger), Login_Doctor.driver);
					Web_GeneralFunctions.click(gpp.getShareToFollowUpButton(Login_Doctor.driver, logger), "click on share to followup button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.wait(2);
					pdfSummaryContent = gpt.getPDFPage();
					pmt.clickOrgHeader();
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
					openInvisitDetailsModal();
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ovp.getCheckoutAndStartNewConsultationButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ovp.getStartFollowupConsultationButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(ovp.getCheckoutAndStartNewConsultationButton(Login_Doctor.driver, logger), "click on checkout and start new consultation button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
					Web_GeneralFunctions.scrollElementToCenter(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), Login_Doctor.driver);
					String diagnosisValue = Web_GeneralFunctions.getAttribute(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), "value", "extract diagnosis text", Login_Doctor.driver, logger);
					assertTrue(diagnosisValue.isEmpty());
				}
				
				//SO_29 covered here
				@Test(groups= {"SoftCheckout"},priority =35)
				public synchronized void veriyAbleToCloseOpenConsultationWithEmptyDiagnosis()throws Exception{
					logger = Reports.extent.createTest("EMR verify Able to close Previous consultation With empty diagnosis");
					SymptomTest.row =1;
					DiagnosisTest.row =0;
					PrescriptionTest.row =0;
					LabTestModuleTest.row =1;
					pmt.clickOrgHeader();
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
					openInvisitDetailsModal();
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ovp.getCheckoutAndStartNewConsultationButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ovp.getStartFollowupConsultationButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(ovp.getCheckoutAndStartNewConsultationButton(Login_Doctor.driver, logger), "click on checkout and start new consultation button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
					pmt.clickOrgHeader();
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
					moveToOpenConsultationsPage();
					String UHID = SheetsAPI.getDataProperties(Web_Testbase.input+".PatientUHID");
					Web_GeneralFunctions.sendkeys(ocp.getPatientIdField(Login_Doctor.driver, logger), UHID, "sending UHID to patient id search field", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
					String newConsultationDateTime =Web_GeneralFunctions.getText(ocp.getDateText(Login_Doctor.driver, logger), "get date time value", Login_Doctor.driver, logger);
					assertTrue(ocp.getListOfOpenConsultationRowElements(Login_Doctor.driver, logger).size()==1);
					assertTrue(!newConsultationDateTime.trim().equals(openConsultationDateTime));
					assertTrue(Web_GeneralFunctions.getAttribute(ocp.getConsultationStatus(Login_Doctor.driver, logger), "title", "get status name", Login_Doctor.driver, logger).trim().equalsIgnoreCase("consulting"));
					assertTrue(Web_GeneralFunctions.getAttribute(ocp.getConsultationStatus(Login_Doctor.driver, logger), "title", "get status name", Login_Doctor.driver, logger).trim().equalsIgnoreCase("consulting"));
					openConsultationDateTime = newConsultationDateTime;
				}
				
				
				//SO_40 covered here
				@Test(groups= {"SoftCheckout"},priority =36)
				public synchronized void verifyAbleToCloseConsultationFromOpenConsultationsPage()throws Exception{
					logger = Reports.extent.createTest("EMR verify Able to close Consultations from open Consultations Page");
					String UHID = SheetsAPI.getDataProperties(Web_Testbase.input+".PatientUHID");
					Web_GeneralFunctions.sendkeys(ocp.getPatientIdField(Login_Doctor.driver, logger), UHID, "sending UHID to patient id search field", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(ocp.getConsultingAppointmentCheckoutButton(Login_Doctor.driver, logger), "click on checkout button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver,ocp.getCloseConsultationConfirmationPopup(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getNoInCloseConsultationModal(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getYesInCloseConsultationModal(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(ocp.getYesInCloseConsultationModal(Login_Doctor.driver, logger), "click yes in confirmation modal", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getPatientNameSearchField(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getPatientIdField(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getNoOpenConsultationsText(Login_Doctor.driver, logger));
					assertTrue(ocp.getNoOpenConsultationsText(Login_Doctor.driver, logger).isDisplayed());
				}
				
				//SO_53 covered here
				@Test(groups= {"SoftCheckout"},priority =37)
				public synchronized void verifyTimeLineNotDisplayedForOldVisit()throws Exception{
					logger = Reports.extent.createTest("EMR verify Time Line Not displayed on Consultation page");
					pmt.clickOrgHeader();
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
					registerAndBookApppointment();
					newPatientId = Web_GeneralFunctions.getText(ocp.getPatientId(Login_Doctor.driver, logger), "extract newly created patient id", Login_Doctor.driver, logger).trim();
					DiagnosisTest.row=0;
					PrescriptionTest.row=0;
					SymptomTest.row=0;
					LabTestModuleTest.row=1;
					PrescriptionPage prescription = new PrescriptionPage();
					PrescriptionTest pTest = new PrescriptionTest();
					Web_GeneralFunctions.click(sp.selectSymptomFromDropDown(Login_Doctor.driver, SymptomTest.row, "pain", logger), "select any one symptom", Login_Doctor.driver, logger);
					Web_GeneralFunctions.scrollElementToCenter(ltp.getLabTest(Login_Doctor.driver, LabTestModuleTest.row, logger), Login_Doctor.driver);
					Web_GeneralFunctions.click(ltp.selectLabTestFromDropDown(Login_Doctor.driver, LabTestModuleTest.row,"Thyroid", logger), "select lab test from dropdown", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(ocp.getExaminationInConsultationPage(Login_Doctor.driver, logger), "click on examination link", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.getMessage(Login_Doctor.driver, logger));
					Web_GeneralFunctions.wait(3);
					Web_GeneralFunctions.click(dp.moveToDiagnosisModule(Login_Doctor.driver, logger), "click on diagnosis", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.getMessage(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(dp.selectDiagnosisFromDropDown(Login_Doctor.driver, DiagnosisTest.row, "Typhoid", logger), "select diagnosis from dropdown ", Login_Doctor.driver, logger);
					Web_GeneralFunctions.scrollElementToCenter(ocp.getPrescriptionHeaderInConsultationPage(Login_Doctor.driver, logger), Login_Doctor.driver);
					Web_GeneralFunctions.click(prescription.getClinicSpecificCheckBox(Login_Doctor.driver, logger), "deselect clinic specific checkbox", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(prescription.selectMedicineFromDropDown("tablet", PrescriptionTest.row, Login_Doctor.driver, logger),"select one medicine from dropdown",Login_Doctor.driver,logger);
					pTest.selectFrequnecyFromMasterList();
					pTest.enterDuration();
					Web_GeneralFunctions.scrollElementToCenter(gap.getGeneralAdviceTextArea(Login_Doctor.driver, logger), Login_Doctor.driver);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gap.getGeneralAdviceTextArea(Login_Doctor.driver, logger));
					gat.enterGeneralAdvice();
					Web_GeneralFunctions.click(gpp.getGlobalPrintModule(Login_Doctor.driver, logger), "click on global print", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.getMessage(Login_Doctor.driver, logger));
					Web_GeneralFunctions.scrollElementToCenter(ocp.getCaseSheetNotesElement(Login_Doctor.driver, logger), Login_Doctor.driver);
					caseSheetDetails = RandomStringUtils.randomAlphabetic(25);
					Web_GeneralFunctions.sendkeys(ocp.getCaseSheetNotesElement(Login_Doctor.driver, logger), caseSheetDetails, "send notes text to case sheet", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(dp.moveToDiagnosisModule(Login_Doctor.driver, logger), "move to diagnosis module", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.getMessage(Login_Doctor.driver, logger));
					Web_GeneralFunctions.wait(2);
					SymptomTest.row = 1;
					symptomName = Web_GeneralFunctions.getAttribute(sp.getSymptom(Login_Doctor.driver, SymptomTest.row, logger), "value", "extract symptom value", Login_Doctor.driver, logger);
					labTestName = Web_GeneralFunctions.getAttribute(ltp.getLabTest(Login_Doctor.driver, LabTestModuleTest.row, logger), "value", "get lab test name", Login_Doctor.driver, logger);
					Web_GeneralFunctions.scrollElementToCenter(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), Login_Doctor.driver);
					diagnosisName = Web_GeneralFunctions.getAttribute(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), "value", "extract diagnosis text", Login_Doctor.driver, logger);
					Web_GeneralFunctions.scrollElementToCenter(prescription.getDrugName(PrescriptionTest.row, Login_Doctor.driver, logger), Login_Doctor.driver);
					medicineName = Web_GeneralFunctions.getAttribute(prescription.getDrugName(PrescriptionTest.row, Login_Doctor.driver, logger), "value", "get medicine name", Login_Doctor.driver, logger);
					Web_GeneralFunctions.scrollElementToCenter(gap.getGeneralAdviceTextArea(Login_Doctor.driver, logger), Login_Doctor.driver);
					generalAdviceText = Web_GeneralFunctions.getText(gap.getGeneralAdviceTextArea(Login_Doctor.driver, logger), "extract generalAdvice text", Login_Doctor.driver, logger);
					Web_GeneralFunctions.scrollElementToCenter(ocp.getFollowupLablel(Login_Doctor.driver, logger), Login_Doctor.driver);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getFollowupCheckbox(Login_Doctor.driver, logger));
					if(!ocp.getFollowupInputField(Login_Doctor.driver, logger).isEnabled()) {
						Web_GeneralFunctions.click(ocp.getFollowupCheckbox(Login_Doctor.driver, logger), "click on followup checkbox", Login_Doctor.driver, logger);
					}
					Web_GeneralFunctions.click(gpp.getGlobalPrintModule(Login_Doctor.driver, logger), "click on global print link in side nav", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.getMessage(Login_Doctor.driver, logger));
					ConsultationTest.UHID=newPatientId;
					openInvisitDetailsModal();
					Web_GeneralFunctions.click(ovp.getStartNewConsultationButton(Login_Doctor.driver, logger), "click on start new consultation", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
					assertTrue(!Web_GeneralFunctions.isDisplayed(ocp.getPatientTimeLine(Login_Doctor.driver, logger)));
				}
				
				
				//SO_50 covered here
				@Test(groups= {"SoftCheckout"},priority =38)
				public synchronized void verifySavedDetailsDispalyedInTimeline()throws Exception{
					logger = Reports.extent.createTest("EMR verify saved consultation details displayed in timeline");
					pmt.clickOrgHeader();
					moveToOpenConsultationsPage();
					Web_GeneralFunctions.sendkeys(ocp.getPatientIdField(Login_Doctor.driver, logger), newPatientId, "search with newly created patient id", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(ocp.getSecondAppointmentCheckoutButton(Login_Doctor.driver, logger), "click on checkout button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver,ocp.getYesInCloseConsultationModal(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(ocp.getYesInCloseConsultationModal(Login_Doctor.driver, logger), "click on yes", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.sendkeys(ocp.getPatientIdField(Login_Doctor.driver, logger), newPatientId, "search with newly created patient id", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(ocp.getConsultingAppointmentViewButton(Login_Doctor.driver, logger), "click on view button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(gpp.getGlobalPrintModule(Login_Doctor.driver, logger),"click on global print ", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getShareToFollowUpButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(gpp.getShareToFollowUpButton(Login_Doctor.driver, logger),"click on share to followup", Login_Doctor.driver, logger);
					Web_GeneralFunctions.wait(3);
					pdfSummaryContent = gpt.getPDFPage();
					pmt.clickOrgHeader();
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
					ConsultationTest.UHID=newPatientId;
					openInvisitDetailsModal();
					Web_GeneralFunctions.click(ovp.getStartFollowupConsultationButton(Login_Doctor.driver, logger), "click on start followup button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(ocp.getLatestTimeLine(Login_Doctor.driver, logger),"click on latest timeline", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getPrintAllButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getPrintSummaryButton(Login_Doctor.driver, logger));
					String generalAdvice = Web_GeneralFunctions.getText(ocp.getGeneralAdviceInTimeLineSummary(Login_Doctor.driver, logger),"extract general advice" , Login_Doctor.driver, logger).trim();
					String diagnosis = Web_GeneralFunctions.getText(ocp.getDiagnosisInTimeLineSummary(Login_Doctor.driver, logger), "extract diagnosis", Login_Doctor.driver, logger);
					String medicine = Web_GeneralFunctions.getText(ocp.getMedicineInTimeLineSummary(Login_Doctor.driver, logger), "extract medicine", Login_Doctor.driver, logger);
					String symptom = Web_GeneralFunctions.getText(ocp.getSymptomInTimeLineSummary(Login_Doctor.driver, logger), "extract symptom", Login_Doctor.driver, logger);
					String caseSheet = Web_GeneralFunctions.getText(ocp.getCaseSheetTextInTimeLineSummary(Login_Doctor.driver, logger), "extract case sheet", Login_Doctor.driver, logger);
					String followup = Web_GeneralFunctions.getText(ocp.getFollowUpInTimeLineSummary(Login_Doctor.driver, logger), "extract followup", Login_Doctor.driver, logger);
					String labTest= Web_GeneralFunctions.getText(ocp.getLabTestInTimeLineSummary(Login_Doctor.driver, logger), "extract labTest", Login_Doctor.driver, logger);
					assertTrue(generalAdvice.trim().equalsIgnoreCase(generalAdviceText));
					assertTrue(diagnosis.trim().equalsIgnoreCase(diagnosisName));
					assertTrue(medicine.trim().equalsIgnoreCase(medicineName));
					assertTrue(symptom.trim().equalsIgnoreCase(symptomName));
					assertTrue(caseSheet.trim().equalsIgnoreCase(caseSheetDetails));
					assertTrue(followup.trim().equalsIgnoreCase("1 Week(s)"));
					assertTrue(labTest.trim().equalsIgnoreCase(labTestName));
				}
				
				
				//SO_51 covered here
				@Test(groups= {"SoftCheckout"},priority =39)
				public synchronized void verifySummaryPdfDisplayedInTimeline()throws Exception{
					logger = Reports.extent.createTest("EMR verify Summary Pdf displayed in timeline");
					Web_GeneralFunctions.click(ocp.getPrintAllButton(Login_Doctor.driver, logger), "click on print all button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.wait(3);
					String summaryPdf = gpt.getPDFPage();
					assertTrue(summaryPdf.equalsIgnoreCase(pdfSummaryContent));
					Web_GeneralFunctions.click(ocp.getPrintSummaryButton(Login_Doctor.driver, logger), "click on print summary button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.wait(3);
					/*failing due to manual issue*/
					//String summaryPdf2 = gpt.getPDFPage();
					//assertTrue(summaryPdf2.equalsIgnoreCase(pdfSummaryContent));
				}
				
				//SO_52 covered here
				@Test(groups= {"SoftCheckout"},priority =40)
				public synchronized void verifySummaryPdfDisplayedInTimelineForNewConsultation()throws Exception{
					logger = Reports.extent.createTest("EMR verify Summary Pdf displayed in timeline for New consultation");
					pmt.clickOrgHeader();
					ConsultationTest.UHID=newPatientId;
					openInvisitDetailsModal();
					Web_GeneralFunctions.click(ovp.getCheckoutAndStartNewConsultationButton(Login_Doctor.driver, logger), "click on checkout and start new consultation", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(ocp.getAllTimeLineElements(Login_Doctor.driver, logger).get(2),"click on latest timeline", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getPrintAllButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getPrintSummaryButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(ocp.getPrintAllButton(Login_Doctor.driver, logger), "click on print all button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.wait(4);
					String summaryPdf = gpt.getPDFPage();
					assertTrue(summaryPdf.equalsIgnoreCase(pdfSummaryContent));
					Web_GeneralFunctions.click(ocp.getPrintSummaryButton(Login_Doctor.driver, logger), "click on print summary button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.wait(4);
					String summaryPdf2 = gpt.getPDFPage();
					assertTrue(summaryPdf2.equalsIgnoreCase(pdfSummaryContent));
					pmt.clickOrgHeader();
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
					moveToOpenConsultationsPage();
					Web_GeneralFunctions.sendkeys(ocp.getPatientIdField(Login_Doctor.driver, logger), newPatientId, "search with newly created patient id", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(ocp.getSecondAppointmentCheckoutButton(Login_Doctor.driver, logger), "click on checkout button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver,ocp.getYesInCloseConsultationModal(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(ocp.getYesInCloseConsultationModal(Login_Doctor.driver, logger), "click on yes", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.sendkeys(ocp.getPatientIdField(Login_Doctor.driver, logger), newPatientId, "search with newly created patient id", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(ocp.getFirstOpenConsultationCheckoutButton(Login_Doctor.driver, logger), "click on checkout button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver,ocp.getYesInCloseConsultationModal(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(ocp.getYesInCloseConsultationModal(Login_Doctor.driver, logger), "click on yes", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
			}
				
				//SO_45 covered here
				@Test(groups= {"SoftCheckout"},priority =41)
				public synchronized void verifyInvisitDetailsPopupNotDisplayed()throws Exception{
					logger = Reports.extent.createTest("EMR verify Invisit details popup not displayed for new visit ");
					pmt.clickOrgHeader();
					ConsultationTest.UHID=SheetsAPI.getDataProperties(Web_Testbase.input+".PatientUHID");
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
					openInvisitDetailsModal();
					assertTrue(!Web_GeneralFunctions.isDisplayed(ovp.getCheckoutAndStartNewConsultationButton(Login_Doctor.driver, logger)));
					assertTrue(!Web_GeneralFunctions.isDisplayed(ovp.getStartFollowupConsultationButton(Login_Doctor.driver, logger)));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
					assertTrue(Web_GeneralFunctions.isDisplayed(lrp.moveToLabResultModule(Login_Doctor.driver, logger)));
					assertTrue(Web_GeneralFunctions.isDisplayed(rp.moveToReferral(Login_Doctor.driver, logger)));
					assertTrue(Web_GeneralFunctions.isDisplayed(sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger)));
				}
				
				//SO_03 covered here
				@Test(groups= {"SoftCheckout"},priority =42)
				public synchronized void verifySoftCheckoutPdfStoredInHealthVault()throws Exception{
					logger = Reports.extent.createTest("EMR validate pdf sent to consumer health vault");
					DiagnosisTest.row =0;
					Web_GeneralFunctions.click(dp.moveToDiagnosisModule(Login_Doctor.driver, logger), "move to diagnosis module", Login_Doctor.driver, logger);
					Web_GeneralFunctions.clearWebElement(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), "click on diagnosis text field", Login_Doctor.driver, logger);
				Web_GeneralFunctions.click(dp.selectDiagnosisFromDropDown(Login_Doctor.driver, DiagnosisTest.row, "Typhoid", logger), "select diagnosis from dropdown ", Login_Doctor.driver, logger);
				Web_GeneralFunctions.click(gpp.getGlobalPrintModule(Login_Doctor.driver, logger), "click on global print", Login_Doctor.driver, logger);
				Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.getMessage(Login_Doctor.driver, logger));
				diagnosisName = Web_GeneralFunctions.getAttribute(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), "value", "extract diagnosis text", Login_Doctor.driver, logger);
				diagnosisName = diagnosisName.substring(6);
				Web_GeneralFunctions.click(gpp.getGlobalPrintModule(Login_Doctor.driver, logger), "click on global print", Login_Doctor.driver, logger);
				Web_GeneralFunctions.click(gpp.getShareToFollowUpButton(Login_Doctor.driver, logger), "click on share to followup button", Login_Doctor.driver, logger);
				Web_GeneralFunctions.wait(4);
				pdfSummaryContent = gpt.getPDFPage();
				openConsumerApp();
				Web_GeneralFunctions.switchTabs("switch to consumer app", 1, Login_Doctor.driver, logger);
				Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getLogin(Login_Doctor.driver, logger));
				Web_GeneralFunctions.click(ocp.getLogin(Login_Doctor.driver, logger), "click on login tab nav", Login_Doctor.driver, logger);
				Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getMobielNoTextField(Login_Doctor.driver, logger));
				Web_GeneralFunctions.sendkeys(ocp.getMobielNoTextField(Login_Doctor.driver, logger), "7981489707", "enter mobile no", Login_Doctor.driver, logger);
				Web_GeneralFunctions.wait(2);
				Web_GeneralFunctions.click(ocp.getContinueButton(Login_Doctor.driver, logger), "click on continue button", Login_Doctor.driver, logger);
				Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getResendOTP(Login_Doctor.driver, logger));
				Web_GeneralFunctions.click(ocp.getResendOTP(Login_Doctor.driver, logger), "click on resend otp", Login_Doctor.driver, logger);
				OTP otp = new OTP();
				String mobileNo = SheetsAPI.getDataProperties(Web_Testbase.input+".PatMobileNumber");
				String passcode = otp.getStagingOTP(mobileNo);
				Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getEnterOTPHeader(Login_Doctor.driver, logger));
				Web_GeneralFunctions.sendkeys(ocp.getOtpTextField(Login_Doctor.driver, logger), passcode, "sending otp text", Login_Doctor.driver, logger);
				Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getHealthLocker(Login_Doctor.driver, logger));
				Web_GeneralFunctions.click(ocp.getHealthLocker(Login_Doctor.driver, logger), "click on health locker", Login_Doctor.driver, logger);
				Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getHealthLockerPinField(Login_Doctor.driver, logger));
				Web_GeneralFunctions.sendkeys(ocp.getHealthLockerPinField(Login_Doctor.driver, logger), "0000", "enter health locker pin", Login_Doctor.driver, logger);
				Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getConsultationAndPrescription(Login_Doctor.driver, logger));
				Web_GeneralFunctions.click(ocp.getConsultationAndPrescription(Login_Doctor.driver, logger), "click on consultation and prescription", Login_Doctor.driver, logger);
				Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getPlusFloatingButton(Login_Doctor.driver, logger));
				Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getFirstConsultationRow(Login_Doctor.driver, logger));
				Web_GeneralFunctions.click(ocp.getFirstConsultationRow(Login_Doctor.driver, logger), "click on first row", Login_Doctor.driver, logger);
				Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getPdfFileButton(Login_Doctor.driver, logger));
				Web_GeneralFunctions.click(ocp.getPdfFileButton(Login_Doctor.driver, logger), "click on pdf file ", Login_Doctor.driver, logger);
				Web_GeneralFunctions.wait(4);
				String pdfContent = getPDFContent();
				Web_GeneralFunctions.closeDriver(Login_Doctor.driver);
				Web_GeneralFunctions.switchTabs("switch to emr tab", 0, Login_Doctor.driver, logger);
				assertTrue(pdfContent.contains(diagnosisName));
				assertTrue(pdfContent.equals(pdfSummaryContent));
			}
	
	//SO_18 covered here
	@Test(groups= {"SoftCheckout"},priority =43)
	public synchronized void verifySummaryPdfNotSentToConsumer()throws Exception{
		logger = Reports.extent.createTest("EMR verify pdf not sent to consumer for no change in consultation details");
		pmt.clickOrgHeader();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
		openInvisitDetailsModal();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ovp.getCheckoutAndStartNewConsultationButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ovp.getStartFollowupConsultationButton(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(ovp.getStartFollowupConsultationButton(Login_Doctor.driver, logger), "click on start followup button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
		openConsumerApp();
		Web_GeneralFunctions.switchTabs("switch to consumer app", 1, Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getHealthLocker(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(ocp.getHealthLocker(Login_Doctor.driver, logger), "click on health locker", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getHealthLockerPinField(Login_Doctor.driver, logger));
			Web_GeneralFunctions.sendkeys(ocp.getHealthLockerPinField(Login_Doctor.driver, logger), "0000", "enter health locker pin", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getConsultationAndPrescription(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(ocp.getConsultationAndPrescription(Login_Doctor.driver, logger), "click on consultation and prescription", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getPlusFloatingButton(Login_Doctor.driver, logger));
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getFirstConsultationRow(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(ocp.getFirstConsultationRow(Login_Doctor.driver, logger), "click on first row", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getPdfFileButton(Login_Doctor.driver, logger));
			assertTrue(ocp.getPdfFilesAsList(Login_Doctor.driver, logger).size()==1);
			Web_GeneralFunctions.closeDriver(Login_Doctor.driver);
			Web_GeneralFunctions.switchTabs("switch to emr tab", 0, Login_Doctor.driver, logger);
		}
	
	//SO_43 covered here
		@Test(groups= {"SoftCheckout"},priority =44)
		public synchronized void verifyPdfSentToConsumerAfterClosingVisit()throws Exception{
			logger = Reports.extent.createTest("EMR verify Pdf sent to consumer after closing visit");
			pmt.clickOrgHeader();
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
			moveToOpenConsultationsPage();
			String UHID = SheetsAPI.getDataProperties(Web_Testbase.input+".PatientUHID");
			Web_GeneralFunctions.sendkeys(ocp.getPatientIdField(Login_Doctor.driver, logger), UHID, "sending UHID to patient id search field", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(ocp.getFirstOpenConsultationViewButton(Login_Doctor.driver, logger), "click on view button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
			Web_GeneralFunctions.scrollElementToCenter(ocp.getPrescriptionHeaderInConsultationPage(Login_Doctor.driver, logger), Login_Doctor.driver);
			Web_GeneralFunctions.click(pp.getClinicSpecificCheckBox(Login_Doctor.driver, logger), "deselect clinic specific checkbox", Login_Doctor.driver, logger);
			Web_GeneralFunctions.clearWebElement(pp.getDrugName(PrescriptionTest.row, Login_Doctor.driver, logger), "click on prescription text field", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(pp.selectMedicineFromDropDown("tablet", PrescriptionTest.row, Login_Doctor.driver, logger),"select one medicine from dropdown",Login_Doctor.driver,logger);
			pt.selectFrequnecyFromMasterList();
			pt.enterDuration();
			Web_GeneralFunctions.click(gpp.getGlobalPrintModule(Login_Doctor.driver, logger), "click on global print", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.getMessage(Login_Doctor.driver, logger));
			Web_GeneralFunctions.scrollElementToCenter(pp.getDrugName(PrescriptionTest.row, Login_Doctor.driver, logger), Login_Doctor.driver);
			diagnosisName = Web_GeneralFunctions.getAttribute(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), "value", "extract diagnosis text", Login_Doctor.driver, logger);
			diagnosisName = diagnosisName.substring(6);
			medicineName = Web_GeneralFunctions.getAttribute(pp.getDrugName(PrescriptionTest.row, Login_Doctor.driver, logger), "value", "get medicine name", Login_Doctor.driver, logger);
			moveToOpenConsultationsPage();
			Web_GeneralFunctions.sendkeys(ocp.getPatientIdField(Login_Doctor.driver, logger), UHID, "sending UHID to patient id search field", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(ocp.getFirstOpenConsultationCheckoutButton(Login_Doctor.driver, logger), "click on checkout button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getYesInCloseConsultationModal(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(ocp.getYesInCloseConsultationModal(Login_Doctor.driver, logger), "click on yes button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getNoOpenConsultationsText(Login_Doctor.driver, logger));
			openConsumerApp();
			Web_GeneralFunctions.switchTabs("switch to consumer app", 1, Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getHealthLocker(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(ocp.getHealthLocker(Login_Doctor.driver, logger), "click on health locker", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getHealthLockerPinField(Login_Doctor.driver, logger));
			Web_GeneralFunctions.sendkeys(ocp.getHealthLockerPinField(Login_Doctor.driver, logger), "0000", "enter health locker pin", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getConsultationAndPrescription(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(ocp.getConsultationAndPrescription(Login_Doctor.driver, logger), "click on consultation and prescription", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getPlusFloatingButton(Login_Doctor.driver, logger));
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getFirstConsultationRow(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(ocp.getFirstConsultationRow(Login_Doctor.driver, logger), "click on first row", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getPdfFileButton(Login_Doctor.driver, logger));
			assertTrue(ocp.getPdfFilesAsList(Login_Doctor.driver, logger).size()==1);
			Web_GeneralFunctions.click(ocp.getPdfFilesAsList(Login_Doctor.driver, logger).get(0), "click on latest pdf", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(4);
			String pdfContent = getPDFContent();
			Web_GeneralFunctions.closeDriver(Login_Doctor.driver);
			Web_GeneralFunctions.switchTabs("switch to emr tab", 0, Login_Doctor.driver, logger);
			assertTrue(pdfContent.contains(diagnosisName));
			assertTrue(pdfContent.contains(medicineName));
		}
	
	//SO_27 covered here
	@Test(groups= {"SoftCheckout"},priority =45)
	public synchronized void verifyPdfNotSentToConsumerAfterClosingVisit()throws Exception{
		logger = Reports.extent.createTest("EMR verify Pdf Not sent to consumer after closing visit");
		pmt.clickOrgHeader();
		DiagnosisTest.row =0;
		LabTestModuleTest.row=1;
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
		ConsultationTest.UHID=SheetsAPI.getDataProperties(Web_Testbase.input+".PatientUHID");
		openInvisitDetailsModal();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.scrollElementToCenter(ltp.getLabTest(Login_Doctor.driver, LabTestModuleTest.row, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(ltp.selectLabTestFromDropDown(Login_Doctor.driver, LabTestModuleTest.row,"Thyroid", logger), "select lab test from dropdown", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ocp.getExaminationInConsultationPage(Login_Doctor.driver, logger), "click on examination link", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.getMessage(Login_Doctor.driver, logger));
		Web_GeneralFunctions.scrollElementToCenter(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(dp.selectDiagnosisFromDropDown(Login_Doctor.driver, DiagnosisTest.row, "Typhoid", logger), "select diagnosis from dropdown ", Login_Doctor.driver, logger);
		Web_GeneralFunctions.scrollElementToCenter(ocp.getPrescriptionHeaderInConsultationPage(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(pp.getClinicSpecificCheckBox(Login_Doctor.driver, logger), "deselect clinic specific checkbox", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(pp.selectMedicineFromDropDown("tablet", PrescriptionTest.row, Login_Doctor.driver, logger),"select one medicine from dropdown",Login_Doctor.driver,logger);
		pt.selectFrequnecyFromMasterList();
		pt.enterDuration();
		Web_GeneralFunctions.click(gpp.getGlobalPrintModule(Login_Doctor.driver, logger), "click on global print", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.getMessage(Login_Doctor.driver, logger));
		diagnosisName = Web_GeneralFunctions.getAttribute(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), "value", "extract diagnosis text", Login_Doctor.driver, logger);
		diagnosisName = diagnosisName.substring(6);
		medicineName = Web_GeneralFunctions.getAttribute(pp.getDrugName(PrescriptionTest.row, Login_Doctor.driver, logger), "value", "get medicine name", Login_Doctor.driver, logger);
		labTestName = Web_GeneralFunctions.getAttribute(ltp.getLabTest(Login_Doctor.driver, LabTestModuleTest.row, logger), "value", "get lab test name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.scrollElementToCenter(gpp.getShareToFollowUpButton(Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.click(gpp.getShareToFollowUpButton(Login_Doctor.driver, logger), "click on share to followup button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(4);
		pdfSummaryContent = gpt.getPDFPage();
		openConsumerApp();
		Web_GeneralFunctions.switchTabs("switch to consumer app", 1, Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getHealthLocker(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(ocp.getHealthLocker(Login_Doctor.driver, logger), "click on health locker", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getHealthLockerPinField(Login_Doctor.driver, logger));
			Web_GeneralFunctions.sendkeys(ocp.getHealthLockerPinField(Login_Doctor.driver, logger), "0000", "enter health locker pin", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getConsultationAndPrescription(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(ocp.getConsultationAndPrescription(Login_Doctor.driver, logger), "click on consultation and prescription", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getPlusFloatingButton(Login_Doctor.driver, logger));
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getFirstConsultationRow(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(ocp.getFirstConsultationRow(Login_Doctor.driver, logger), "click on first row", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getPdfFileButton(Login_Doctor.driver, logger));
			assertTrue(ocp.getPdfFilesAsList(Login_Doctor.driver, logger).size()==1);
			Web_GeneralFunctions.click(ocp.getPdfFilesAsList(Login_Doctor.driver, logger).get(0), "click on latest pdf", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(3);
			String pdfContent = getPDFContent();
			Web_GeneralFunctions.closeDriver(Login_Doctor.driver);
			Web_GeneralFunctions.switchTabs("switch to emr tab", 0, Login_Doctor.driver, logger);
			assertTrue(pdfContent.equals(pdfSummaryContent));
			pmt.clickOrgHeader();
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
			moveToOpenConsultationsPage();
			String UHID=SheetsAPI.getDataProperties(Web_Testbase.input+".PatientUHID");
			Web_GeneralFunctions.sendkeys(ocp.getPatientIdField(Login_Doctor.driver, logger), UHID, "sending UHID to patient id search field", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(ocp.getFirstOpenConsultationCheckoutButton(Login_Doctor.driver, logger), "click on checkout button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getYesInCloseConsultationModal(Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(ocp.getYesInCloseConsultationModal(Login_Doctor.driver, logger),"click Yes on modal", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
			openConsumerApp();
			Web_GeneralFunctions.switchTabs("switch to consumer app", 1, Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getHealthLocker(Login_Doctor.driver, logger));
				Web_GeneralFunctions.click(ocp.getHealthLocker(Login_Doctor.driver, logger), "click on health locker", Login_Doctor.driver, logger);
				Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getHealthLockerPinField(Login_Doctor.driver, logger));
				Web_GeneralFunctions.sendkeys(ocp.getHealthLockerPinField(Login_Doctor.driver, logger), "0000", "enter health locker pin", Login_Doctor.driver, logger);
				Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getConsultationAndPrescription(Login_Doctor.driver, logger));
				Web_GeneralFunctions.click(ocp.getConsultationAndPrescription(Login_Doctor.driver, logger), "click on consultation and prescription", Login_Doctor.driver, logger);
				Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getPlusFloatingButton(Login_Doctor.driver, logger));
				Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getFirstConsultationRow(Login_Doctor.driver, logger));
				Web_GeneralFunctions.click(ocp.getFirstConsultationRow(Login_Doctor.driver, logger), "click on first row", Login_Doctor.driver, logger);
				if(ocp.getSessionExpireModal(Login_Doctor.driver, logger)!=null) {
					Web_GeneralFunctions.sendkeys(ocp.getHealthLockerPinField(Login_Doctor.driver, logger), "0000", "enter health locker pin", Login_Doctor.driver, logger);
				}
				Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getPdfFileButton(Login_Doctor.driver, logger));
				assertTrue(ocp.getPdfFilesAsList(Login_Doctor.driver, logger).size()==1);
				Web_GeneralFunctions.closeDriver(Login_Doctor.driver);
				Web_GeneralFunctions.switchTabs("switch to emr tab", 0, Login_Doctor.driver, logger);
			}
	
				//SO_46 covered here
				@Test(groups= {"SoftCheckout"},priority =46)
				public synchronized void verifyClosedVisitNotDispalyedInOpenConsultationsModule()throws Exception{
					logger = Reports.extent.createTest("EMR verify Closed visit Not displayed in open visit module");
					ConsultationTest.UHID= SheetsAPI.getDataProperties(Web_Testbase.input+".PatientUHID");
					pmt.clickOrgHeader();
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
					openInvisitDetailsModal();
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
					moveToOpenConsultationsPage();
					String UHID = SheetsAPI.getDataProperties(Web_Testbase.input+".PatientUHID");
					Web_GeneralFunctions.sendkeys(ocp.getPatientIdField(Login_Doctor.driver, logger), UHID, "sending UHID to patient id search field", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
					String oldConsultationDateTime =Web_GeneralFunctions.getText(ocp.getDateText(Login_Doctor.driver, logger), "get date time value", Login_Doctor.driver, logger);
					pmt.clickOrgHeader();
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
					openInvisitDetailsModal();
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ovp.getCheckoutAndStartNewConsultationButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ovp.getStartFollowupConsultationButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(ovp.getCheckoutAndStartNewConsultationButton(Login_Doctor.driver, logger), "click on checkout and start new consultation button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
					pmt.clickOrgHeader();
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
					moveToOpenConsultationsPage();
					Web_GeneralFunctions.sendkeys(ocp.getPatientIdField(Login_Doctor.driver, logger), UHID, "sending UHID to patient id search field", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(ocp.getSearchButton(Login_Doctor.driver, logger), "click on search button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getSearchButton(Login_Doctor.driver, logger));
					openConsultationDateTime =Web_GeneralFunctions.getText(ocp.getDateText(Login_Doctor.driver, logger), "get date time value", Login_Doctor.driver, logger);
					assertTrue(ocp.getListOfOpenConsultationRowElements(Login_Doctor.driver, logger).size()==1);
					assertTrue(!openConsultationDateTime.trim().equalsIgnoreCase(oldConsultationDateTime));
				}
				
				//SO_30 covered here
				@Test(groups= {"SoftCheckout"},priority =47)
				public synchronized void verifyThreeButtonsDisplayedOnOpenVisitModule()throws Exception{
					logger = Reports.extent.createTest("EMR Verify Three buttons displayed on open visit module");	
					DiagnosisTest.row =0;
					openInvisitDetailsModal();
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ovp.getCheckoutAndStartNewConsultationButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ovp.getStartFollowupConsultationButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(ovp.getStartNewConsultationButton(Login_Doctor.driver, logger), "click on start new consultation button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(dp.moveToDiagnosisModule(Login_Doctor.driver, logger), "move to diagnosis module", Login_Doctor.driver, logger);
					Web_GeneralFunctions.clearWebElement(dp.getDiagnosisTextField(DiagnosisTest.row, Login_Doctor.driver, logger), "click on diagnosis text field", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(dp.selectDiagnosisFromDropDown(Login_Doctor.driver, DiagnosisTest.row, "Typhoid", logger), "select diagnosis from dropdown ", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(gpp.getGlobalPrintModule(Login_Doctor.driver, logger), "click on global print", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, dp.getMessage(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(gpp.getCheckOutButton(Login_Doctor.driver, logger), "click on checkout button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver,gpp.getViewSummaryCloseButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(gpp.getViewSummaryCloseButton(Login_Doctor.driver, logger), "click on close button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, bp.getEmrCreateBillServiceSectionText(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getBillingHeader(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getCreateBillHeader(Login_Doctor.driver, logger));
					Web_GeneralFunctions.sendkeys(bp.getEmrCreateBillServiceColumnTextBox(Login_Doctor.driver, logger, "service", 0), "test", "sending service text", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, bp.getEmrCreateBillServiceList(Login_Doctor.driver, logger).get(2));
					Web_GeneralFunctions.click(bp.getEmrCreateBillServiceList(Login_Doctor.driver, logger).get(2), "select a service", Login_Doctor.driver, logger);
					Web_GeneralFunctions.click(bp.getEmrCreateBillPayementModeTextBox(Login_Doctor.driver, logger, "paymentModeCash"), "check cash payment option", Login_Doctor.driver, logger);
					Web_GeneralFunctions.isChecked(bp.getEmrCreateBillPayementModeTextBox(Login_Doctor.driver, logger, "paymentModeCash"), Login_Doctor.driver);
					Web_GeneralFunctions.click(bp.getEmrCreateBillGenerateButton(Login_Doctor.driver, logger), "click on generate bill button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, bp.getEmrGeneratePatientNameBillNumber(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, bp.getEmrGenerateBillCloseButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(bp.getEmrGenerateBillCloseButton(Login_Doctor.driver, logger), "click on bill close button", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
					openInvisitDetailsModal();
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ovp.getCheckoutAndStartNewConsultationButton(Login_Doctor.driver, logger));
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ovp.getStartFollowupConsultationButton(Login_Doctor.driver, logger));
					assertTrue(Web_GeneralFunctions.isDisplayed(ovp.getCheckoutAndStartNewConsultationButton(Login_Doctor.driver, logger)));
					assertTrue(Web_GeneralFunctions.isDisplayed(ovp.getStartFollowupConsultationButton(Login_Doctor.driver, logger)));
					assertTrue(Web_GeneralFunctions.isDisplayed(ovp.getStartNewConsultationButton(Login_Doctor.driver, logger)));
					assertTrue(ovp.getOpenVisitButtonRows(Login_Doctor.driver, logger).size()==1);
					Web_GeneralFunctions.click(ovp.getCloseButton(Login_Doctor.driver, logger), "close open visit", Login_Doctor.driver, logger);
				}

				//SO_02,SO_26 covered here
				@Test(groups= {"SoftCheckout"},priority =48)
				public synchronized void verifyNotificationsInConsumerApp()throws Exception{
					logger = Reports.extent.createTest("EMR verify Notification in Consumer App After EMR softCheckout");
					pmt.clickOrgHeader();
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
					openConsumerApp();
					Web_GeneralFunctions.switchTabs("switch to consumer app", 1, Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getNotificationBell(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(ocp.getNotificationBell(Login_Doctor.driver, logger), "click on notification bell icon", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver,ocp.getLatestLabTestNotfication(Login_Doctor.driver, logger));
					assertTrue(Web_GeneralFunctions.isDisplayed(ocp.getLatestLabTestNotfication(Login_Doctor.driver, logger)));
					assertTrue(Web_GeneralFunctions.isDisplayed(ocp.getLatestMedicineNotification(Login_Doctor.driver, logger)));
					assertTrue(Web_GeneralFunctions.isDisplayed(ocp.getLatestReminderNotification(Login_Doctor.driver, logger)));
					assertTrue(Web_GeneralFunctions.isDisplayed(ocp.getLatestFeedbackNotification(Login_Doctor.driver, logger)));
				}
				
				//SO_44covered here
				@Test(groups= {"SoftCheckout"},priority =49)
				public synchronized void verifyLabTestAndMedicineNotificationsInConsumerApp()throws Exception{
					logger = Reports.extent.createTest("EMR verify LabTest and Medicine Notification in Consumer App");
					Web_GeneralFunctions.closeDriver(Login_Doctor.driver);
					Web_GeneralFunctions.switchTabs("switch to emr tab", 0, Login_Doctor.driver, logger);
					pmt.clickOrgHeader();
					openConsumerApp();
					Web_GeneralFunctions.switchTabs("switch to consumer app", 1, Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getNotificationElement(Login_Doctor.driver, logger));
					Web_GeneralFunctions.click(ocp.getNotificationElement(Login_Doctor.driver, logger), "click on notification bell icon", Login_Doctor.driver, logger);
					Web_GeneralFunctions.waitForElement(Login_Doctor.driver,ocp.getLatestLabTestNotfication(Login_Doctor.driver, logger));
					assertTrue(Web_GeneralFunctions.isDisplayed(ocp.getLatestLabTestNotfication(Login_Doctor.driver, logger)));
					assertTrue(Web_GeneralFunctions.isDisplayed(ocp.getLatestMedicineNotification(Login_Doctor.driver, logger)));
					assertTrue(Web_GeneralFunctions.isDisplayed(ocp.getLatestReminderNotification(Login_Doctor.driver, logger)));
					Web_GeneralFunctions.closeDriver(Login_Doctor.driver);
					Web_GeneralFunctions.switchTabs("switch to emr tab", 0, Login_Doctor.driver, logger);
				}
	
	public void moveToOpenConsultationsPage() throws Exception {
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, pm.getLeftMainMenu(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(pm.getLeftMainMenu(Login_Doctor.driver, logger), "Click Left main menu ", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,ocp.getOpenConsultationsSideBarLink(Login_Doctor.driver, logger) );
		Web_GeneralFunctions.click(ocp.getOpenConsultationsSideBarLink(Login_Doctor.driver, logger),"click on open consultations link",Login_Doctor.driver,logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,ocp.getSearchButton(Login_Doctor.driver, logger));
	}
	
	public synchronized void startConsultation() throws Exception {
		logger = Reports.extent.createTest("EMR start new Consultation");
		PracticeManagementPages pmp = new PracticeManagementPages();
		ConsultationTest consult = new ConsultationTest();
		if((!pmp.isappointmentDisplayed(Login_Doctor.driver, logger)) && (ocp.getActiveConsultationsOnAppointmentDashboard(Login_Doctor.driver, logger)!=null || ocp.getActiveFollowUpsOnAppointmentDashboard(Login_Doctor.driver, logger)!=null)) {
			int checkedIncount = ap.getCheckedinCount(Login_Doctor.driver, logger);
			int completedCount = ap.getCompletedCount(Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(ap.getAppointmentDropDown(checkedIncount+completedCount, Login_Doctor.driver, logger), "click on appointment dropdown", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getConsultfromAppointmentDropDown(checkedIncount+completedCount, Login_Doctor.driver, logger));
			Web_GeneralFunctions.click(ap.getConsultfromAppointmentDropDown(checkedIncount+completedCount, Login_Doctor.driver, logger), "click on consulting/followup button", Login_Doctor.driver, logger);
			Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
		}else {
			ConsultationTest.UHID=SheetsAPI.getDataProperties(Web_Testbase.input+".PatientUHID");
			consult.startConsultation();
		}
	}
	
	public synchronized void goToConsultationPage() throws Exception {
		logger = Reports.extent.createTest("EMR start new Consultation");
		int checkedIncount = ap.getCheckedinCount(Login_Doctor.driver, logger);
		int completedCount = ap.getCompletedCount(Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(ap.getAppointmentDropDown(checkedIncount+completedCount, Login_Doctor.driver, logger), "click on appointment dropdown", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getConsultfromAppointmentDropDown(checkedIncount+completedCount, Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(ap.getConsultfromAppointmentDropDown(checkedIncount+completedCount, Login_Doctor.driver, logger), "click on consulting/followup button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
	}
	
	public synchronized void openInvisitDetailsModal() throws Exception {
		logger = Reports.extent.createTest("EMR start Second new Consultation");
		AllSlotsPage asPage = new AllSlotsPage();
		ConsultationPage consultation = new ConsultationPage();
		pmt.clickOrgHeader();
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ap.getAllSlotsPage(Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(consultation.clickAllSlots(Login_Doctor.driver, logger), "Click all slots", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(10);	
			ConsultationTest.availableSlot = asPage.getAvailableSlot(Login_Doctor.driver, logger);
			ConsultationTest.slotId = asPage.getSlotId(ConsultationTest.availableSlot, Login_Doctor.driver, logger);
			ConsultationTest.prevSlotId = Integer.toString(Integer.parseInt(ConsultationTest.slotId)-2);
			Web_GeneralFunctions.scrollToElement(asPage.getPlusIconforAvailableSlot(ConsultationTest.prevSlotId, Login_Doctor.driver, logger), Login_Doctor.driver);
			Web_GeneralFunctions.wait(3);
			Web_GeneralFunctions.click(asPage.getPlusIconforAvailableSlot(ConsultationTest.slotId, Login_Doctor.driver, logger), "Clicking on Plus Icon", Login_Doctor.driver, logger);
			Web_GeneralFunctions.sendkeys(asPage.getSearchBoxinAvailableSlot(ConsultationTest.slotId, Login_Doctor.driver, logger), "*"+ConsultationTest.UHID, "sending value in search box", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(3);
			Web_GeneralFunctions.click(asPage.getFirstAvailableConsumerStartConsultation(Login_Doctor.driver, logger), "Clicking start consultation on first available consumer", Login_Doctor.driver, logger);
			Web_GeneralFunctions.wait(4);
		}
	
	public synchronized String getPDFContent() throws Exception {
		logger = Reports.extent.createTest("EMR Move to Pdf page");
		String url = Web_GeneralFunctions.switchTabs("Move to new PDF tab", 2, Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(2);
		DownloadPDF pdf = new DownloadPDF();
		String filename = pdf.pdfDownload(url);
		String pdfContent = pdf.verifyPDFContent(filename);
		Web_GeneralFunctions.closeDriver(Login_Doctor.driver);
		 Web_GeneralFunctions.switchTabs("Move to application tab", 1, Login_Doctor.driver, logger);
		return pdfContent;
	}
	
	public synchronized void openConsumerApp() throws Exception {
		logger = Reports.extent.createTest("EMR load consumer url");
		String consumerURL = SheetsAPI.getDataProperties(Web_Testbase.input+".PWAURL");
		String a = "window.open('"+consumerURL+"','_blank');";
		((JavascriptExecutor)Login_Doctor.driver).executeScript(a);
	}
	
	public synchronized void registerAndBookApppointment() throws Exception {
		logger = Reports.extent.createTest("EMR regsiter and book appointment");
		ConsultationPage consultation = new ConsultationPage();
		AllSlotsPage asPage = new AllSlotsPage();
		Web_GeneralFunctions.click(consultation.clickAllSlots(Login_Doctor.driver, logger), "Click all slots", Login_Doctor.driver, logger);
		Web_GeneralFunctions.wait(10);	
		ConsultationTest.availableSlot = asPage.getAvailableSlot(Login_Doctor.driver, logger);
		ConsultationTest.slotId = asPage.getSlotId(ConsultationTest.availableSlot, Login_Doctor.driver, logger);
		ConsultationTest.prevSlotId = Integer.toString(Integer.parseInt(ConsultationTest.slotId)-2);
		Web_GeneralFunctions.scrollToElement(asPage.getPlusIconforAvailableSlot(ConsultationTest.prevSlotId, Login_Doctor.driver, logger), Login_Doctor.driver);
		Web_GeneralFunctions.wait(3);
		Web_GeneralFunctions.click(asPage.getPlusIconforAvailableSlot(ConsultationTest.slotId, Login_Doctor.driver, logger), "Clicking on Plus Icon", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getRegisterPatientButton(ConsultationTest.slotId, Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(ocp.getRegisterPatientButton(ConsultationTest.slotId, Login_Doctor.driver, logger), "click on register button", Login_Doctor.driver, logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, ocp.getInPersonConsultationButton(ConsultationTest.slotId, Login_Doctor.driver, logger));
		Web_GeneralFunctions.click(prp.getTitleDropDown(Login_Doctor.driver, logger), "Clicking to Save without any Mandatory Details", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(prp.setTitlefromDropdown(Login_Doctor.driver, logger), "Clicking to Save ", Login_Doctor.driver, logger);
		String randomPatientName = RandomStringUtils.randomAlphabetic(6);
		Web_GeneralFunctions.sendkeys(prp.getNameTextBox(Login_Doctor.driver, logger), randomPatientName, "Clicking to Save without any Mandatory Details", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(prp.getAgeinYears(Login_Doctor.driver, logger), RandomStringUtils.randomNumeric(2), "Adding years", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(prp.getAgeinMonths(Login_Doctor.driver, logger), RandomStringUtils.randomNumeric(1), "Adding months", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(prp.getAgeinDays(Login_Doctor.driver, logger), RandomStringUtils.randomNumeric(1), "Adding days", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(prp.setMobileNoNA(Login_Doctor.driver, logger), "Clicking on N/A Button", Login_Doctor.driver, logger);
		if(prp.getGenderDropDown(Login_Doctor.driver, logger).isEnabled()) {
			Web_GeneralFunctions.click(prp.getGenderDropDown(Login_Doctor.driver, logger), "Clicking to Save", Login_Doctor.driver, logger);
			Web_GeneralFunctions.click(prp.setGenderfromDropdown(Login_Doctor.driver, logger),"Clicking to Save", Login_Doctor.driver, logger);
		}
		Web_GeneralFunctions.click(ocp.getInPersonConsultationButton(ConsultationTest.slotId, Login_Doctor.driver, logger),"click on in person consultation",Login_Doctor.driver,logger);
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, lrp.moveToLabResultModule(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, rp.moveToReferral(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, sp.getSymptomModuleElementFromRightMenu(Login_Doctor.driver, logger));
		Web_GeneralFunctions.waitForElement(Login_Doctor.driver, gpp.getGlobalPrintModule(Login_Doctor.driver, logger));
	}
	
	public synchronized String getFutureDate(int days) throws ParseException {
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		Calendar c = Calendar.getInstance();
		String todaysDate = sdf.format(d);
		c.setTime(sdf.parse(todaysDate));
		c.add(Calendar.DAY_OF_MONTH, days);
		return sdf.format(c.getTime());
	}
	
}
