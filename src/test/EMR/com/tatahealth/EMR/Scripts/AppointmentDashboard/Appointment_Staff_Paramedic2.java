package com.tatahealth.EMR.Scripts.AppointmentDashboard;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.Scripts.Login.Login_Staff_Paramedic;
import com.tatahealth.EMR.pages.AppointmentDashboard.AllSlotsPage;
import com.tatahealth.EMR.pages.AppointmentDashboard.AppointmentsPage;
import com.tatahealth.EMR.pages.AppointmentDashboard.ConsultationPage;
import com.tatahealth.EMR.pages.AppointmentDashboard.PreConsultationPage;
import com.tatahealth.EMR.pages.CommonPages.CommonPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.ReusableModules.reusableModules;

public class Appointment_Staff_Paramedic2 {
	
	ExtentTest logger;
	static WebDriver driver;
	
	@BeforeClass(alwaysRun=true)
	public static void beforeClass() throws Exception {
		Login_Staff_Paramedic.LoginTest();
		System.out.println("Initialized Driver");
		driver = Login_Staff_Paramedic.driver;
	}
	
	@AfterClass(alwaysRun=true)
	public static void afterClass() throws Exception {
		Login_Staff_Paramedic.LogoutTest();
		System.out.println("Closed Driver");
	}
	
	
	
	
	/***
	 * @author Bala Yaswanth
	 * @throws Exception
	 * Check appointment time scroll function - Booked appointment for shown slot
	 */
	@Test(priority=56,groups= {"Regression","Appointment_Digitizer"},enabled=false)
	public synchronized void APP_STA_PAR_006() throws Exception {
		
		logger = Reports.extent.createTest("APP_STA_PAR_010");
		
		AllSlotsPage ASPage = new AllSlotsPage();
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("mm");
		//String localDate = date.toString().substring(8, 10);
		
		ASPage.getDoctorWithTimings(Appointment_Staff_Paramedic.k, driver, logger).click();
		//Moving to All slots Page
		Thread.sleep(10000);
		int n = driver.findElements(By.className("appointmentTimeScroll")).size();
		Thread.sleep(10000);
		//System.out.println(n);
		
		//Checking and Printing time
		String SlotTime = ASPage.getTimeforSelectedSlot(n+1, driver, logger);
		int SlotTimeMin = Integer.parseInt(SlotTime.substring((SlotTime.indexOf(":")+1)));
		int CurrentTime = Integer.parseInt(df.format(date));
		System.out.println(SlotTimeMin);
		System.out.println(CurrentTime);
		
		if(SlotTimeMin-CurrentTime>=0||SlotTimeMin-CurrentTime<=-50) {
			System.out.println("Scrolled Correctly");
		}
		
	
	}
	
	
	
	
	
	
	
	
	
	/***
	 * @author Bala Yaswanth
	 * @throws Exception
	 * Blocked Slot check - All Cases
	 * Counts - All Cases
	 */
	@Test(priority=57,groups= {"Regression","Appointment_Digitizer"})
	public synchronized void APP_STA_PAR_007() throws Exception {
		
		logger = Reports.extent.createTest("APP_STA_PAR_011");
		
		
		AppointmentsPage appoiPage = new AppointmentsPage();
		AllSlotsPage ASpage = new AllSlotsPage();
		CommonPage CPage = new CommonPage();
		PreConsultationPage PreconPage = new PreConsultationPage();
		ConsultationPage ConsPage = new ConsultationPage();
 		//Moving to All slots Page
		// added code of one line
	//	Thread.sleep(5000);
	//	Web_GeneralFunctions.click(appoiPage.getDoctorDropdown( Login_Doctor.driver, logger), "...", Login_Doctor.driver, logger);

//		Web_GeneralFunctions.selectElementByVisibleText(appoiPage.getDoctorDropdown( Login_Doctor.driver, logger), "StgDocforstaff", "StgDocforstaff", Login_Doctor.driver, logger);
 	//	Web_GeneralFunctions.click(appoiPage.getAllSlotsPage(Login_Doctor.driver, logger), "Moving to All slots page from Appointments Page", Login_Doctor.driver, logger);
		Thread.sleep(5000);
	    new Select(driver.findElement(By.id("clinicUserId"))).selectByVisibleText("StgDocforstaff");
	   // driver.findElement(By.id("clinicUserId")).click();
		Thread.sleep(5000);

		int n = driver.findElements(By.className("appointmentTimeScroll")).size();
		int appCnt = 0;
		
		//Booking Appointment For Reschedule
		int BA_R = n+6;
		String slotId = ASpage.getSlotId(BA_R, driver, logger);
		String prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		String StartSlotTime = ASpage.getTimeforSelectedSlot(BA_R, driver, logger);
		String StartType = ASpage.getAMPMforSelectedSlot(BA_R, driver, logger);
		appCnt = appoiPage.getAppointmentCount(driver, logger);
		ScrollAndBookAllSlots(prevSlotId, slotId);
		appCountCheck(appCnt, true);
		
		
		
		//Booking Appointment For Cancel
		int BA_C = BA_R+1;
		slotId = ASpage.getSlotId(BA_C, driver, logger);
		prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		appCnt = appoiPage.getAppointmentCount(driver, logger);
		ScrollAndBookAllSlots(prevSlotId, slotId);
		appCountCheck(appCnt, true);
		
		
		//Booking Appointment & Checkin 
		int BA_CI = BA_C+1;
		slotId = ASpage.getSlotId(BA_CI, driver, logger);
		prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		appCnt = appoiPage.getAppointmentCount(driver, logger);
		ScrollAndBookAllSlots(prevSlotId, slotId);
		appCountCheck(appCnt, true);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, driver, logger), driver);
		//code changed --int CheckinCnt = appoiPage.getAppointmentCount(driver, logger);
		int CheckinCnt = appoiPage.getAppointmentCount(driver, logger);

		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI, driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI,1, driver, logger), "Clicking on Checkin in Dropdown", driver, logger);
		CheinCountCheck(CheckinCnt, true);
		Thread.sleep(6000);
		
		
		//Booking Appointment & Checkin & Preconsulting
		int BA_CI_P_ing = BA_CI+1;
		slotId = ASpage.getSlotId(BA_CI_P_ing, driver, logger);
		prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		appCnt = appoiPage.getAppointmentCount(driver, logger);
		ScrollAndBookAllSlots(prevSlotId, slotId);
		appCountCheck(appCnt, true);
		CheckinCnt = appoiPage.getCheckedinCount(driver, logger);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, driver, logger), driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI_P_ing, driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI_P_ing,1, driver, logger), "Clicking on Reschedule in Dropdown", driver, logger);
		Thread.sleep(6000);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, driver, logger), driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI_P_ing, driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI_P_ing,1, driver, logger), "Clicking on Reschedule in Dropdown", driver, logger);
		CheinCountCheck(CheckinCnt, true);
		Thread.sleep(5000);
		Web_GeneralFunctions.click(CPage.getPoweredByLink(driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Thread.sleep(6000);
		
		//Booking Appointment & Checkin & Preconsulted
		int BA_CI_P_ed = BA_CI_P_ing+1;
		slotId = ASpage.getSlotId(BA_CI_P_ed, driver, logger);
		prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		appCnt = appoiPage.getAppointmentCount(driver, logger);
		ScrollAndBookAllSlots(prevSlotId, slotId);
		appCountCheck(appCnt, true);
	//	changed code---CheckinCnt = appoiPage.getAppointmentCount(driver, logger);
		CheckinCnt = appoiPage.getCheckedinCount(driver, logger);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, driver, logger), driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI_P_ed, driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI_P_ed,1, driver, logger), "Clicking on Reschedule in Dropdown", driver, logger);
		Thread.sleep(6000);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, driver, logger), driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI_P_ed, driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI_P_ed,1, driver, logger), "Clicking on Reschedule in Dropdown", driver, logger);
		Thread.sleep(6000);
		Web_GeneralFunctions.scrollToElement(PreconPage.getSaveBtn(driver, logger), driver);
		Web_GeneralFunctions.click(PreconPage.getSaveBtn(driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Thread.sleep(6000);
		CheinCountCheck(CheckinCnt, true);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, driver, logger), driver);
		
		//Book Appointment and Consulting---blocked -- staff cannt do Consulting
		
		int BA_C_ing = BA_CI_P_ed+1;
		slotId = ASpage.getSlotId(BA_C_ing, driver, logger);
		prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		appCnt = appoiPage.getAppointmentCount(driver, logger);
		/*
		 * ScrollAndBookAllSlots(prevSlotId, slotId); appCountCheck(appCnt, true);
		 * CheckinCnt = appoiPage.getAppointmentCount(driver, logger);
		 * Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(
		 * prevSlotId, driver, logger), driver);
		 * Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_C_ing, driver,
		 * logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		 * Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_C_ing,2, driver,
		 * logger), "Clicking on Reschedule in Dropdown", driver, logger);
		 * ConsultPopupCheck(); Thread.sleep(6000);
		 * Web_GeneralFunctions.click(CPage.getPoweredByLink(driver, logger),
		 * "Clicking on Dropdown in Booked Appointment", driver, logger);
		 * Thread.sleep(6000); CheinCountCheck(CheckinCnt, true);
		 */
		
		//Book Appointment & Consulted--check out
		
		 // cahnged QA CheckinCnt = appoiPage.getAppointmentCount(driver, logger); 
		CheckinCnt = appoiPage.getCheckedinCount(driver, logger);
		
		  int BA_C_ed = BA_C_ing+1; slotId = ASpage.getSlotId(BA_C_ed, driver, logger); 
		  prevSlotId = Integer.toString(Integer.parseInt(slotId)-2); 
		  String EndSlotTime =ASpage.getTimeforSelectedSlot(BA_C_ed+1, driver, logger); 
		  String EndType =  ASpage.getAMPMforSelectedSlot(BA_C_ed+1, driver, logger); 
		  appCnt = appoiPage.getAppointmentCount(driver, logger);
		  
		/*
		 * ScrollAndBookAllSlots(prevSlotId, slotId); appCountCheck(appCnt, true); int
		 * CheckOutCnt = appoiPage.getAppointmentCount(driver, logger);
		 * Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(
		 * prevSlotId, driver, logger), driver);
		 * Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_C_ed, driver,
		 * logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		 * Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_C_ed,2, driver,
		 * logger), "Clicking on Reschedule in Dropdown", driver, logger);
		 * ConsultPopupCheck(); Thread.sleep(6000);
		 * Web_GeneralFunctions.scrollToElement(ConsPage.getCheckoutBtn(driver, logger),
		 * driver); Web_GeneralFunctions.click(CPage.getPoweredByLink(driver, logger),
		 * "Clicking on Dropdown in Booked Appointment", driver, logger);
		 * Thread.sleep(6000); CompletedCountCheck(CheckOutCnt, true); int newCheckinCnt
		 * = appoiPage.getAppointmentCount(driver, logger);
		 * if(newCheckinCnt==CheckinCnt+1) { System.out.println("Working Fine"); }
		 */
		 
		
		
		//Now main (p)art : Blocking the slots
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		String date = df.format(new Date());
		
		String BlockStartTime = date+" "+StartSlotTime+" "+StartType;
		String BlockEndTime = date+" "+EndSlotTime+" "+EndType;
		
		System.out.println("BlockStartTime...."+ BlockStartTime);
		System.out.println("BlockEndTime---"+BlockEndTime);
		Web_GeneralFunctions.click(ASpage.getBlockSlotIcon(driver, logger), "dummy", driver, logger);
		Web_GeneralFunctions.click(ASpage.setStartTime(driver, logger), "Cliked on start time", driver, logger);

		Web_GeneralFunctions.clear(ASpage.setStartTime(driver, logger), "dummy", driver, logger);
		Web_GeneralFunctions.sendkeys(ASpage.setStartTime(driver, logger), BlockStartTime, "dummy", driver, logger);
		Web_GeneralFunctions.click(ASpage.setEndTime(driver, logger), "Cliked on end time", driver, logger);

		Web_GeneralFunctions.clear(ASpage.setEndTime(driver, logger), "dummy", driver, logger);
		Web_GeneralFunctions.sendkeys(ASpage.setEndTime(driver, logger), BlockEndTime, "dummy", driver, logger);
		
		Web_GeneralFunctions.click(ASpage.getBlockReason(driver, logger), "dummy", driver, logger);
		Web_GeneralFunctions.click(ASpage.setBlockReason(driver, logger), "dummy", driver, logger);
		Web_GeneralFunctions.click(ASpage.getBlockSlotSubmitReason(driver, logger), "dummy", driver, logger);
		Thread.sleep(6000);
		
		
		
		
		
		
		//Now Clap for Checks after blocking slot
		//Rescheduling First Booked Appointment
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getBlockedSlot(BA_R, driver, logger), driver);
		Thread.sleep(6000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_R, driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_R,1, driver, logger), "Clicking on Reschedule in Dropdown", driver, logger);
		Web_GeneralFunctions.click(ASpage.getTimingsfromRescheduleAlert(driver, logger), "Clicking to get timings in Reschedule Popup", driver, logger);
		Web_GeneralFunctions.click(ASpage.setTimingsinRescheduleAlert((BA_R+2), driver, logger), "Clicking to select next slot timings in Reschedule Popup", driver, logger);
		Web_GeneralFunctions.click(ASpage.getOKBtnfromRescheduleAlert(driver, logger), "Clicking on Ok in Popup", driver, logger);
		Thread.sleep(3000);
		
		//Cancelling Second Booked Appointment
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getBlockedSlot(BA_C, driver, logger), driver);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_C, driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_C,2, driver, logger), "Clicking on Reschedule in Dropdown", driver, logger);
		Web_GeneralFunctions.click(ASpage.getReasondropdownfromCancelAlert(driver, logger), "Clicking to get reasons in Cancel Alert", driver, logger);
		Web_GeneralFunctions.click(ASpage.setReasoninCancelAlert(driver, logger),"Clicking on reasons in Cancel Alert", driver, logger);
		Web_GeneralFunctions.click(ASpage.getSubmitBtnfromCancelAlert(driver, logger), "Clicking on Submit in Alert", driver, logger);
		Thread.sleep(3000);
		
		
		//Cancelling Checked in Appointment
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getBlockedSlot(BA_CI, driver, logger), driver);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI, driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI,1, driver, logger), "Clicking on Reschedule in Dropdown", driver, logger);
		Web_GeneralFunctions.click(ASpage.getReasondropdownfromCancelAlert(driver, logger), "Clicking to get reasons in Cancel Alert", driver, logger);
		Web_GeneralFunctions.click(ASpage.setReasoninCancelAlert(driver, logger),"Clicking on reasons in Cancel Alert", driver, logger);
		Web_GeneralFunctions.click(ASpage.getSubmitBtnfromCancelAlert(driver, logger), "Clicking on Submit in Alert", driver, logger);
		Thread.sleep(3000);
		
		
		//Preconsulting check
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getBlockedSlot(BA_CI_P_ing, driver, logger), driver);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI_P_ing, driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI_P_ing,1, driver, logger), "Clicking on Reschedule in Dropdown", driver, logger);
		Thread.sleep(6000);
		Web_GeneralFunctions.click(CPage.getPoweredByLink(driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Thread.sleep(6000);
		
		
		//Preconsulted--check-out check--staff dont have access
		
		  Web_GeneralFunctions.scrollElementToCenter(ASpage.getBlockedSlot(BA_CI_P_ed,
		  driver, logger), driver); Thread.sleep(3000);
		  Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI_P_ed, driver,
		  logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		  Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI_P_ed,1, driver,
		  logger), "Clicking on Reschedule in Dropdown", driver, logger);
		  ConsultPopupCheck(); Thread.sleep(6000);
		  Web_GeneralFunctions.click(CPage.getPoweredByLink(driver, logger),
		  "Clicking on Dropdown in Booked Appointment", driver, logger);
		  Thread.sleep(6000);
		 
		
		
		//Consulting Check-- staff dont have access
		/*
		 * Web_GeneralFunctions.scrollElementToCenter(ASpage.getBlockedSlot(BA_C_ed,
		 * driver, logger), driver); Thread.sleep(3000);
		 * Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_C_ed, driver,
		 * logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		 * Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_C_ed,1, driver,
		 * logger), "Clicking on Reschedule in Dropdown", driver, logger);
		 * Thread.sleep(6000); Web_GeneralFunctions.click(CPage.getPoweredByLink(driver,
		 * logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		 * Thread.sleep(6000);
		 */
		
		
		/*
		 * //Consulted Check-- staff dont have access
		 * Web_GeneralFunctions.scrollElementToCenter(ASpage.getBlockedSlot(BA_C_ing,
		 * driver, logger), driver); Thread.sleep(3000);
		 * Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_C_ing, driver,
		 * logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		 * Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_C_ing,1, driver,
		 * logger), "Clicking on Reschedule in Dropdown", driver, logger);
		 * Thread.sleep(6000); Web_GeneralFunctions.click(CPage.getPoweredByLink(driver,
		 * logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		 * Thread.sleep(6000); Thread.sleep(6000);
		 */
		
		
		
		
	}
	
	
	
	
	
	/***
	 * @author Bala Yaswanth
	 * @throws Exception
	 * Booking Appointments today for past date check tomorrow
	 */
	@Test(priority=58,groups= {"Regression_PastDate","Appointment_Digitizer_PastDate"},enabled=false)
	public synchronized void APP_STA_PAR_008() throws Exception {
		
		logger = Reports.extent.createTest("APP_STA_PAR_012");
		
		
		AllSlotsPage ASpage = new AllSlotsPage();
		AppointmentsPage appoiPage = new AppointmentsPage();
		CommonPage CPage = new CommonPage();
		PreConsultationPage PreconPage = new PreConsultationPage();
		ConsultationPage ConsPage = new ConsultationPage();
		
		//Moving to All slots Page
		
		Thread.sleep(5000);
		int n = driver.findElements(By.className("applist")).size();
		
		//Booking Appointment
		int BA_R = n-5;
		String slotId = ASpage.getSlotId(BA_R, driver, logger);
		String prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		ScrollAndBookAllSlots(prevSlotId, slotId);
		
		//Booking Appointment & Checkin 
		int BA_CI = BA_R+1;
		slotId = ASpage.getSlotId(BA_CI, driver, logger);
		prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		ScrollAndBookAllSlots(prevSlotId, slotId);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, driver, logger), driver);
		int CheckinCnt = appoiPage.getAppointmentCount(driver, logger);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI, driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI,1, driver, logger), "Clicking on Checkin in Dropdown", driver, logger);
		CheinCountCheck(CheckinCnt, true);
		Thread.sleep(6000);
		
		
		//Booking Appointment & Checkin & Preconsulting
		int BA_CI_P_ing = BA_CI+1;
		slotId = ASpage.getSlotId(BA_CI_P_ing, driver, logger);
		prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		ScrollAndBookAllSlots(prevSlotId, slotId);
		CheckinCnt = appoiPage.getAppointmentCount(driver, logger);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, driver, logger), driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI_P_ing, driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI_P_ing,1, driver, logger), "Clicking on Reschedule in Dropdown", driver, logger);
		Thread.sleep(6000);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, driver, logger), driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI_P_ing, driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI_P_ing,1, driver, logger), "Clicking on Reschedule in Dropdown", driver, logger);
		CheinCountCheck(CheckinCnt, true);
		Thread.sleep(5000);
		Web_GeneralFunctions.click(CPage.getPoweredByLink(driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		
		Thread.sleep(6000);
		
		//Booking Appointment & Checkin & Preconsulted
		int BA_CI_P_ed = BA_CI_P_ing+1;
		slotId = ASpage.getSlotId(BA_CI_P_ed, driver, logger);
		prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		ScrollAndBookAllSlots(prevSlotId, slotId);
		CheckinCnt = appoiPage.getAppointmentCount(driver, logger);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, driver, logger), driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI_P_ed, driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI_P_ed,1, driver, logger), "Clicking on Reschedule in Dropdown", driver, logger);
		Thread.sleep(6000);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, driver, logger), driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI_P_ed, driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI_P_ed,1, driver, logger), "Clicking on Reschedule in Dropdown", driver, logger);
		Thread.sleep(6000);
		Web_GeneralFunctions.scrollToElement(PreconPage.getSaveBtn(driver, logger), driver);
		Web_GeneralFunctions.click(PreconPage.getSaveBtn(driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Thread.sleep(6000);
		CheinCountCheck(CheckinCnt, true);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, driver, logger), driver);
		
		//Book Appointment and Consulting
		int BA_C_ing = BA_CI_P_ed+1;
		slotId = ASpage.getSlotId(BA_C_ing, driver, logger);
		prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		ScrollAndBookAllSlots(prevSlotId, slotId);
		CheckinCnt = appoiPage.getAppointmentCount(driver, logger);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, driver, logger), driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_C_ing, driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_C_ing,3, driver, logger), "Clicking on Reschedule in Dropdown", driver, logger);
		ConsultPopupCheck();
		Thread.sleep(6000);
		Web_GeneralFunctions.click(CPage.getPoweredByLink(driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		
		Thread.sleep(6000);
		CheinCountCheck(CheckinCnt, true);
		
		//Book Appointment & Consulted
		CheckinCnt = appoiPage.getAppointmentCount(driver, logger);
		int BA_C_ed = BA_C_ing+1;
		slotId = ASpage.getSlotId(BA_C_ed, driver, logger);
		prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
		ScrollAndBookAllSlots(prevSlotId, slotId);
		int CheckOutCnt = appoiPage.getAppointmentCount(driver, logger);
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, driver, logger), driver);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_C_ed, driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_C_ed,3, driver, logger), "Clicking on Reschedule in Dropdown", driver, logger);
		ConsultPopupCheck();
		Thread.sleep(6000);
		Web_GeneralFunctions.scrollToElement(ConsPage.getCheckoutBtn(driver, logger), driver);
		Web_GeneralFunctions.click(CPage.getPoweredByLink(driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		
		Thread.sleep(6000);
		CompletedCountCheck(CheckOutCnt, true);
		int newCheckinCnt = appoiPage.getAppointmentCount(driver, logger);
		if(newCheckinCnt==CheckinCnt+1) {
			System.out.println("Working Fine");
		}
		
	
	}
	
	
	
	
	/***
	 * @author Bala Yaswanth
	 * @throws Exception
	 * Check past appointments
	 */
	@Test(priority=99,groups= {"Regression_PastDate","Appointment_Digitizer_PastDate"},enabled=false)
	public synchronized void APP_STA_PAR_009() throws Exception {
		
		logger = Reports.extent.createTest("APP_STA_PAR_013");
		
		AllSlotsPage ASpage = new AllSlotsPage();
		CommonPage CPage = new CommonPage();
		driver.findElement(By.cssSelector("#inputDate")).click();
		Thread.sleep(2000);
		
		int week = reusableModules.getcurrentWeekfromCalendar();
		int n = reusableModules.getcurrentDayfromCalendar(driver);
		
		if(week==1&&n==1) {
			week = 7;
			n = 5;
			
		}else if(week==1){
			week = 7;
			n = n-1;
		}else {
			week = week-1;
		}
		
		driver.findElement(By.cssSelector("#inputDate")).click();
		if(week==7&&n==5) {
			Web_GeneralFunctions.click(ASpage.getPrevMonthBtn(driver, logger), "Clicking to go to previous month", driver, logger);
		}
		Web_GeneralFunctions.click(ASpage.getDatetoClick(n, week, driver, logger), "Clicking on previous date", driver, logger);
		Thread.sleep(3000);
		int slotsCnt = driver.findElements(By.className("appointmentTimeScroll")).size();
		System.out.println(slotsCnt);
		
		int BA_R = slotsCnt-5;
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getAppointmentDropDown(BA_R, driver, logger), driver);
		Thread.sleep(6000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_R, driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		try {
			Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_R,1, driver, logger), "Clicking on Reschedule in Dropdown", driver, logger);
		}catch(Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(3000);
		
		//Checked In
		int BA_CI = BA_R+1;
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getAppointmentDropDown(BA_CI, driver, logger), driver);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI, driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		try {
			Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI,1, driver, logger), "Clicking on Reschedule in Dropdown", driver, logger);
		}catch(Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(3000);
		
		//Preconsulting check
		int BA_CI_P_ing = BA_CI+1;
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getAppointmentDropDown(BA_CI_P_ing, driver, logger), driver);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI_P_ing, driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		try {
			Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI_P_ing,1, driver, logger), "Clicking on Reschedule in Dropdown", driver, logger);
		}catch(Exception e) {
			e.printStackTrace();
		}
		Thread.sleep(6000);
		
		
		//Consulted Check
		int BA_C_ed = BA_CI_P_ing+2;
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getBlockedSlot(BA_C_ed, driver, logger), driver);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_C_ed, driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_C_ed,1, driver, logger), "Clicking on Reschedule in Dropdown", driver, logger);
		Thread.sleep(6000);
		Web_GeneralFunctions.click(CPage.getPoweredByLink(driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		
		Thread.sleep(6000);
		
		
		//Consulted Check
		int BA_C_ing = BA_C_ed+1;
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getBlockedSlot(BA_C_ing, driver, logger), driver);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_C_ing, driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_C_ing,1, driver, logger), "Clicking on Reschedule in Dropdown", driver, logger);
		Thread.sleep(6000);
		Web_GeneralFunctions.click(CPage.getPoweredByLink(driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Thread.sleep(6000);
		
		Thread.sleep(6000);
		
		//preconsulted check
		int BA_CI_P_ed = BA_CI_P_ing+1;
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getAppointmentDropDown(BA_CI_P_ed, driver, logger), driver);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(BA_CI_P_ed, driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(BA_CI_P_ed,1, driver, logger), "Clicking on Reschedule in Dropdown", driver, logger);
		ConsultPopupCheck();
		Thread.sleep(6000);
		Web_GeneralFunctions.click(CPage.getPoweredByLink(driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Thread.sleep(6000);
		
	
	}
	
	
	
	
	
	/***
	 * @author Bala Yaswanth
	 * @throws Exception
	 * Appointment Booking - Reschedule and Cancel - Future Date
	 */
	@Test(priority=64,groups= {"Regression","Appointment_Digitizer"},enabled=false)
	public synchronized void APP_STA_PAR_010() throws Exception {
		
		logger = Reports.extent.createTest("APP_STA_PAR_014");
		
		AllSlotsPage ASpage = new AllSlotsPage();
		CommonPage CPage = new CommonPage();
		Thread.sleep(2000);
		driver.findElement(By.cssSelector("#inputDate")).click();
		Thread.sleep(2000);
		
		int week = reusableModules.getcurrentWeekfromCalendar();
		int n = reusableModules.getcurrentDayfromCalendar(driver);
		
		if(week==7) {
			week = 1;
			n = n+1;
		}else {
			week = week+1;
		}
		
		
		Web_GeneralFunctions.click(ASpage.getDatetoClick(n, week, driver, logger), "Clicking on Future date", driver, logger);
		Thread.sleep(6000);
		
		//Getting Available Slot Code
		n = ASpage.getAvailableSlot(driver, logger);
		System.out.println(n);
		String slotId = ASpage.getSlotId(n, driver, logger);
		String prevSlotId = Integer.toString(Integer.parseInt(slotId)-2);
				
		//Scrolling to available slot and booking appointment
		ScrollAndBookAllSlots(prevSlotId, slotId);
				
				
		//Rescheduling appointment to next slot
		Web_GeneralFunctions.scrollToElement(ASpage.getPlusIconforAvailableSlot(prevSlotId, driver, logger), driver);
		Thread.sleep(2000);
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n, 1, driver, logger), "Clicking on Reschedule in Dropdown", driver, logger);
		Thread.sleep(2000);
		Web_GeneralFunctions.click(ASpage.getTimingsfromRescheduleAlert(driver, logger), "Clicking to get timings in Reschedule Popup", driver, logger);
		Web_GeneralFunctions.click(ASpage.setTimingsinRescheduleAlert((n+2), driver, logger), "Clicking to select next slot timings in Reschedule Popup", driver, logger);
		Web_GeneralFunctions.click(ASpage.getOKBtnfromRescheduleAlert(driver, logger), "Clicking on Ok in Popup", driver, logger);
		Thread.sleep(6000);
		
		//Cancelling Appointment
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getPlusIconforAvailableSlot(slotId, driver, logger), driver);
		n=n+1;
		Web_GeneralFunctions.click(ASpage.getAppointmentDropDown(n, driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		Web_GeneralFunctions.click(ASpage.getButtonfromDropdown(n, 2, driver, logger), "Clicking on Reschedule in Dropdown", driver, logger);
		Web_GeneralFunctions.click(ASpage.getReasondropdownfromCancelAlert(driver, logger), "Clicking to get reasons in Cancel Alert", driver, logger);
		Web_GeneralFunctions.click(ASpage.setReasoninCancelAlert(driver, logger),"Clicking on reasons in Cancel Alert", driver, logger);
		Web_GeneralFunctions.click(ASpage.getSubmitBtnfromCancelAlert(driver, logger), "Clicking on Submit in Alert", driver, logger);
		Thread.sleep(6000);
		
		Web_GeneralFunctions.click(CPage.getPoweredByLink(driver, logger), "Clicking on Dropdown in Booked Appointment", driver, logger);
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public synchronized void ScrollAndBookAllSlots(String prevSlotId,String slotId) throws Exception {
		
		
		
		AllSlotsPage ASpage = new AllSlotsPage();
		Thread.sleep(3000);
		Web_GeneralFunctions.scrollElementToCenter(ASpage.getPlusIconforAvailableSlot(slotId, driver, logger), driver);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getPlusIconforAvailableSlot(slotId, driver, logger), "Clicking on Plus Icon", driver, logger);
		Web_GeneralFunctions.sendkeys(ASpage.getSearchBoxinAvailableSlot(slotId, driver, logger), "8553406065", "sending value in search box", driver, logger);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(ASpage.getFirstAvailableConsumer(driver, logger), "Clicking on first available consumer", driver, logger);
		Web_GeneralFunctions.sendkeys(ASpage.setReasonforVisit(driver, logger), "EMR Automation Testing", "Setting Visit Reason", driver, logger);
		Web_GeneralFunctions.click(ASpage.getBookAppointmentSubmitBtn(driver, logger), "Clicking to Submit Appointment", driver, logger);
		Thread.sleep(6000);
		
	}
	
	
	
	public synchronized void appCountCheck(int prevCnt,boolean inc) throws Exception {
		
		AppointmentsPage appoiPage = new AppointmentsPage();
		int appCnt = appoiPage.getAppointmentCount(driver, logger);
		if(inc&&appCnt==prevCnt+1) {
			System.out.println("Count Properly Increased");
		}else if(!inc&&appCnt==prevCnt-1) {
			System.out.println("Count Properly Increased");
		}else {
			System.out.println("Error: Please Check Counts Again");
		}
		
		
	}
	
	
	
	public synchronized void CheinCountCheck(int prevCnt,boolean inc) throws Exception {
		
		AppointmentsPage appoiPage = new AppointmentsPage();
		int appCnt = appoiPage.getCheckedinCount(driver, logger);
		if(inc&&appCnt==prevCnt+1) {
			System.out.println("Count Properly Increased");
		}else if(!inc&&appCnt==prevCnt-1) {
			System.out.println("Count Properly Increased");
		}else {
			System.out.println("Error: Please Check Counts Again");
		}
		
		
	}
	
	
	public synchronized void CompletedCountCheck(int prevCnt,boolean inc) throws Exception {
		
		AppointmentsPage appoiPage = new AppointmentsPage();
		//changed code ---int appCnt = appoiPage.getCheckedinCount(driver, logger); 
		int appCnt = appoiPage.getCompletedCount(driver, logger);
		if(inc&&appCnt==prevCnt+1) {
			System.out.println("Count Properly Increased");
		}else if(!inc&&appCnt==prevCnt-1) {
			System.out.println("Count Properly Increased");
		}else {
			System.out.println("Error: Please Check Counts Again");
		}
		
		
	}
	
	
	public synchronized void ConsultPopupCheck() throws Exception {
		
		
		CommonPage CPage = new CommonPage();
		try {
			if(CPage.getConsultationAlert(driver, logger,10).isDisplayed()) {
				Web_GeneralFunctions.click(CPage.getConsultationAlert(driver, logger), "Clicking to Checkout and start new Appointment", driver, logger);
			}
		}catch (Exception e) {
			
		}
		
	}
	
	
	public synchronized void NoDiagnosisCheck() throws Exception {
		
		
		ConsultationPage ConsPage = new ConsultationPage();
		
		if(ConsPage.getYesinSweetAlert(driver, logger).isDisplayed()) {
			Web_GeneralFunctions.click(ConsPage.getYesinSweetAlert(driver, logger),"Clicking to Checkout and start new Appointment", driver, logger);
		}
		
	}
	
	
	
			
}