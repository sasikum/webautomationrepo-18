package com.tatahealth.API.Billing;

import org.json.JSONObject;
import java.io.FileWriter; 
import java.io.IOException; 

public class PaymentDetails {
	
	public static String modeOfPayment;
	public static String appointmentId;
	public static Integer billId;
	public static	Integer amount;
	public static Integer additionalServiceAmount;
	public static Integer serviceDiscount;
	public static Integer additionalDiscount;
	public static String paid;
	public static String appointmentName;
	public static String slotId;
	public static String status;
	public JSONObject consultationService;
	public JSONObject extraService;
	
	
	public String getModeOfPayment() {
		return modeOfPayment;
	}
	public void setModeOfPayment(String modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}
	public String getSlotId() {
		return slotId;
	}
	public void setSlotId(String slotId) {
		this.slotId = slotId;
	}
	public String getAppointmentId() {
		return appointmentId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public void setAppointmentId(String appointmentId) {
		this.appointmentId = appointmentId;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public Integer getAdditionalServiceAmount() {
		return additionalServiceAmount;
	}
	public void setAdditionalServiceAmount(Integer additionalServiceAmount) {
		this.additionalServiceAmount = additionalServiceAmount;
	}
	public Integer getServiceDiscount() {
		return serviceDiscount;
	}
	public void setServiceDiscount(Integer serviceDiscount) {
		this.serviceDiscount = serviceDiscount;
	}
	public Integer getAdditionalDiscount() {
		return additionalDiscount;
	}
	public void setAdditionalDiscount(Integer additionalDiscount) {
		this.additionalDiscount = additionalDiscount;
	}
	public String getPaid() {
		return paid;
	}
	public void setPaid(String paid) {
		this.paid = paid;
	}
	public String getAppointmentName() {
		return appointmentName;
	}
	public void setAppointmentName(String appointmentName) {
		this.appointmentName = appointmentName;
	}
	public Integer getBillId() {
		return billId;
	}
	public void setBillId(Integer billId) {
		this.billId = billId;
	}
	public JSONObject getConsultationService() {
		return consultationService;
	}
	public void setConsultationService(JSONObject consultationService) {
		this.consultationService = consultationService;
	}
	public JSONObject getExtraService() {
		return extraService;
	}
	public void setExtraService(JSONObject extraService) {
		this.extraService = extraService;
	}
	
	public void printPaymentDetails(String reportname){
		try{
			FileWriter logFile = new FileWriter(reportname+"_automation_log.txt",true);
			logFile.write("\n -----------------------------Printing payment details obj-----------------------------");
		    logFile.write("\n modeOfPayment String:"+modeOfPayment);
		    logFile.write("\n AppointmentId String:"+ appointmentId);
		    logFile.write("\n billId int:"+ billId);
		    logFile.write("\n amount int:"+amount);
		    logFile.write("\n additionalServiceAmount int:"+additionalServiceAmount);
		    logFile.write("\n serviceDiscount int:"+serviceDiscount);
		    logFile.write("\n additionalDiscount int"+ additionalDiscount);
		    logFile.write("\n paid string:"+paid);
		    logFile.write("\n appointmentName string:"+appointmentName);
		    logFile.write("\n status string:"+status);
		    logFile.write("\n consultationService:"+ consultationService);
		    logFile.write("\n extraService:"+extraService);
		    logFile.write("\n ----------------------------------------------------------\n\n");
		    logFile.close();
		}catch (IOException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
		
	}
}
